#include <Server/server.h>

#include <QtCore/QCoreApplication>

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);

    Server *server = new Server();

    return app.exec();
}
