CONFIG += c++11
QT += sql network

HEADERS += \
    Server/server.h \
    Server/database.h \
    Server/client.h

SOURCES += \
    Server/server.cpp \
    main.cpp \
    Server/database.cpp \
    Server/client.cpp
