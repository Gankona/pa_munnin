#include "server.h"

Server::Server()
{
    clientList.clear();
    db = new DataBase;

    createServer();
}

void Server::createServer()
{
    server = new QTcpServer(this);
    server->listen(QHostAddress::Any, 32802);
    qDebug() << "server create at " << QDateTime::currentDateTime().toString();
    QObject::connect(server, SIGNAL(newConnection()), SLOT(slotNewConnect()));
}

void Server::slotNewConnect()
{
    clientList.push_back(new Client(server->nextPendingConnection(), db));
    qDebug() << "CONNECT!";
}
