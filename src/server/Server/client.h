#ifndef CLIENT_H
#define CLIENT_H

#include <Server/database.h>

#include <QtCore/QDir>
#include <QtCore/QTime>
#include <QtCore/QDebug>
#include <QtCore/QDataStream>
#include <QtNetwork/QTcpSocket>

class Client : public QObject
{
    Q_OBJECT
public:
    Client(QTcpSocket *socketS, DataBase *dbS);
    DataBase *db;
    QTcpSocket *socket;
    QDataStream stream;
    QDir dir;

    bool isAvtorizate;

public slots:
    void slotReadyRead();
};

#endif // CLIENT_H
