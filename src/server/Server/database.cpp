#include "database.h"

DataBase::DataBase()
{
    if (connectToDB()){
        qDebug() << "Подсоединидись к БД";
        query = new QSqlQuery(db);
        query->exec("CREATE TABLE IF NOT EXISTS Users ("
                    "name STRING, login STRING, email STRING,"
                    "password STRING, server TEXT,"
                    "timeReg DATETIME, lastEnter DATETIME);");
    }
    else
        qDebug() << "Не смогли подключиться к БД";
}

bool DataBase::connectToDB()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("accountData");
    bool ok = db.open();
    return ok;
}
