#ifndef SERVER_H
#define SERVER_H

#include <Server/client.h>
#include <Server/database.h>

#include <QtCore/QTimer>
#include <QtNetwork/QTcpServer>

class Server : public QObject
{
    Q_OBJECT
public:
    Server();
    QTcpServer *server;
    QList <Client*> clientList;
    DataBase *db;

    void createServer();

signals:

public slots:
    void slotNewConnect();
};

#endif // SERVER_H
