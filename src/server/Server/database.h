#ifndef DATABASE_H
#define DATABASE_H

#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

class DataBase
{
public:
    DataBase();
    QSqlDatabase db;
    QSqlQuery *query;

    bool connectToDB();
};

#endif // DATABASE_H
