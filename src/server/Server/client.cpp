#include "client.h"

Client::Client(QTcpSocket *socketS, DataBase *dbS)
{
    isAvtorizate = false;
    db = dbS;
    socket = socketS;

    QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    stream.setDevice(socket);
}

void Client::slotReadyRead()
{
    /// 0 - аккаунт не существует
    /// 1 - пароль неверен
    /// 2 - логин уже занят
    /// 3 - не удалось зарегестрировать, причина неизвестна

    QString var;
    stream >> var;
    qDebug() << "Client::slotReadyRead() " << var;

    //авторизация
    if (var == "login"){
        QStringList list;
        stream >> list;
        bool isSend = db->query->exec("SELECT name, email, password, timeReg, server FROM Users WHERE login = '"
                        + list.at(0) + "';");
        qDebug() << "SELECT name, password, email, timeReg, server FROM Users WHERE login = '"
                    + list.at(0) + "';" << isSend;
        db->query->next();
        if (list.at(1) == db->query->value(2).toString()){
            isAvtorizate = true;
            stream << QString("login")
                   << true << QVariantList{db->query->value(0), QVariant(list.at(0)), db->query->value(1),
                                           db->query->value(2), db->query->value(3), db->query->value(4)};
            qDebug() << QString("login")
                   << true << QVariantList{db->query->value(0), QVariant(list.at(0)), db->query->value(1),
                                           db->query->value(2), db->query->value(3), db->query->value(4)};
            dir = QDir::current();
            if (! dir.cd(list.at(1))){
                dir.mkdir(list.at(1));
                dir.cd(list.at(1));
            }
        }
        else if (db->query->value(0).isNull())
            stream << QString("login") << false << 0;
        else
            stream << QString("login") << false << 1;
    }

    //регистрация
    else if (var == "reg"){
        QStringList list;
        stream >> list;
        db->query->exec("SELECT login FROM Users WHERE login = "
                        +list.at(1) + ';');
        db->query->next();
        if (db->query->isNull(0)){
            qDebug() << "Client::slotReadyRead() norm";
            if (db->query->exec("INSERT INTO Users "
                            "(name, login, email, password, timeReg, lastEnter) VALUES('"
                            + list.at(0) + "', '" + list.at(1) + "', '"
                            + list.at(2) + "', '" + list.at(3) + "', '"
                            + QTime::currentTime().toString("dd/MM/yyyy_hh:mm:ss") + "', '"
                                + QTime::currentTime().toString("dd/MM/yyyy_hh:mm:ss") + "');")){
                stream << QString("reg")
                       << true << QVariantList{list.at(0), list.at(1), list.at(2),
                          list.at(3), QTime::currentTime(), QTime::currentTime()};
                qDebug() << QString("reg")
                         << true << QVariantList{list.at(0), list.at(1), list.at(2),
                            list.at(3), QTime::currentTime(), QTime::currentTime()};
            }
            else
                stream << QString("reg") << false << 3;
        }
        else
            stream << QString("reg") << false << 2;
    }

    //смена профиля
    else if (var == "editProfile"){}

    //если не авторизированы то и дальше нечего делать
    else if (! isAvtorizate)
        return;
}
