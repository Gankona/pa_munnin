#include "control.h"

Control::Control()
{
    qDebug() << "start " << QTime::currentTime();
    model = new Model;

#ifdef Q_OS_ANDROID
    window = new MobileView;
#else
    window = new DesktopView;
#endif

    window->setWindowTitle(tr("Munnin"));
    qDebug() << "end " << QTime::currentTime();

    QObject::connect(model,  SIGNAL(signalSetLogin(MProfileInfo*)),
                     window, SLOT  (slotSetLogin  (MProfileInfo*)));

    QObject::connect(model,  SIGNAL(signalSetMsg(QString, int)),
                     window, SLOT  (slotSetMsg  (QString, int)));

    QObject::connect(model,  SIGNAL(signalSetData(QList<MAbstractData*>)),
                     window, SIGNAL(signalSetData(QList<MAbstractData*>)));

    QObject::connect(window, SIGNAL(signalGetLogin(QStringList)),
                     model,  SLOT  (slotGetLogin  (QStringList)));

    QObject::connect(window, SIGNAL(signalGetRegistration(QStringList)),
                     model,  SLOT  (slotGetRegistration  (QStringList)));

    QObject::connect(window, SIGNAL(signalLogout()),
                     model,  SLOT  (slotGetLogout()));

    QObject::connect(window, SIGNAL(signalWriteEditTodo(MAbstractData)),
                     model,  SLOT  (slotEditStick      (MAbstractData)));

    QObject::connect(window, SIGNAL(signalWriteTodo(MAbstractData)),
                     model,  SLOT  (slotWriteStick (MAbstractData)));

    QObject::connect(window, SIGNAL(signalGetData(Menum)),
                     model,  SIGNAL(signalGetData(Menum)));

    model->firstStart();
}

void Control::start(QSplashScreen &splash)
{
    int timeToStart = 1500;
    QTimer::singleShot(timeToStart, &splash, SLOT(hide()));

#ifdef Q_OS_ANDROID
    QTimer::singleShot(timeToStart + 100, window, SLOT(show()));
#else
    QTimer::singleShot(timeToStart + 100, window->tray, SLOT(show()));
#endif
}
