#include <control.h>

#include <QtWidgets/QApplication>

int main (int argc, char **argv)
{
    QApplication app(argc, argv);

    QSplashScreen splashScreen(QPixmap(":/Other/Files/Images/Other/trayIcon.jpg"));
    splashScreen.show();

    Control *main = new Control;
    main->start(splashScreen);

    app.setQuitOnLastWindowClosed(false);
    app.setWindowIcon(QIcon(":/Other/Files/Images/Other/trayIcon.jpg"));
    app.setApplicationName(QObject::tr("Муннин"));

    return app.exec();
}
