CONFIG += c++11

QT += widgets

QT -= gui

include($$PWD/../../lib/Model/Model.pri)
include($$PWD/../../lib_view/DesktopView/DesktopView.pri)
include($$PWD/../../lib_view/MobileView/MobileView.pri)

HEADERS += \
    control.h

SOURCES += \
    main.cpp \
    control.cpp
