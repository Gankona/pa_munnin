#ifndef MFLOATOPASITYWIDGET_H
#define MFLOATOPASITYWIDGET_H



class MFloatOpasityWidget : public QFrame
{
    Q_OBJECT
public:
    explicit MFloatOpasityWidget(QWidget *parent = nullptr);

    void deleteWidget();

private:
    QGraphicsOpacityEffect *opacity;

public slots:
    void showWidget(QPoint point);
};

#endif // MFLOATOPASITYWIDGET_H
