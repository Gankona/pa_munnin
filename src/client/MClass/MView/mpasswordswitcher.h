#ifndef MPASSWORDSWITCHER_H
#define MPASSWORDSWITCHER_H



class MPasswordSwitcher : public QLabel
{
public:
    explicit MPasswordSwitcher(QWidget *parent = 0,
                      QLineEdit *lineS = nullptr,
                      QLineEdit *line2S = nullptr);

    explicit MPasswordSwitcher(QLineEdit *lineS = nullptr,
                      QLineEdit *line2S = nullptr);

private:
    QLineEdit *line;
    QLineEdit *line2;

    bool isVisiblePassword;

    void mousePressEvent(QMouseEvent*);
};

#endif // MPASSWORDSWITCHER_H
