#ifndef CONTROL_H
#define CONTROL_H

#include "model.h"

#ifdef Q_OS_ANDROID
#include "mobileview.h"
#else
#include "desktopview.h"
#endif

#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtWidgets/QSplashScreen>

class Control : public QObject
{
    Q_OBJECT
public:
    Control();

    void start(QSplashScreen &splash);

private:
#ifdef Q_OS_ANDROID
    MobileView *window;
#else
    DesktopView *window;
#endif

    Model *model;
};

#endif // CONTROL_H
