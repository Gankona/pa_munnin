#-------------------------------------------------
#
# Project created by QtCreator 2016-01-17T02:27:43
#
#-------------------------------------------------

QT       -= gui

TARGET = MVersion
TEMPLATE = lib

DEFINES += MVERSION_LIBRARY

SOURCES += mversion.cpp

HEADERS += mversion.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MVersion.pri
