isEmpty(MVersion.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib/MVersion/ -lMVersion
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib/MVersion/ -lMVersion

    INCLUDEPATH += $$PWD/../../lib/MVersion
    DEPENDPATH += $$PWD/../../lib/MVersion
}
