#ifndef MVERSION_H
#define MVERSION_H

#include <QtCore/qglobal.h>

#if defined(MVERSION_LIBRARY)
#  define MVERSIONSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MVERSIONSHARED_EXPORT Q_DECL_IMPORT
#endif

class MVERSIONSHARED_EXPORT MVersion
{

public:
    MVersion();
};

#endif // MVERSION_H
