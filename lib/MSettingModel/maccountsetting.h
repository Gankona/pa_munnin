#ifndef MACCOUNTSETTING_H
#define MACCOUNTSETTING_H

#include <QtCore/QObject>
#include <QtCore/QSettings>

class MAccountSetting : public QObject
{
    Q_OBJECT
public:
    MAccountSetting();
    QSettings setting;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, MAccountSetting &s);
    friend QDataStream &operator >> (QDataStream &d, MAccountSetting &s);

signals:
    void saveSetting();
};

#endif // MACCOUNTSETTING_H
