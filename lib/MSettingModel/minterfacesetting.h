#ifndef MINTERFACESETTING_H
#define MINTERFACESETTING_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>

class MInterfaceSetting : public QObject
{
    Q_OBJECT
public:
    MInterfaceSetting();
    QSettings setting;

    QString language;
    QString style;
    int volume;
    QString dateTimeFormat;
    QString firstWeekDay;
    float timeZone;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, MInterfaceSetting &s);
    friend QDataStream &operator >> (QDataStream &d, MInterfaceSetting &s);

signals:
    void saveSetting();
};

#endif // MINTERFACESETTING_H
