#ifndef MSETTINGMODEL_H
#define MSETTINGMODEL_H

#include "maccountsetting.h"
#include "mgraphicssetting.h"
#include "mhotkeysetting.h"
#include "minterfacesetting.h"
#include "mserversetting.h"

#include <QtCore/qglobal.h>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QDataStream>

#if defined(MSETTINGMODEL_LIBRARY)
#  define MSETTINGMODELSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MSETTINGMODELSHARED_EXPORT Q_DECL_IMPORT
#endif

class MSETTINGMODELSHARED_EXPORT MSettingModel : public QObject
{
    Q_OBJECT
public:
    MSettingModel();
    QSettings setting;

    MAccountSetting *account;
    MGraphicsSetting *graphic;
    MHotKeySetting *hotKey;
    MInterfaceSetting *interFace;//interface in windows is a special word
    MServerSetting *server;

    QDir dir;
    QString currentLogin;

    void createSetting(QString login);

    friend QDataStream &operator << (QDataStream &d, MSettingModel &s);
    friend QDataStream &operator >> (QDataStream &d, MSettingModel &s);

signals:
    void updateSetting();

public slots:
    void setDefault();
    void saveSetting();
};

#endif // MSETTINGMODEL_H
