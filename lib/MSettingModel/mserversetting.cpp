#include "mserversetting.h"

MServerSetting::MServerSetting() : setting("Gankona", "Munnin")
{

}

void MServerSetting::setDefault()
{
    defaultVersionSave = "1.0";
    isAyRateably = false;

    this->applySetting();
}

void MServerSetting::applySetting()
{
    setting.setValue("Server/defaultVersionSave", defaultVersionSave);
    setting.setValue("Server/isAyRateably", isAyRateably);

    emit saveSetting();
}

QDataStream &operator << (QDataStream &d, MServerSetting &s)
{
    return d;
}

QDataStream &operator >> (QDataStream &d, MServerSetting &s)
{
    return d;
}
