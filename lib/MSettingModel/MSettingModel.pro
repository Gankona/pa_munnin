#-------------------------------------------------
#
# Project created by QtCreator 2016-01-21T20:03:32
#
#-------------------------------------------------

QT       -= gui

TARGET = MSettingModel
TEMPLATE = lib

DEFINES += MSETTINGMODEL_LIBRARY

SOURCES += msettingmodel.cpp \
    maccountsetting.cpp \
    mserversetting.cpp \
    mgraphicssetting.cpp \
    mhotkeysetting.cpp \
    minterfacesetting.cpp

HEADERS += msettingmodel.h \
    maccountsetting.h \
    mserversetting.h \
    mgraphicssetting.h \
    mhotkeysetting.h \
    minterfacesetting.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MSettingModel.pri
