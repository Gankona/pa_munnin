#include "minterfacesetting.h"

MInterfaceSetting::MInterfaceSetting() : setting("Gankona", "Munnin")
{

}

void MInterfaceSetting::setDefault()
{
    language = "rus";
    style = "default";
    volume = 0;
    dateTimeFormat = "hh:MM dd.mm.yyyy";
    firstWeekDay = "mo";
    timeZone = 2;

    this->applySetting();
}

void MInterfaceSetting::applySetting()
{
    setting.setValue("Interface/language", language);
    setting.setValue("Interface/style", style);
    setting.setValue("Interface/volume", volume);
    setting.setValue("Interface/dateTimeFormat", dateTimeFormat);
    setting.setValue("Interface/firstWeekDay", firstWeekDay);
    setting.setValue("Interface/timeZone", timeZone);

    emit saveSetting();
}

QDataStream &operator << (QDataStream &d, MInterfaceSetting &s)
{
    d << s.language << s.style << s.volume << s.dateTimeFormat
      << s.firstWeekDay << s.timeZone;
    return d;
}

QDataStream &operator >> (QDataStream &d, MInterfaceSetting &s)
{
    d >> s.language >> s.style >> s.volume >> s.dateTimeFormat
      >> s.firstWeekDay >> s.timeZone;
    return d;
}
