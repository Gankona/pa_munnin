isEmpty(MSettingModel.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib/MSettingModel/ -lMSettingModel
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib/MSettingModel/ -lMSettingModel

    INCLUDEPATH += $$PWD/../../lib/MSettingModel
    DEPENDPATH  += $$PWD/../../lib/MSettingModel
}
