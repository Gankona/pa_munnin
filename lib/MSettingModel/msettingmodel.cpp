#include "msettingmodel.h"

MSettingModel::MSettingModel() : setting("Gankona", "Munnin")
{
    account = new MAccountSetting;
    graphic = new MGraphicsSetting;
    hotKey = new MHotKeySetting;
    interFace = new MInterfaceSetting;
    server = new MServerSetting;

    this->setDefault();
    account->applySetting();
    graphic->applySetting();
    hotKey->applySetting();
    interFace->applySetting();
    server->applySetting();

    dir.setPath(setting.value("Path/dataPath", "/home").toString());

    QObject::connect(account,   SIGNAL(saveSetting()), this, SLOT(saveSetting()));
    QObject::connect(graphic,   SIGNAL(saveSetting()), this, SLOT(saveSetting()));
    QObject::connect(hotKey,    SIGNAL(saveSetting()), this, SLOT(saveSetting()));
    QObject::connect(interFace, SIGNAL(saveSetting()), this, SLOT(saveSetting()));
    QObject::connect(server,    SIGNAL(saveSetting()), this, SLOT(saveSetting()));
}

void MSettingModel::setDefault()
{
    account->setDefault();
    graphic->setDefault();
    hotKey->setDefault();
    interFace->setDefault();
    server->setDefault();
    emit updateSetting();
}

void MSettingModel::createSetting(QString login)
{
    currentLogin = login;
    QString str = dir.absolutePath() + "/" + currentLogin
            + "/" + currentLogin + ".setting";
    QFile file;
    file.setFileName(str);
    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream;
        stream.setDevice(&file);
        stream >> *this;
        file.close();
    }
    else {
        this->setDefault();
        this->saveSetting();
        this->createSetting(currentLogin);
    }
}

void MSettingModel::saveSetting()
{
    QString str = dir.absolutePath() + "/" + currentLogin
            + "/" + currentLogin + ".setting";
    QFile file;
    file.setFileName(str);
    file.open(QIODevice::WriteOnly);
    QDataStream stream;
    stream.setDevice(&file);
    stream << *this;
    file.close();
    emit updateSetting();
}

QDataStream &operator << (QDataStream &d, MSettingModel &s)
{
    d << *s.account << *s.graphic << *s.hotKey << *s.interFace << *s.server;
    return d;
}

QDataStream &operator >> (QDataStream &d, MSettingModel &s)
{
    d >> *s.account >> *s.graphic >> *s.hotKey >> *s.interFace >> *s.server;
    return d;
}
