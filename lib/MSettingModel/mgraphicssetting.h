#ifndef MGRAPHICSSETTING_H
#define MGRAPHICSSETTING_H


#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QSize>
#include <QtCore/QDataStream>

class MGraphicsSetting : public QObject
{
    Q_OBJECT
public:
    MGraphicsSetting();
    QSettings setting;

    bool isCurrentVisibleMenu;
    bool isCurrentVisibleWindow;
    QSize currentSize;

    bool isFixedSizeWindow;
    bool isChangeSizeRateably;
    bool isMenuVisible;
    bool isAlwaysShowWindow;
    bool isFloatLoginWindow;
    bool isFixedStartWindowSize;
    QSize startFixedWindowSize;
    QSize minimumSize;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, MGraphicsSetting &s);
    friend QDataStream &operator >> (QDataStream &d, MGraphicsSetting &s);

signals:
    void saveSetting();
};


#endif // MGRAPHICSSETTING_H
