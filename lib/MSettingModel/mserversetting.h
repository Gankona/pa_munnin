#ifndef MSERVERSETTING_H
#define MSERVERSETTING_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>

class MServerSetting : public QObject
{
    Q_OBJECT
public:
    MServerSetting();
    QSettings setting;

    QString defaultVersionSave;
    bool isAyRateably;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, MServerSetting &s);
    friend QDataStream &operator >> (QDataStream &d, MServerSetting &s);

signals:
    void saveSetting();
};

#endif // MSERVERSETTING_H
