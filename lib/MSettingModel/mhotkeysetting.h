#ifndef MHOTKEYSETTING_H
#define MHOTKEYSETTING_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDataStream>

class MHotKeySetting : public QObject
{
    Q_OBJECT
public:
    MHotKeySetting();
    QSettings setting;

    void setDefault();
    void applySetting();

    friend QDataStream &operator << (QDataStream &d, MHotKeySetting &s);
    friend QDataStream &operator >> (QDataStream &d, MHotKeySetting &s);

signals:
    void saveSetting();
};

#endif // MHOTKEYSETTING_H
