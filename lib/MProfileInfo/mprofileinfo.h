#ifndef MPROFILEINFO_H
#define MPROFILEINFO_H

#include "mserverinfo.h"

#include <QtCore/qglobal.h>
#include <QtCore/QDateTime>
#include <QtCore/QMap>
#include <QtCore/QObject>

#if defined(MPROFILEINFO_LIBRARY)
#  define MPROFILEINFOSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MPROFILEINFOSHARED_EXPORT Q_DECL_IMPORT
#endif

class MPROFILEINFOSHARED_EXPORT MProfileInfo
{
public:
    explicit MProfileInfo(bool isLoginS = false,
                QString nameS = "", QString loginS = "NoName",
                QString emailS = "", QString passwordS = "",
                QDateTime timer = QDateTime::currentDateTime(),
                QDateTime timeLastComming = QDateTime::currentDateTime(),
                QString server = "");

    bool isLogin;

    QString name;
    QString login;
    QString email;
    QString password;

    MServerInfo *ipPort;
    QList <MServerInfo*> listIpPort;

    QDateTime timeRegistration;
    QDateTime timeLastComming;

    void setDefault();

    friend QDataStream &operator << (QDataStream &s, MProfileInfo & m);
    friend QDataStream &operator >> (QDataStream &s, MProfileInfo & m);
};

#endif // MPROFILEINFO_H
