include($$PWD/../../lib/MServerInfo/MServerInfo.pri)

isEmpty(MProfileInfo.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib/MProfileInfo/ -lMProfileInfo
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib/MProfileInfo/ -lMProfileInfo

    INCLUDEPATH += $$PWD/../../lib/MProfileInfo
    DEPENDPATH += $$PWD/../../lib/MProfileInfo
}
