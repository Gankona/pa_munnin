#include "mprofileinfo.h"

MProfileInfo::MProfileInfo(bool isLogin,
                         QString name, QString login,
                         QString email, QString password,
                         QDateTime timeRegistration,
                         QDateTime timeLastComming, QString server)
    : isLogin(isLogin), name(name), login(login), email(email),
      password(password), timeRegistration(timeRegistration),
      timeLastComming(timeLastComming)
{
    ipPort = new MServerInfo();
    if (server != ""){
        // тут прописываем список серверов
    }
    listIpPort.clear();
}

void MProfileInfo::setDefault()
{
    isLogin = false;
    name = "";
    login = "NoName";
    email = "";
    password = "";
    timeRegistration = QDateTime::currentDateTime();
    timeLastComming = QDateTime::currentDateTime();
}

QDataStream &operator << (QDataStream &s, MProfileInfo & m)
{
    s << m.login << m.name << m.email << m.password << m.timeRegistration
            << m.timeLastComming << m.isLogin << *m.ipPort;
    foreach (MServerInfo *ms, m.listIpPort)
        s << *ms;
    return s;
}

QDataStream &operator >> (QDataStream &s, MProfileInfo & m)
{
    s >> m.login >> m.name >> m.email >> m.password >> m.timeRegistration
            >> m.timeLastComming >> m.isLogin >> *m.ipPort;
    foreach (MServerInfo *ms, m.listIpPort)
        s >> *ms;
    return s;
}
