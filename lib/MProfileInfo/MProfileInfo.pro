#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T21:11:20
#
#-------------------------------------------------

QT       -= gui

TARGET = MProfileInfo
TEMPLATE = lib

DEFINES += MPROFILEINFO_LIBRARY

SOURCES += mprofileinfo.cpp

HEADERS += mprofileinfo.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../MServerInfo/MServerInfo.pri)

DISTFILES += \
    MProfileInfo.pri
