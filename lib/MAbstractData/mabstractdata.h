#ifndef MABSTRACTDATA_H
#define MABSTRACTDATA_H

#include "mdatalist.h"

#include <QtCore/qglobal.h>
#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QDataStream>
#include <QtCore/QDateTime>
#include <QtGui/QPixmap>

#if defined(MABSTRACTDATA_LIBRARY)
#  define MABSTRACTDATASHARED_EXPORT Q_DECL_EXPORT
#else
#  define MABSTRACTDATASHARED_EXPORT Q_DECL_IMPORT
#endif

class MABSTRACTDATASHARED_EXPORT MAbstractData
{
public:
    MAbstractData();

    QPixmap image;
    QString dataKey;
    QString title;
    QString text;
    QString tegs;
    QDateTime createDate;
    QDateTime endDate;
    QList<QDateTime> listDate;
    MDataList dataList;

    Menum repeatTypeData;
    Menum accessTypeData;
    Menum prioriTypeData;
    Menum statusTypeData;
    Menum typeData;
    Menum timerTypeData;

    bool isTextPresent;
    bool isListPresent;
    bool isImagePresent;
    bool isMainDataPresent;

    float important = 0;

    void clear();

    friend bool operator == (MAbstractData &l, MAbstractData &r);
    friend QDataStream &operator >> (QDataStream &s, MAbstractData &m);
    friend QDataStream &operator << (QDataStream &s, MAbstractData &m);
};

#endif // MABSTRACTDATA_H
