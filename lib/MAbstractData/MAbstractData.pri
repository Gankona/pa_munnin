isEmpty(MAbstractData.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib/MAbstractData/ -lMAbstractData
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib/MAbstractData/ -lMAbstractData

    INCLUDEPATH += $$PWD/../../lib/MAbstractData
    DEPENDPATH += $$PWD/../../lib/MAbstractData
}
