#ifndef MDATALIST_H
#define MDATALIST_H

#include "mdataelement.h"

#include <QtCore/QList>
#include <QtCore/QDataStream>

class MDataList
{
public:
    MDataList();

    QList<MDataElement> list;

    friend bool operator != (MDataList &l, MDataList &r);
    friend QDataStream &operator << (QDataStream &s, MDataList &m);
    friend QDataStream &operator >> (QDataStream &s, MDataList &m);

    void addElement(MDataElement e);
    void deleteElement(MDataElement &e);
};

#endif // MDATALIST_H
