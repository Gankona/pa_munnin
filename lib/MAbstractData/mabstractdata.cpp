#include "mabstractdata.h"

MAbstractData::MAbstractData()
{
    clear();
}

void MAbstractData::clear()
{
    title.clear();
    text.clear();
    tegs.clear();
    listDate.clear();

    isTextPresent = false;
    isListPresent = false;
    isImagePresent = false;
    isMainDataPresent = false;
    repeatTypeData = Menum::No;
    accessTypeData = Menum::Private;
    prioriTypeData = Menum::Middle;
}

bool operator == (MAbstractData &l, MAbstractData &r)
{
    if (l.statusTypeData != r.statusTypeData)
        return false;
    if (l.title != r.title)
        return false;
    if (l.tegs != r.tegs)
        return false;
    if (l.createDate != r.createDate)
        return false;

    if (l.isImagePresent != r.isImagePresent)
        return false;
    else if (l.image.isNull() || r.image.isNull())
        return false;

    if (l.isMainDataPresent != r.isMainDataPresent)
        return false;
    else if (l.endDate.isNull() || r.endDate.isNull())
        return false;
    else if (l.endDate != r.endDate)
        return false;

    if (l.isTextPresent != r.isTextPresent)
        return false;
    else if (l.text != r.text)
        return false;

    if (l.isListPresent != r.isListPresent)
        return false;
    else if (l.dataList != r.dataList)
        return false;

    if (l.repeatTypeData != r.repeatTypeData)
        return false;
    else if (l.listDate != r.listDate)
        return false;

    if (l.prioriTypeData != r.prioriTypeData)
        return false;

    if (l.accessTypeData != r.accessTypeData)
        return false;

    if (l.repeatTypeData != r.repeatTypeData)
        return false;

    return true;
}

QDataStream &operator >> (QDataStream &s, MAbstractData &m)
{
    s >> m.title >> m.createDate >> m.tegs >> m.isImagePresent
      >> m.isListPresent  >> m.isMainDataPresent >> m.isTextPresent;
    for (int i = 0; i < 7; i++)
        if (i == m.repeatTypeData)
            s >> i;
    for (int i = 0; i < 7; i++)
        if (i == m.accessTypeData)
            s >> i;
    for (int i = 0; i < 7; i++)
        if (i == m.prioriTypeData)
            s >> i;
    for (int i = 0; i < 7; i++)
        if (i == m.statusTypeData)
            s >> i;
    for (int i = 0; i < 7; i++)
        if (i == m.typeData)
            s >> i;
    for (int i = 0; i < 7; i++)
        if (i == m.timerTypeData)
            s >> i;
    if (m.isImagePresent)
        s >> m.image;
    if (m.isListPresent)
        s >> m.dataList;
    if (m.isTextPresent)
        s >> m.text;
    if (m.isMainDataPresent)
        s >> m.endDate;
    return s;
}

QDataStream &operator << (QDataStream &s, MAbstractData & m)
{
    s << m.title << m.createDate << m.tegs << m.isImagePresent
      << m.isListPresent << m.isMainDataPresent << m.isTextPresent
      << m.repeatTypeData << m.accessTypeData << m.prioriTypeData
      << m.statusTypeData << m.typeData << m.timerTypeData;
    if (m.isImagePresent)
        s << m.image;
    if (m.isListPresent)
        s << m.dataList;
    if (m.isTextPresent)
        s << m.text;
    if (m.isMainDataPresent)
        s << m.endDate;
    return s;
}
