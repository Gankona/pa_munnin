#ifndef MDATAELEMENT_H
#define MDATAELEMENT_H

#include <QtCore/QDataStream>
#include <QtCore/QDateTime>

enum Menum {
    Present, Denaid,
    //status
    Complete, Waiting, Archive,
    //type
    Stick, ToDo, Calendar, Plan,
    //access
    Public, Private, Friendly,
    //priority
    Max, High, Middle, Low, Min,
    //repeat
    No, Daily, Weekly, Weekend, Weekday, Unstatic,
    //viewVariant
    OneView, MosaicView, ListView
};


class MDataElement
{
public:
    MDataElement(QString text = "", bool isComplete = false,
                      bool isDatePresent = false, QDateTime dateTime = QDateTime::currentDateTime());

    QDateTime dateTime;
    QString text;
    bool isComplete;
    bool isDatePresent;

    void clear();
    void complete();
    void notComplete();

    //void operator =(MDataElement &r);
    friend bool operator != (MDataElement &l, MDataElement &r);
    friend bool operator == (MDataElement &l, const MDataElement &r);
    friend QDataStream &operator >> (QDataStream &s, MDataElement &m);
    friend QDataStream &operator << (QDataStream &s, MDataElement &m);
};

#endif // MDATAELEMENT_H
