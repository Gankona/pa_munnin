#include "mdatalist.h"

MDataList::MDataList()
{
    list.clear();
}

void MDataList::addElement(MDataElement e)
{
    list.push_back(e);
}

void MDataList::deleteElement(MDataElement &e)
{
    list.removeOne(e);
}

bool operator != (MDataList &l, MDataList &r)
{
    if (l.list.length() != r.list.length())
        return false;
    for (int i = 0; i < l.list.length(); i++)
        if (&l.list.at(i) != &r.list.at(i))
            return false;
    return true;
}

QDataStream &operator >> (QDataStream &s, MDataList &m)
{
    int kol(0);
    m.list.clear();
    s >> kol;
    for (int i = 0; i < kol; i++){
        MDataElement e;
        s >> e;
        m.list.push_back(e);
    }
    return s;
}

QDataStream &operator << (QDataStream &s, MDataList & m)
{
    s << m.list.length();
    foreach (MDataElement e, m.list)
        s << e;
    return s;
}
