#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T17:55:20
#
#-------------------------------------------------

CONFIG += c++11

TARGET = MAbstractData
TEMPLATE = lib

DEFINES += MABSTRACTDATA_LIBRARY

unix {
    target.path = /usr/lib
    INSTALLS += target
}

HEADERS += \
    mabstractdata.h \
    mdataelement.h \
    mdatalist.h

SOURCES += \
    mabstractdata.cpp \
    mdataelement.cpp \
    mdatalist.cpp

DISTFILES += \
    MAbstractData.pri
