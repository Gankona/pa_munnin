#include "mdataelement.h"

MDataElement::MDataElement(QString text, bool isComplete,
                                     bool isDatePresent, QDateTime dateTime)
{
    this->text = text;
    this->isComplete = isComplete;
    this->isDatePresent = isDatePresent;
    this->dateTime = dateTime;
}

void MDataElement::clear()
{
    text.clear();
    isComplete = isDatePresent = false;
}

/*void MDataElement::operator = (MDataElement &r)
{
    this->isDatePresent = r.isDatePresent;
    if (isDatePresent)
        dateTime = r.dateTime;
    isComplete = r.isComplete;
    text = r.text;
}*/

bool operator != (MDataElement &l, MDataElement &r)
{
    if (l.isDatePresent != r.isDatePresent)
        return true;
    if (l.isDatePresent)
        if (l.dateTime != r.dateTime)
            return true;
    if (l.isComplete != r.isComplete)
        return true;
    if (l.text != r.text)
        return true;
    return false;
}

bool operator == (MDataElement &l, const MDataElement &r)
{
    if (l.isDatePresent != r.isDatePresent)
        return false;
    if (l.isDatePresent)
        if (l.dateTime != r.dateTime)
            return false;
    if (l.isComplete != r.isComplete)
        return false;
    if (l.text != r.text)
        return false;
    return true;
}

QDataStream &operator >> (QDataStream &s, MDataElement &m)
{
    s >> m.isDatePresent;
    if (m.isDatePresent)
        s >> m.dateTime;
    s >> m.isComplete >> m.text;
    return s;
}

QDataStream &operator << (QDataStream &s, MDataElement &m)
{
    s << m.isDatePresent;
    if (m.isDatePresent)
        s << m.dateTime;
    s << m.isComplete << m.text;
    return s;
}

void MDataElement::complete()
{
    isComplete = true;
}

void MDataElement::notComplete()
{
    this->isComplete = false;
}
