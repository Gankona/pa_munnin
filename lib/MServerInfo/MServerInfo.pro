#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T21:11:55
#
#-------------------------------------------------

CONFIG += c++11

QT       -= gui

TARGET = MServerInfo
TEMPLATE = lib

DEFINES += MSERVERINFO_LIBRARY

SOURCES += mserverinfo.cpp

HEADERS += mserverinfo.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MServerInfo.pri
