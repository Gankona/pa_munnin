isEmpty(MServerInfo.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib/MServerInfo/ -lMServerInfo
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib/MServerInfo/ -lMServerInfo

    INCLUDEPATH += $$PWD/../../lib/MServerInfo
    DEPENDPATH  += $$PWD/../../lib/MServerInfo
}
