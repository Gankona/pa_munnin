#ifndef MSERVERINFO_H
#define MSERVERINFO_H

#include <QtCore/qglobal.h>

#include <QtCore/QDataStream>

#if defined(MSERVERINFO_LIBRARY)
#  define MSERVERINFOSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MSERVERINFOSHARED_EXPORT Q_DECL_IMPORT
#endif

class MSERVERINFOSHARED_EXPORT MServerInfo
{
public:
    MServerInfo(QString name = "",
                QString ip = "127.0.0.1",
                int port = 32802,
                int ping = -1);

    QString name;
    QString ip;

    int port;
    int ping;

    friend QDataStream &operator << (QDataStream &s, MServerInfo & m);
    friend QDataStream &operator >> (QDataStream &s, MServerInfo & m);
};

#endif // MSERVERINFO_H
