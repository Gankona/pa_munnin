#include "mserverinfo.h"

MServerInfo::MServerInfo(QString name, QString ip, int port, int ping)
    : name(name), ip(ip), port(port), ping(ping){}

QDataStream &operator << (QDataStream &s, MServerInfo & m)
{
    s << m.name << m.ip << m.port << m.ping;
    return s;
}

QDataStream &operator >> (QDataStream &s, MServerInfo & m)
{
    s >> m.name >> m.ip >> m.port >> m.ping;
    return s;
}
