#include "socketprototype.h"

SocketPrototype::SocketPrototype(QObject *parent) : QTcpSocket(parent)
{
    stream.setDevice(this);

    QObject::connect(this, SIGNAL(connected()), this, SLOT(slotConnect()));
    QObject::connect(this, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    QObject::connect(this, SIGNAL(disconnected()), this, SLOT(slotDisconnected()));
}

void SocketPrototype::slotConnect()
{
    emit signalSetConnectStatus(true);
}

void SocketPrototype::slotDisconnected()
{
    emit signalSetConnectStatus(false);
}

void SocketPrototype::slotReadyRead()
{
    QString var;
    stream >> var;
    qDebug() << "SocketPrototype::slotReadyRead() " << var;

    if (var == "login"){
        bool isCorrect;
        stream >> isCorrect;
        if (isCorrect){
            QVariantList list;
            stream >> list;
            qDebug() << list << list.length();
            emit signalSetLogin(new MProfileInfo(true,
                                             list.at(0).toString(),
                                             list.at(1).toString(),
                                             list.at(2).toString(),
                                             list.at(3).toString(),
                                             list.at(4).toDateTime(),
                                             QDateTime::currentDateTime(),
                                             list.at(5).toString()));
            return;
        }
        else {
            int errorMsg;
            stream >> errorMsg;
            emit signalSetMsg(errorMsg);
            return;
        }
    }
    else if (var == "reg"){
        bool isCorrect;
        stream >> isCorrect;
        if (isCorrect){
            QVariantList list;
            stream >> list;
            qDebug() << list << list.length();
            emit signalSetRegistration(new MProfileInfo(true,
                                             list.at(0).toString(),
                                             list.at(1).toString(),
                                             list.at(2).toString(),
                                             list.at(3).toString(),
                                             list.at(4).toDateTime(),
                                             QDateTime::currentDateTime(),
                                             list.at(5).toString()));
            return;
        }
        else {
            int errorMsg;
            stream >> errorMsg;
            emit signalSetMsg(errorMsg);
            return;
        }
    }
    else if (var == "editProfile"){}
}

void SocketPrototype::getEditProfile(QStringList editProfile)
{
    qDebug() << "SocketPrototype::getEditProfile() " << editProfile;
    buffer.clear();
    buffer = editProfile;
    stream << (QString)"editProfile" << buffer;
}

void SocketPrototype::getLogin(QStringList loginParametr)
{
    qDebug() << "SocketPrototype::getLogin() " << loginParametr;
    buffer.clear();
    buffer = loginParametr;
    stream << (QString)"login" << buffer;
}

void SocketPrototype::getRegistration(QStringList reg)
{
    qDebug() << "SocketPrototype::getRegistration() " << reg;
    buffer.clear();
    buffer = reg;
    stream << (QString)"reg" << buffer;
}
