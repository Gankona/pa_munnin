#ifndef SERVER_H
#define SERVER_H

#include "Server/socketprototype.h"

#include <QtCore/QObject>
#include <QtCore/QSettings>

class Server : public QObject
{
    Q_OBJECT
public:
    Server();

    SocketPrototype *mainSocket;
    QList <SocketPrototype*> socketList;

    bool isMainServerConnect;

private:
    QSettings settings;

    void disconnectFromServer();

signals:
    void signalSetMsg(int);
    void signalSetServerStatus(bool);
    void signalSetLogin(MProfileInfo*, bool);
    void signalSetRegistration(MProfileInfo*);

public slots:
    void slotSetServerStatus(bool result);
    void slotSetLogin(MProfileInfo* profile);
};

#endif // SERVER_H
