#ifndef SOCKETPROTOTYPE_H
#define SOCKETPROTOTYPE_H

#include "mprofileinfo.h"

#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QDataStream>
#include <QtNetwork/QTcpSocket>

class SocketPrototype : public QTcpSocket
{
    Q_OBJECT
public:
    SocketPrototype(QObject *parent = 0);

    QDataStream stream;
    QStringList buffer;

    bool isConnect;

    void getRegistration(QStringList reg);
    void getLogin(QStringList loginParametr);
    void getEditProfile(QStringList editProfile);

signals:
    void signalSetMsg(int);
    void signalSetConnectStatus(bool);

    void signalSetLogin(MProfileInfo*);
    void signalSetRegistration(MProfileInfo*);
    void signalSetEditProfile(bool, QString, QStringList);

public slots:
    void slotConnect();
    void slotReadyRead();
    void slotDisconnected();
};

#endif // SOCKETPROTOTYPE_H
