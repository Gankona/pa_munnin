#include "server.h"

Server::Server() : settings("Gankona", "Munnin")
{
    socketList.clear();
    mainSocket = new SocketPrototype(this);
    mainSocket->connectToHost(
                settings.value("Server/MainServer", "127.0.0.1").toString(), 32802);
    isMainServerConnect = false;

    QObject::connect(mainSocket, SIGNAL(signalSetConnectStatus(bool)),
                     this,       SLOT  (slotSetServerStatus   (bool)));

    QObject::connect(mainSocket, SIGNAL(signalSetMsg(int)),
                     this,       SIGNAL(signalSetMsg(int)));

    QObject::connect(mainSocket, SIGNAL(signalSetLogin(MProfileInfo*)),
                     this,       SLOT  (slotSetLogin  (MProfileInfo*)));

    QObject::connect(mainSocket, SIGNAL(signalSetRegistration(MProfileInfo*)),
                     this,       SIGNAL(signalSetRegistration(MProfileInfo*)));
}

void Server::disconnectFromServer()
{
    //foreach (QTcpSocket, ) {}
}

void Server::slotSetServerStatus(bool result)
{
    qDebug() << "Server::slotSetServerStatus(bool result)";
    isMainServerConnect = result;
}

void Server::slotSetLogin(MProfileInfo *profile)
{
    qDebug() << "Server::slotSetLogin(MProfileInfo *profile)";
    emit signalSetLogin(profile, true);
}
