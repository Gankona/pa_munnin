#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T20:42:41
#
#-------------------------------------------------

CONFIG += c++11

QT       += network

QT       -= gui

TARGET = Model
TEMPLATE = lib

DEFINES += MODEL_LIBRARY

SOURCES += model.cpp \
    Server/server.cpp \
    Server/socketprototype.cpp \
    Local/local.cpp \
    Local/modeldata.cpp \
    Local/modelpathcontrol.cpp \
    Local/modelprofile.cpp

HEADERS += model.h\
    Server/server.h \
    Server/socketprototype.h \
    Local/local.h \
    Local/modeldata.h \
    Local/modelpathcontrol.h \
    Local/modelprofile.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../MServerInfo/MServerInfo.pri)
include($$PWD/../MProfileInfo/MProfileInfo.pri)
include($$PWD/../MAbstractData/MAbstractData.pri)

DISTFILES += \
    Model.pri
