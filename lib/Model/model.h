#ifndef MODEL_H
#define MODEL_H

#include "mabstractdata.h"
#include "mprofileinfo.h"
#include "mserverinfo.h"
#include "Local/local.h"
#include "Server/server.h"

#include <QtCore/QObject>
#include <QtCore/qglobal.h>

#if defined(MODEL_LIBRARY)
#  define MODELSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MODELSHARED_EXPORT Q_DECL_IMPORT
#endif

class MODELSHARED_EXPORT Model : public QObject
{
    Q_OBJECT
public:
    Model();

    void firstStart();

private:
    Local *local;
    Server *server;
    QString returnError(int error);

private slots:
    void slotGetLogout();
    void slotGetLogin(QStringList loginParametr);
    void slotGetEditProfile(QStringList editParametr);
    void slotGetRegistration(QStringList regParametr);

    void slotSetMsg(int error);
    void slotSetRegistration(MProfileInfo *profile);
    void slotSetLogin(MProfileInfo *profile, bool isFromServer);
    void slotSetNetworkStatus(QList <MServerInfo*> networkStatus);

    void slotWriteStick(MAbstractData data);
    void slotEditStick(MAbstractData data);

signals:
    void signalSetMsg(QString, int);
    void signalSetLogin(MProfileInfo*);
    void signalSetNetworkStatus(bool, int);
    void signalSetData(QList<MAbstractData*>);
    void signalGetData(Menum parametr);
};

#endif // MODEL_H
