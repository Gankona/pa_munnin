#include "model.h"

/// ошибки регистрации
/// 0 - Аккаунт не существует
/// 1 - Пароль неверен
/// 2 - Логин уже занят
/// 3 - Не удалось зарегестрировать, причина неизвестна
/// 4 - Связь с сервером отсутствует
/// 5 - Регистрация прошла успешно
///
/// ошибки изменения профиля
/// 10 - Пароли не совпадают
/// 11 - Еmail не совпадает
/// 12 - Имя не совпадает
/// 13 - Прочто что то пошло не так
/// 14 - Если меня видите, значит все работает не так как планировалось
///
/// ошибки авторизации
/// 20 - Не коректно передались параметры, попробуйте еще раз
/// 21 - Пароль неверен
/// 22 - Пользователь с таким именем не найден
/// 23 - Вход в аккаунт успешно выполнен
///
/// удачные действия
/// 30 - успешная регистрация
/// 31 - успешная смена профиля
/// 32 - успешная авторизация

Model::Model()
{
    server = new Server;

    local = new Local;

    QObject::connect(local->profile, SIGNAL(signalSetLogin(int)),
                     this,           SLOT  (slotSetMsg    (int)));

    QObject::connect(local->profile, SIGNAL(signalSetLogin(MProfileInfo*, bool)),
                     this,           SLOT  (slotSetLogin  (MProfileInfo*, bool)));

    QObject::connect(local, SIGNAL(signalSetData(QList<MAbstractData*>)),
                     this,  SIGNAL(signalSetData(QList<MAbstractData*>)));

    QObject::connect(this,  SIGNAL(signalGetData(Menum)),
                     local, SLOT  (slotGetData  (Menum)));

    QObject::connect(server, SIGNAL(signalSetMsg(int)),
                     this,   SLOT  (slotSetMsg    (int)));

    QObject::connect(server, SIGNAL(signalSetLogin(MProfileInfo*, bool)),
                     this,   SLOT  (slotSetLogin  (MProfileInfo*, bool)));

    QObject::connect(server, SIGNAL(signalSetRegistration(MProfileInfo*)),
                     this,   SLOT  (slotSetRegistration  (MProfileInfo*)));
}

void Model::firstStart()
{
    local->profile->readProfile(local->path->appDir);
}

void Model::slotGetEditProfile(QStringList editParametr){}

void Model::slotGetLogin(QStringList loginParametr)
{
    qDebug() << "Model::slotGetLogin() " << loginParametr << server->isMainServerConnect;
    if (server->isMainServerConnect){
        server->mainSocket->getLogin(loginParametr);
    }
    else
        local->profile->slotLogin(loginParametr);
}

void Model::slotGetLogout(){}

void Model::slotGetRegistration(QStringList regParametr)
{
    qDebug() << "Model::slotGetRegistration() " << regParametr << server->isMainServerConnect;
    if (server->isMainServerConnect)
        server->mainSocket->getRegistration(regParametr);
    else
        slotSetMsg(4);
}

void Model::slotSetLogin(MProfileInfo *profile, bool isFromServer)
{
    qDebug() << "void Model::slotSetLogin()" << isFromServer;
    emit signalSetLogin(profile);
    if (isFromServer)
        local->profile->addProfile(profile, local->path->appDir);
    profile->login == "NoName" ? this->slotSetMsg(33)
                               : this->slotSetMsg(32);
    local->data->loginPathSet(local->path->getProfilePath(profile->login));
}

void Model::slotSetNetworkStatus(QList <MServerInfo*> networkStatus){}

void Model::slotSetMsg(int msg)
{
    qDebug() << "Model::slotSetMsg() " << msg << local->returnError(msg);
    emit signalSetMsg(local->returnError(msg), int(msg/10));
}

void Model::slotSetRegistration(MProfileInfo *profile)
{
    qDebug() << "Model::slotSetRegistration()";
    local->profile->addProfile(profile, local->path->appDir);
    this->slotSetMsg(30);
}

void Model::slotWriteStick(MAbstractData data)
{
    local->data->addStick(data, local->path->getProfilePath(local->profile->currentInfo->login));
    qDebug() << "void Model::slotWriteTodo(MAbstractData data)";
}

void Model::slotEditStick(MAbstractData data)
{
    local->data->editStick(data, local->path->getProfilePath(local->profile->currentInfo->login));
    qDebug() << "void Model::slotEditTodo(MAbstractData data)";
}
