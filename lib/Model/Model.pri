include($$PWD/../MServerInfo/MServerInfo.pri)
include($$PWD/../MProfileInfo/MProfileInfo.pri)
include($$PWD/../MAbstractData/MAbstractData.pri)

isEmpty(Model.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib/Model/ -lModel
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib/Model/ -lModel
    INCLUDEPATH += $$PWD/../../lib/Model
    DEPENDPATH += $$PWD/../../lib/Model
}
