#include "local.h"

Local::Local(QObject *parent) : QObject(parent)
{
    profile = new ModelProfile;
    path = new ModelPathControl;
    data = new ModelData;

    QObject::connect(profile, SIGNAL(signalSetLogin(MProfileInfo*, bool)),
                     path,    SLOT(slotSetLoginDir (MProfileInfo*, bool)));
}

QString Local::returnError(int error)
{
    qDebug() << "Local::returnError(int error)";
    switch (error){
    // ошибки регистрации
    case 0:
        return tr("Аккаунт не существует");
    case 1:
        return tr("Пароль неверен");
    case 2:
        return tr("Логин уже занят");
    case 3:
        return tr("Не удалось зарегестрировать, причина неизвестна");

    // ошибки изменения профиля
    case 10:
        return tr("Пароли не совпадают");
    case 11:
        return tr("Еmail не совпадает");
    case 12:
        return tr("Имя не совпадает");
    case 13:
        return tr("Прочто что то пошло не так");
    case 14:
        return tr("Если меня видите, значит все работает не так как планировалось");

    // ошибки авторизации
    case 20:
        return tr("Не коректно передались параметры, попробуйте еще раз");
    case 21:
        return tr("Пароль неверен");
    case 22:
        return tr("Пользователь с таким именем не найден");
    case 23:
        return tr("Данные польщователя удалены с устройства");

    // все прошло успешно
    case 30:
        return tr("Регистрация прошла успешно");
    case 31:
        return tr("Профиль коректно изменен");
    case 32:
        return tr("Вы вошли в учетную запись");
    case 33:
        return tr("Вы вышли с учетной записи, "
                  "теперь действует аккаунт по умолчанию");

    default:
        return "";
    }
}

void Local::slotGetData(Menum parametr)
{
    qDebug() << "Local::slotGetData(Menum parametr)";
    emit signalSetData(data->readParametr(parametr));
}
