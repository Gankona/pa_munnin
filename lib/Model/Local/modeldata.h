#ifndef MODELDATA_H
#define MODELDATA_H

#include "mabstractdata.h"

#include <QtCore/QDir>
#include <QtCore/QFile>

class ModelData
{
public:
    ModelData();

    void loginPathSet(QDir dir);
    void addStick(MAbstractData data, QDir dir);
    void editStick(MAbstractData data, QDir dir);
    QList <MAbstractData*> readAll();
    QList <MAbstractData*> readParametr(Menum parametr);

private:
    QDir path;
    QStringList listFile;
    QList <MAbstractData*> listData;

    void logoutClean();
    void updateFiles();

    void getMAbstractData();
    QString getDataKey(MAbstractData data);
};

#endif // MODELDATA_H
