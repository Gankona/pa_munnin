#ifndef LOCAL_H
#define LOCAL_H

#include <Local/modeldata.h>
#include <Local/modelprofile.h>
#include <Local/modelpathcontrol.h>

#include <QtCore/QObject>

class Local : public QObject
{
    Q_OBJECT
public:
    explicit Local(QObject *parent = 0);

    ModelData *data;
    ModelProfile *profile;
    ModelPathControl *path;

    QString returnError(int error);

signals:
    void signalSetLogin(int);
    void signalSetData(QList<MAbstractData*>);

public slots:
    void slotGetData(Menum parametr);
};

#endif // LOCAL_H
