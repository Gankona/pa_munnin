#include "modelpathcontrol.h"

ModelPathControl::ModelPathControl() : setting("Gankona", "Munnin")
{
    appDir = QDir::home();
    QString nameDir;
#ifdef Q_OS_LINUX
    nameDir = ".pa_munnin";
#else
    nameDir = "pa_munnin";
#endif
    if (! appDir.cd(nameDir)){
        appDir.mkdir(nameDir);
        appDir.cd(nameDir);
    }

    setting.setValue("Path/dataPath", appDir.path());
}

QDir ModelPathControl::getProfilePath(QString str)
{
    QDir dir = appDir;
    if (! dir.cd (str)){
        dir.mkdir(str);
        dir.cd   (str);
    }
    return dir;
}

void ModelPathControl::slotSetLoginDir(MProfileInfo *p, bool)
{
    listLoginDir.clear();
    loginDir = getProfilePath(p->login);
    if (! loginDir.cd (p->login + '_' + p->ipPort->ip/* + '_' + p->ipPort->port*/)){
        loginDir.mkdir(p->login + '_' + p->ipPort->ip/* + '_' + p->ipPort->port*/);
        loginDir.cd   (p->login + '_' + p->ipPort->ip/* + '_' + p->ipPort->port*/);
    }

    foreach (MServerInfo *s, p->listIpPort){
        listLoginDir.push_back(QDir(appDir));
        if (! listLoginDir.last().cd(p->login + '_' + s->ip/* + '_' + s->port*/)){
            listLoginDir.last().mkdir(p->login + '_' + s->ip/* + '_' + s->port*/);
            listLoginDir.last().cd(p->login + '_' + s->ip/* + '_' + s->port*/);
        }
    }
}
