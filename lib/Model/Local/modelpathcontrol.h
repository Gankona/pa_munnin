#ifndef MODELPATHCONTROL_H
#define MODELPATHCONTROL_H

#include "mprofileinfo.h"

#include <QtCore/QDir>
#include <QtCore/QObject>
#include <QtCore/QSettings>

class ModelPathControl : public QObject
{
    Q_OBJECT
public:
    ModelPathControl();
    QSettings setting;

    QDir appDir;
    QDir loginDir;
    QList <QDir> listLoginDir;

    QDir getProfilePath(QString str);

public slots:
    void slotSetLoginDir(MProfileInfo *p, bool);
};

#endif // MODELPATHCONTROL_H
