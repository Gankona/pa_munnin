#ifndef MODELPROFILE_H
#define MODELPROFILE_H

#include "mprofileinfo.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtCore/QDataStream>

class ModelProfile : public QObject
{
    Q_OBJECT
public:     
    ModelProfile();

    MProfileInfo *currentInfo;
    QMap<QString, bool> mapProfile;
    QDir dir;

    bool isEnter;

    void readProfile (QDir dir);
    void writeProfile(QDir dir);
    void addProfile(MProfileInfo *profile, QDir dir);

signals:
    void signalSetLogin(int);
    void signalSetLogin(MProfileInfo*, bool);

public slots:
    void slotLogout(bool isJustLogout);
    void slotLogin(QStringList loginParametr);
    QString slotEditProfile(QStringList editParametr, QDir dir);
};

#endif // MODELPROFILE_H
