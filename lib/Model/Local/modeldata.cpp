#include "modeldata.h"

ModelData::ModelData()
{

}

void ModelData::addStick(MAbstractData data, QDir dir)
{
    data.dataKey = this->getDataKey(data);
    QFile file(dir.path() + '/' + data.dataKey + ".aid");
    file.open(QIODevice::WriteOnly);
    QDataStream stream;
    stream.setDevice(&file);
    stream << data;
    file.close();
}

void ModelData::editStick(MAbstractData data, QDir dir){}

QString ModelData::getDataKey(MAbstractData data)
{
    QString key = "";
    switch (data.typeData){
    case Menum::ToDo : key += 't'; break;
    case Menum::Stick : key += 'k'; break;
    case Menum::Calendar : key += 'c'; break;
    case Menum::Plan : key += 'p'; break;
    default:;
    }
    switch (data.accessTypeData){
    case Menum::Public : key += 'p'; break;
    case Menum::Private : key += 's'; break;
    case Menum::Friendly : key += 'f'; break;
    default:;
    }
    switch (data.statusTypeData){
    case Menum::Complete : key += 'c'; break;
    case Menum::Waiting : key += 'w'; break;
    case Menum::Archive : key += 'a'; break;
    default:;
    }
    switch (data.prioriTypeData){
    case Menum::Max : key += 'b'; break;
    case Menum::High : key += 'h'; break;
    case Menum::Middle : key += 'm'; break;
    case Menum::Low : key += 'l'; break;
    case Menum::Min : key += 'k'; break;
    default:;
    }

    int day = data.createDate.date().toJulianDay() - 2450000;
    key += char(97 + day/(26*26*26));
    day %= 26*26*26;
    key += char(97 + day/(26*26));
    day %= 26*26;
    key += char(97 + day/26);
    key += char(97 + day%26);

    int time = data.createDate.time().hour()*60 + data.createDate.time().minute();
    key += char(97 + time/(26*26));
    time %= 26;
    key += char(97 + time/26);
    key += char(97 + time%26);

    for (int i = 0; i < 5; i++)
        key += char(97 + rand()%26);

    qDebug() << key << " key";
    return key;
}

QList <MAbstractData*> ModelData::readAll()
{
    updateFiles();
    getMAbstractData();
    return listData;
}

QList <MAbstractData*> ModelData::readParametr(Menum parametr)
{
    updateFiles();
    //тут проверка на условие
    getMAbstractData();
    return listData;
}

void ModelData::logoutClean()
{
    listData.clear();
    foreach (MAbstractData* data, listData)
        delete data;
    listData.clear();
}

void ModelData::loginPathSet(QDir dir)
{
    path = dir;
}

inline void ModelData::updateFiles()
{
    QStringList filter;
    filter << "*.aid";
    path.setNameFilters(filter);
    listFile = path.entryList();
    qDebug() << "ModelData::updateFiles()" << listFile;
}

inline void ModelData::getMAbstractData()
{
    qDebug() << "ModelData::getMAbstractData()";
    listData.clear();
    foreach (QString str, listFile){
        QFile file(path.path() + "/" + str);
        if (file.open(QIODevice::ReadOnly)){
            QDataStream stream;
            stream.setDevice(&file);
            listData.push_back(new MAbstractData);
            stream >> *listData.last();
            file.close();
        }
    }
}
