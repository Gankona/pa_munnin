#include "modelprofile.h"

ModelProfile::ModelProfile()
{
    mapProfile.clear();
    currentInfo = new MProfileInfo;
}

void ModelProfile::readProfile(QDir dir)
{
    this->dir = dir;
    QFile file(dir.path() + "/profile.aid");

    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream;
        stream.setDevice(&file);
        stream >> mapProfile;
        file.close();

        foreach (QString s, mapProfile.keys())
            if (mapProfile.value(s) == true){
                QFile file2(dir.path() + "/" + s + "/" + s + ".apd");
                if (file2.open(QIODevice::ReadOnly)){
                    stream.setDevice(&file2);
                    stream >> *currentInfo;
                    emit signalSetLogin(currentInfo, false);
                    file2.close();
                    return;
                }
            }
        this->slotLogout(false);
    }
    else {
        currentInfo->setDefault();
        this->addProfile(currentInfo, dir);
        this->writeProfile(dir);
        this->readProfile(dir);
    }
}

void ModelProfile::writeProfile(QDir dir)
{
    QFile file(dir.path() + "/profile.aid");
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QDataStream stream(&file);
    stream << mapProfile;
    file.close();
}

/// ошибки регистрации
/// 0 - Аккаунт не существует
/// 1 - Пароль неверен
/// 2 - Логин уже занят
/// 3 - Не удалось зарегестрировать, причина неизвестна
///
/// ошибки изменения профиля
/// 10 - Пароли не совпадают
/// 11 - Еmail не совпадает
/// 12 - Имя не совпадает
/// 13 - Прошло что то пошло не так
/// 14 - Если меня видите, значит все работает не так как планировалось
///
/// ошибки авторизации
/// 20 - Не коректно передались параметры, попробуйте еще раз
/// 21 - пароль неверен
/// 22 - Пользователь с таким именем не найден
/// 23 - Данные пользователя удалены с устройства


QString ModelProfile::slotEditProfile(QStringList editParametr, QDir dir)
{
    //тут все подправить, сменил систему авторизации
    /// 0 - editType
    /// 1 - login
    /// 2 - password,     email,     name
    /// 3 - new password, new email, new name

    bool isFind = false;
    /*foreach (MProfileInfo *p, listProfile){
        if ((p->login == editParametr.at(1)) && (p->login != "NoName")){
            isFind = true;
            if (editParametr.at(0) == "password"){
                if (editParametr.at(2) == p->password){
                    p->password = editParametr.at(3);
                    this->writeProfile(dir);
                    return QString(tr("изменения прошли успешно"));
                }
                else
                    return QString(tr("Пароль не совпадает"));
            }
            else if (editParametr.at(0) == "email"){
                if (editParametr.at(2) == p->email){
                    p->email = editParametr.at(3);
                    this->writeProfile(dir);
                    return QString(tr("изменения прошли успешно"));
                }
                else
                    return QString(tr("Email не совпадает"));
            }
            else if (editParametr.at(0) == "name"){
                if (editParametr.at(2) == p->name){
                    p->name = editParametr.at(3);
                    this->writeProfile(dir);
                    return QString(tr("изменения прошли успешно"));
                }
                else
                    return QString(tr("Имя не совпадает"));
            }
            else
                return QString(tr("Что то не так"));
        }
    }
    if (! isFind)
        return QString(tr("Польщоватаель не найден"));*/
    return QString(tr("Если меня видите, значит все работает не так как планировалось"));
}

void ModelProfile::slotLogin(QStringList loginParametr)
{
    /// 0 - login
    /// 1 - password

    if (loginParametr.length() != 2){
        emit signalSetLogin(20);
        return;
    }

    bool isPresent = false;
    foreach (QString s, mapProfile.keys())
        if (s == loginParametr.at(0))
            isPresent = true;

    if (! isPresent){
        emit signalSetLogin(22);
        return;
    }

    QDir dirTemp = dir;
    if (! dir.cd(loginParametr.at(0))){
        emit signalSetLogin(23);
        return;
    }

    QFile file;
    file.setFileName(dir.path() + "/" + loginParametr.at(0) + ".apd");
    if (file.open(QIODevice::ReadOnly)){
        QDataStream stream(&file);
        stream >> *currentInfo;
        if (currentInfo->password == loginParametr.at(1)){
            mapProfile.remove(loginParametr.at(0));
            mapProfile.insert(loginParametr.at(0), true);
            emit signalSetLogin(currentInfo, false);
            this->writeProfile(dirTemp);
        }
        else {
            emit signalSetLogin(21);
            currentInfo->setDefault();
        }
        file.close();
    }
    else {
        emit signalSetLogin(23);
        currentInfo->setDefault();
    }
    dir = dirTemp;
}

void ModelProfile::slotLogout(bool isJustLogout)
{
    bool isChange = false;
    foreach (QString s, mapProfile.keys())
        if (mapProfile.value(s) == true){
            mapProfile.remove(s);
            mapProfile.insert(s, false);
            isChange = true;
        }
    if (isChange)
        this->writeProfile(dir);

    if (isJustLogout)
        this->slotLogin(QStringList{"NoName", ""});
    else {
        currentInfo->setDefault();
        this->addProfile(currentInfo, dir);
    }
}

void ModelProfile::addProfile(MProfileInfo *profile, QDir dir)
{
    bool isPresent = false;
    foreach (QString s, mapProfile.keys())
        if (s == profile->login)
            isPresent = true;
    if (! isPresent){
        mapProfile.insert(profile->login, false);
        this->writeProfile(dir);
    }

    qDebug() << QFile::exists(dir.path() + "/" + profile->login + "/" + profile->login + ".apd");
    qDebug() << dir.path() + "/" + profile->login + "/" + profile->login + ".apd";
    if (! QFile::exists(dir.path() + "/" + profile->login + "/" + profile->login + ".apd")){
        if (! dir.cd(profile->login)){
            dir.mkdir(profile->login);
            dir.cd(profile->login);
        }
        QFile file(dir.path() + "/" + profile->login + ".apd");
        qDebug() << file.open(QIODevice::WriteOnly);
        QDataStream stream;
        stream.setDevice(&file);
        stream << *currentInfo;
        emit signalSetLogin(currentInfo, false);
        file.close();
        return;
    }
}
