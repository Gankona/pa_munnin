#ifndef MSORTBAR_H
#define MSORTBAR_H

#include "mservice.h"

#include <QtCore/qglobal.h>
#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFrame>
#include <QtWidgets/QWidget>

#include <QDebug>

#if defined(MSORTBAR_LIBRARY)
#  define MSORTBARSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MSORTBARSHARED_EXPORT Q_DECL_IMPORT
#endif

enum MSort {AtoZ, ZtoA, Optimal, OlderFirst,
            NewFirst, Priority, DateComplete};


class MSORTBARSHARED_EXPORT MSortBar : public QWidget
{
    Q_OBJECT
public:
    explicit MSortBar(QWidget *parent = 0);

private:
    QPushButton *sort_AtoZ_Button;
    QPushButton *sort_ZtoA_Button;

    QPushButton *sort_Optimal_Button;

    QPushButton *sort_olderFirst_Button;
    QPushButton *sort_newFirst_Button;

    QPushButton *sort_priority_Button;

    QPushButton *sort_dateComplete_Button;

    QGraphicsOpacityEffect *opacity;
    QPushButton *currentButton;
    QHBoxLayout *layout;

signals:
    void signalSort(MSort);

private slots:
    void slotChooseSort();
};

#endif // MSORTBAR_H

