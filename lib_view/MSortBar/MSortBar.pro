#-------------------------------------------------
#
# Project created by QtCreator 2016-01-10T00:29:21
#
#-------------------------------------------------

CONFIG += c++11

QT += widgets
QT -= gui

TARGET = MSortBar
TEMPLATE = lib

DEFINES += MSORTBAR_LIBRARY

SOURCES += msortbar.cpp

HEADERS += msortbar.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
include($$PWD/../../lib_view/MService/MService.pri)

DISTFILES += \
    MSortBar.pri
