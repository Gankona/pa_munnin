#include "msortbar.h"

MSortBar::MSortBar(QWidget *parent) : QWidget(parent)
{
    layout = new QHBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);

    opacity = new QGraphicsOpacityEffect(this);
    opacity->setOpacity(0.8);

    QString trek = ":/Other/Files/Images/Other/";
    sort_Optimal_Button = MService::convertButton(QIcon(trek+"trayIcon.jpg"), tr("Оптимальное распределение"));
    sort_AtoZ_Button    = MService::convertButton(QIcon(trek+"trayIcon.jpg"), tr("Отсортировать по алфавиту"));
    sort_ZtoA_Button    = MService::convertButton(QIcon(trek+"trayIcon.jpg"), tr("Отсортировать против алфавита"));
    sort_priority_Button = MService::convertButton(QIcon(trek+"trayIcon.jpg"), tr("Отсортировать по приоритету"));
    sort_newFirst_Button = MService::convertButton(QIcon(trek+"trayIcon.jpg"), tr("Показывать сначала новые"));
    sort_olderFirst_Button   = MService::convertButton(QIcon(trek+"trayIcon.jpg"), tr("Отсортировать по дате добавления"));
    sort_dateComplete_Button = MService::convertButton(QIcon(trek+"trayIcon.jpg"), tr("Отсортировать по дате окончания выполнения"));

    layout->addWidget(sort_Optimal_Button);
    layout->addWidget(new QFrame);
    layout->addWidget(sort_AtoZ_Button);
    layout->addWidget(sort_ZtoA_Button);
    layout->addWidget(new QFrame);
    layout->addWidget(sort_priority_Button);
    layout->addWidget(new QFrame);
    layout->addWidget(sort_newFirst_Button);
    layout->addWidget(sort_olderFirst_Button);
    layout->addWidget(new QFrame);
    layout->addWidget(sort_dateComplete_Button);

    QObject::connect( sort_Optimal_Button, SIGNAL(released()), this, SLOT(slotChooseSort()));
    QObject::connect( sort_AtoZ_Button,    SIGNAL(released()), this, SLOT(slotChooseSort()));
    QObject::connect( sort_ZtoA_Button,    SIGNAL(released()), this, SLOT(slotChooseSort()));
    QObject::connect( sort_priority_Button, SIGNAL(released()), this, SLOT(slotChooseSort()));
    QObject::connect( sort_newFirst_Button, SIGNAL(released()), this, SLOT(slotChooseSort()));
    QObject::connect( sort_olderFirst_Button,   SIGNAL(released()), this, SLOT(slotChooseSort()));
    QObject::connect( sort_dateComplete_Button, SIGNAL(released()), this, SLOT(slotChooseSort()));
}

void MSortBar::slotChooseSort()
{
    qDebug() << "MSortBar::slotChooseSort()";
    QPushButton *send = (QPushButton*)sender();

    //по алфавиту от А до Я
    if      (send ==    sort_AtoZ_Button)
        emit signalSort(MSort::AtoZ);

    //по алфавиту от Я до А(обратное)
    else if (send ==    sort_ZtoA_Button)
        emit signalSort(MSort::ZtoA);

    //по рейтингу заметок, см. клас MPrototypeStick
    else if (send ==    sort_Optimal_Button)
        emit signalSort(MSort::Optimal);

    //сначала новые
    else if (send ==    sort_newFirst_Button)
        emit signalSort(MSort::NewFirst);

    //сначала старые
    else if (send ==    sort_olderFirst_Button)
        emit signalSort(MSort::OlderFirst);

    //по приоритету
    else if (send ==    sort_priority_Button)
        emit signalSort(MSort::Priority);

    //по дате исполнения
    else if (send ==    sort_dateComplete_Button)
        emit signalSort(MSort::DateComplete);

    //показываем что конкретно выделено
    currentButton->setChecked(false);
    send->setChecked(true);
    currentButton = send;
}
