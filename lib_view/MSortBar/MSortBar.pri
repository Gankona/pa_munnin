include($$PWD/../../lib_view/MService/MService.pri)

isEmpty(MSortBar.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MSortBar/ -lMSortBar
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MSortBar/ -lMSortBar
    INCLUDEPATH += $$PWD/../../lib_view/MSortBar
    DEPENDPATH += $$PWD/../../lib_view/MSortBar
}
