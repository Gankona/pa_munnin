include($$PWD/../../lib_view/MService/MService.pri)

isEmpty(MItemSwitch.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MItemSwitch/ -lMItemSwitch
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MItemSwitch/ -lMItemSwitch
    INCLUDEPATH += $$PWD/../../lib_view/MItemSwitch
    DEPENDPATH += $$PWD/../../lib_view/MItemSwitch
}
