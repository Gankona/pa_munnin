#include "mitemswitch.h"

MItemSwitch::MItemSwitch(QString text, QWidget *parent, int height) : QWidget(parent), text(text)
{
    textLabel = new QLabel(this->text, this);
    yButton = MService::convertButton(tr(""), this);
    nButton = MService::convertButton(tr(""), this);
    this->setFixedHeight(height);

    QObject::connect(yButton, SIGNAL(released()), this, SLOT(slotSetButton()));
    QObject::connect(nButton, SIGNAL(released()), this, SLOT(slotSetButton()));
}

void MItemSwitch::resizeEvent(QResizeEvent *e)
{

}

void MItemSwitch::slotSetButton(){}
