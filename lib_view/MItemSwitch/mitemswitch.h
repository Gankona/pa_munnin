#ifndef MITEMSWITCH_H
#define MITEMSWITCH_H

#include "mservice.h"

#include <QtCore/qglobal.h>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

#if defined(MITEMSWITCH_LIBRARY)
#  define MITEMSWITCHSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MITEMSWITCHSHARED_EXPORT Q_DECL_IMPORT
#endif

class MITEMSWITCHSHARED_EXPORT MItemSwitch : public QWidget
{
    Q_OBJECT

    QPushButton *yButton;
    QPushButton *nButton;
    QLabel *textLabel;
    QString text;

public:
    MItemSwitch(QString text = "", QWidget *parent = nullptr, int height = 25);

    void resizeEvent(QResizeEvent *e);

signals:
    void setButton(bool);

private slots:
    void slotSetButton();
};

#endif // MITEMSWITCH_H
