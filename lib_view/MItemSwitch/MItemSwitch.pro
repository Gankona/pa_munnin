#-------------------------------------------------
#
# Project created by QtCreator 2016-01-23T20:27:34
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = MItemSwitch
TEMPLATE = lib

DEFINES += MITEMSWITCH_LIBRARY

SOURCES += mitemswitch.cpp

HEADERS += mitemswitch.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../lib_view/MService/MService.pri)

DISTFILES += \
    MItemSwitch.pri
