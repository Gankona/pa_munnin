#ifndef MLISTEDIT_H
#define MLISTEDIT_H

#include "mservice.h"
#include "mlistelement.h"
#include "mdatalist.h"

#include <QtCore/qglobal.h>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>

#if defined(MLISTEDIT_LIBRARY)
#  define MLISTEDITSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MLISTEDITSHARED_EXPORT Q_DECL_IMPORT
#endif

class MLISTEDITSHARED_EXPORT MListEdit : public QWidget
{
    Q_OBJECT
public:
    explicit MListEdit(QWidget *parent = 0);

    bool isDatePresent;

    void clear();
    bool isComplete();
    void setDateVisible(bool isVisible);

    void resizeEvent(QResizeEvent *e);

    MDataList getData();

private:
    QPushButton *addNew;
    QList <MListElement*> listElement;

signals:
    void setMaxHeight(int);

private slots:
    void addItem();
    void deleteItem();
};

#endif // MLISTEDIT_H

