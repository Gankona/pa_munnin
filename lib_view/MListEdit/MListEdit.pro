#-------------------------------------------------
#
# Project created by QtCreator 2016-01-10T00:46:24
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = MListEdit
TEMPLATE = lib

DEFINES += MLISTEDIT_LIBRARY

SOURCES += mlistedit.cpp \
    mlistelement.cpp

HEADERS += mlistedit.h\
    mlistelement.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../lib_view/MService/MService.pri)
include($$PWD/../../lib/MAbstractData/MAbstractData.pri)
include($$PWD/../../lib_view/MPushEditLabel/MPushEditLabel.pri)

DISTFILES += \
    MListEdit.pri
