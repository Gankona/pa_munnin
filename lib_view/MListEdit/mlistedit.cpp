#include "mlistedit.h"

MListEdit::MListEdit(QWidget *parent) : QWidget(parent)
{
    addNew = MService::convertButton("+", this);
    addNew->setFixedSize(30, 30);

    resizeEvent(nullptr);

    QObject::connect(addNew, SIGNAL(released()), this, SLOT(addItem()));
}

void MListEdit::clear()
{
    foreach (MListElement *e, listElement)
        delete e;
    listElement.clear();
    resizeEvent(nullptr);
}

void MListEdit::addItem()
{
    listElement.push_back(new MListElement(this));
    listElement.last()->show();
    isDatePresent ? listElement.last()->setDateTimeVisible(true)
                  : listElement.last()->setDateTimeVisible(false);
    //resizeEvent(nullptr);
    this->setFixedHeight((listElement.length() + 1)*30);

    QObject::connect(listElement.last(), SIGNAL(signalDelete()), this, SLOT(deleteItem()));
}

void MListEdit::setDateVisible(bool isVisible)
{
    isDatePresent = isVisible;
    foreach (MListElement *e, listElement)
        isDatePresent ? e->setDateTimeVisible(true)
                      : e->setDateTimeVisible(false);
}

void MListEdit::resizeEvent(QResizeEvent *e)
{
    int w;
    e == nullptr ? w = this->width() : w = e->size().width();
    int i = 0;
    foreach (MListElement *e, listElement) {
       e->setFixedSize(w, 30);
       e->move(0, i*30);
       i++;
    }
    addNew->move(0, i*30);
    emit setMaxHeight((i+1)*30);
}

MDataList MListEdit::getData()
{
    MDataList list;
    foreach (MListElement *e, listElement)
        list.addElement(e->getData());
    return list;
}

void MListEdit::deleteItem()
{
    foreach (MListElement *e, listElement)
        if (e == (MListElement*)sender())
            listElement.removeOne(e);
    this->setFixedHeight((listElement.length() + 1)*30);
    //resizeEvent(nullptr);
}

bool MListEdit::isComplete()
{
    foreach (MListElement *e, listElement)
        if (!e->isComplete())
            return false;
    return true;
}
