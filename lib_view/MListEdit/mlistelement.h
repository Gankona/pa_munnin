#ifndef MLISTELEMENT_H
#define MLISTELEMENT_H

#include "mservice.h"
#include "mpusheditlabel.h"
#include "mdataelement.h"

#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QDateTimeEdit>

class MListElement : public QFrame
{
    Q_OBJECT
public:
    explicit MListElement(QWidget *parent = 0);

    void setDateTimeVisible(bool isVisible);
    bool isComplete();
    MDataElement getData();

private:
    QHBoxLayout *box;
    QPushButton *deleteButton;
    QDateTimeEdit *timeEdit;
    MPushEditLabel *edit;

signals:
    void signalDelete();
};

#endif // MLISTELEMENT_H
