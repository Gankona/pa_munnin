include($$PWD/../../lib_view/MService/MService.pri)
include($$PWD/../../lib/MAbstractData/MAbstractData.pri)
include($$PWD/../../lib_view/MPushEditLabel/MPushEditLabel.pri)

isEmpty(MListEdit.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MListEdit/ -lMListEdit
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MListEdit/ -lMListEdit
    INCLUDEPATH += $$PWD/../../lib_view/MListEdit
    DEPENDPATH += $$PWD/../../lib_view/MListEdit
}
