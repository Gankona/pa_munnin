#include "mlistelement.h"

MListElement::MListElement(QWidget *parent) : QFrame(parent)
{
    this->setFixedHeight(30);
    deleteButton = MService::convertButton("-", nullptr);
    deleteButton->setFixedSize(30, 30);
    timeEdit = new QDateTimeEdit;
    timeEdit->setCalendarPopup(true);
    timeEdit->setCalendarWidget(new QCalendarWidget);
    timeEdit->calendarWidget()->setMinimumDate(QDate::currentDate());
//    timeEdit->setStyleSheet("background: white; color: black;");
    edit = new MPushEditLabel;

    box = new QHBoxLayout(this);
    box->setSpacing(2);
    box->setMargin(0);
    box->addWidget(timeEdit);
    box->addWidget(edit);
    box->addWidget(deleteButton);

    QObject::connect(deleteButton, SIGNAL(released()), this, SIGNAL(signalDelete()));
    QObject::connect(deleteButton, SIGNAL(released()), this, SLOT(deleteLater()));
}

void MListElement::setDateTimeVisible(bool isVisible)
{
    timeEdit->setVisible(isVisible);
}

bool MListElement::isComplete()
{
    return edit->isComplete();
}

MDataElement MListElement::getData()
{
    return MDataElement(edit->result, false, timeEdit->isVisible(), timeEdit->dateTime());
}
