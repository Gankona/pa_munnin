#ifndef MMIDDLESCROLLWIDGET_H
#define MMIDDLESCROLLWIDGET_H

#include <QtCore/qglobal.h>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QGroupBox>

#if defined(MMIDDLESCROLLWIDGET_LIBRARY)
#  define MMIDDLESCROLLWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MMIDDLESCROLLWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MMIDDLESCROLLWIDGETSHARED_EXPORT MMiddleScrollWidget : public /*QScrollArea*/QWidget
{
public:
    MMiddleScrollWidget(QWidget *parent = nullptr);

    //QFrame *contentFrame;
    QScrollArea *contentArea;

private:
    QFrame *leftMargin;
    QFrame *rightMargin;
    QGroupBox *groupBox;
    QHBoxLayout *scrollAreaLayout;
};

#endif // MMIDDLESCROLLWIDGET_H
