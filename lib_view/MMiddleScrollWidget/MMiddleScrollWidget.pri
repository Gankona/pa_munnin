isEmpty(MMiddleScrollWidget.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MMiddleScrollWidget/ -lMMiddleScrollWidget
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MMiddleScrollWidget/ -lMMiddleScrollWidget
    INCLUDEPATH += $$PWD/../../lib_view/MMiddleScrollWidget
    DEPENDPATH += $$PWD/../../lib_view/MMiddleScrollWidget
}
