#include "mmiddlescrollwidget.h"


MMiddleScrollWidget::MMiddleScrollWidget(QWidget *parent)
    : QWidget/*QScrollArea*/(parent)
{
    scrollAreaLayout = new QHBoxLayout;
    scrollAreaLayout->setMargin(0);
    scrollAreaLayout->setSpacing(0);
    leftMargin = new QFrame;
    contentArea = new QScrollArea;
    contentArea->setMouseTracking(true);
    contentArea->setWidgetResizable(true);
    rightMargin = new QFrame;
    groupBox = new QGroupBox;

//    this->setStyleSheet("background: black; color: white");
//    leftMargin->setStyleSheet("background: #202020; color: white");
//    rightMargin->setStyleSheet("background: #202020; color: white");

    scrollAreaLayout->addWidget(leftMargin, 1);
    scrollAreaLayout->addWidget(contentArea, 5);
    scrollAreaLayout->addWidget(rightMargin, 1);
    this/*groupBox*/->setLayout(scrollAreaLayout);
    //this->setWidget(groupBox);
}
