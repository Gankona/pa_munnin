#-------------------------------------------------
#
# Project created by QtCreator 2016-01-22T14:20:15
#
#-------------------------------------------------

CONFIG += c++11

QT       -= gui
QT       += widgets

TARGET = MMiddleScrollWidget
TEMPLATE = lib

DEFINES += MMIDDLESCROLLWIDGET_LIBRARY

SOURCES += mmiddlescrollwidget.cpp

HEADERS += mmiddlescrollwidget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MMiddleScrollWidget.pri
