#include "mpusheditlabel.h"

MPushEditLabel::MPushEditLabel(QWidget *parent) : QFrame(parent)
{
    isHaveTegs = false;

    label = new QLabel;
    label->setFixedHeight(25);
    edit = new QLineEdit;
    edit->setVisible(false);
    edit->setFixedHeight(25);

    box = new QHBoxLayout(this);
    box->addWidget(label);
    box->addWidget(edit);
    box->setSpacing(0);
    box->setMargin(0);
}

void MPushEditLabel::keyPressEvent(QKeyEvent *key)
{
    qDebug() << "MPushEditLabel::keyPressEvent(QKeyEvent *key) " << key;
    if (edit->isVisible()){
        if (key->key() == Qt::Key_Escape){
            edit->clear();
            edit->setVisible(false);
            label->setVisible(true);
            return;
        }
        if (key->key() == Qt::Key_Return){
            QString str = MService::checkDataInput(edit->text(), "");
            if (str == ""){
                if (isHaveTegs){
                    QStringList list = edit->text().split(' ');
                    QString res = "";
                    foreach (QString str, list)
                        res += ('#' + str + ", ");
                    label->setText(res);
                }
                else
                    label->setText(edit->text());
                result = edit->text();
                edit->clear();
                edit->setVisible(false);
                label->setVisible(true);
            }
        }
    }
}

void MPushEditLabel::mouseReleaseEvent(QMouseEvent*)
{
    qDebug() << "MPushEditLabel::mouseReleaseEvent(QMouseEvent*)";
    ! edit->isVisible() ? edit->setVisible(true),  label->setVisible(false), edit->setText(result)
                        : edit->setVisible(false), label->setVisible(true);
}

bool MPushEditLabel::isComplete()
{
    qDebug() << "MPushEditLabel::isComplete()";
    if (label->text().length() != 0)
        return true;
    else
        return false;
}

void MPushEditLabel::setTags(bool tegStatus)
{
    qDebug() << "MPushEditLabel::setTags(bool tegStatus)";
    isHaveTegs = tegStatus;
}

void MPushEditLabel::clear()
{
    qDebug() << "MPushEditLabel::clear()";
    label->clear();
    result.clear();
    edit->clear();
}
