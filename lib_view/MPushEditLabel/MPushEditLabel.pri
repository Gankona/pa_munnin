include($$PWD/../../lib_view/MService/MService.pri)

isEmpty(MPushEditLabel.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MPushEditLabel/ -lMPushEditLabel
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MPushEditLabel/ -lMPushEditLabel
    INCLUDEPATH += $$PWD/../../lib_view/MPushEditLabel
    DEPENDPATH += $$PWD/../../lib_view/MPushEditLabel
}
