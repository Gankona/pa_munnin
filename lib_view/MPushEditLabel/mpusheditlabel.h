#ifndef MPUSHEDITLABEL_H
#define MPUSHEDITLABEL_H

#include "mservice.h"

#include <QtCore/qglobal.h>
#include <QtCore/QDebug>
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QHBoxLayout>

#if defined(MPUSHEDITLABEL_LIBRARY)
#  define MPUSHEDITLABELSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MPUSHEDITLABELSHARED_EXPORT Q_DECL_IMPORT
#endif

class MPUSHEDITLABELSHARED_EXPORT MPushEditLabel : public QFrame
{
public:
    explicit MPushEditLabel(QWidget *parent = 0);
    explicit MPushEditLabel(QString title, QWidget *parent = 0);

    bool isHaveTegs;

    QString result;

    bool isComplete();
    void setTags(bool tegStatus = true);
    void clear();

private:
    QLabel *title;
    QLabel *label;
    QLineEdit *edit;
    QHBoxLayout *box;

    void keyPressEvent(QKeyEvent *key);
    void mouseReleaseEvent(QMouseEvent *);
};

#endif // MPUSHEDITLABEL_H
