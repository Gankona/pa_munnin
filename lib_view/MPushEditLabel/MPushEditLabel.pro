#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T22:57:06
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = MPushEditLabel
TEMPLATE = lib

DEFINES += MPUSHEDITLABEL_LIBRARY

SOURCES += mpusheditlabel.cpp

HEADERS += mpusheditlabel.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../lib_view/MService/MService.pri)

DISTFILES += \
    MPushEditLabel.pri
