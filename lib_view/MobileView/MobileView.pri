isEmpty(MobileView.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MobileView/ -lMobileView
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MobileView/ -lMobileView
    INCLUDEPATH += $$PWD/../../lib_view/MobileView
    DEPENDPATH += $$PWD/../../lib_view/MobileView
}
