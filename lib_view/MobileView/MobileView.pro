#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T23:51:40
#
#-------------------------------------------------

QT       += widgets

TARGET = MobileView
TEMPLATE = lib

DEFINES += MOBILEVIEW_LIBRARY

SOURCES += mobileview.cpp

HEADERS += mobileview.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MobileView.pri
