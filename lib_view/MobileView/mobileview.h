#ifndef MOBILEVIEW_H
#define MOBILEVIEW_H

#include <QtCore/qglobal.h>

#if defined(MOBILEVIEW_LIBRARY)
#  define MOBILEVIEWSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MOBILEVIEWSHARED_EXPORT Q_DECL_IMPORT
#endif

class MOBILEVIEWSHARED_EXPORT MobileView
{

public:
    MobileView();
};

#endif // MOBILEVIEW_H
