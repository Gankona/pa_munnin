#ifndef MPASSWORDSWITCHER_H
#define MPASSWORDSWITCHER_H

#include <QtCore/qglobal.h>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

#if defined(MPASSWORDSWITCHER_LIBRARY)
#  define MPASSWORDSWITCHERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MPASSWORDSWITCHERSHARED_EXPORT Q_DECL_IMPORT
#endif

class MPASSWORDSWITCHERSHARED_EXPORT MPasswordSwitcher : public QLabel
{
public:
    explicit MPasswordSwitcher(QWidget *parent = 0,
                      QLineEdit *lineS = nullptr,
                      QLineEdit *line2S = nullptr);

    explicit MPasswordSwitcher(QLineEdit *lineS = nullptr,
                      QLineEdit *line2S = nullptr);

private:
    QLineEdit *line;
    QLineEdit *line2;

    bool isVisiblePassword;

    void mousePressEvent(QMouseEvent*);
};

#endif // MPASSWORDSWITCHER_H
