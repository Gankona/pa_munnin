#include "mpasswordswitcher.h"

MPasswordSwitcher::MPasswordSwitcher(QWidget *parent, QLineEdit *lineS, QLineEdit *line2S)
    : line(lineS), line2(line2S)
{
    this->setParent(parent);
    isVisiblePassword = false;
    this->setText(" " + tr("Показать пароль"));
}

MPasswordSwitcher::MPasswordSwitcher(QLineEdit *lineS, QLineEdit *line2S)
    : line(lineS), line2(line2S)
{
    isVisiblePassword = false;
    this->setText(" " + tr("Показать пароль"));
}

void MPasswordSwitcher::mousePressEvent(QMouseEvent *)
{
    isVisiblePassword = ! isVisiblePassword;
    if (isVisiblePassword){
        this->setText(" " + tr("Не показывать пароль"));
        if (line != nullptr)
            line->setEchoMode(QLineEdit::Normal);
        if (line2 != nullptr)
            line2->setEchoMode(QLineEdit::Normal);
    }
    else {
        this->setText(" " + tr("оказать пароль"));
        if (line != nullptr)
            line->setEchoMode(QLineEdit::Password);
        if (line2 != nullptr)
            line2->setEchoMode(QLineEdit::Password);
    }
}
