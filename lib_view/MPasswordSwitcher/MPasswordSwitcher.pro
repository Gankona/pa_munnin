#-------------------------------------------------
#
# Project created by QtCreator 2016-01-10T00:41:31
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = MPasswordSwitcher
TEMPLATE = lib

DEFINES += MPASSWORDSWITCHER_LIBRARY

SOURCES += mpasswordswitcher.cpp

HEADERS += mpasswordswitcher.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MPasswordSwitcher.pri
