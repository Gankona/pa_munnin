isEmpty(MPasswordSwitcher.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MPasswordSwitcher/ -lMPasswordSwitcher
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MPasswordSwitcher/ -lMPasswordSwitcher
    INCLUDEPATH += $$PWD/../../lib_view/MPasswordSwitcher
    DEPENDPATH += $$PWD/../../lib_view/MPasswordSwitcher
}
