TEMPLATE = subdirs

SUBDIRS += \
    MMiddleScrollWidget \
    MFloatOpasityWidget \
    MPasswordSwitcher \
    MService \
    MSortBar \
    MPrototypeRight \
    MWidget \
    MPushEditLabel \
    MWritePrototype \
    MPrototypeView \
    MListEdit \
    MobileView \
    DesktopView \
    MItemSwitch
