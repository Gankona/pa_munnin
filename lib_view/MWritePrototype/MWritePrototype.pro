#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T23:59:16
#
#-------------------------------------------------

CONFIG += c++11

QT       -= gui
QT       += widgets

TARGET = MWritePrototype
TEMPLATE = lib

DEFINES += MWRITEPROTOTYPE_LIBRARY

SOURCES += mwriteprototype.cpp \
    mforestrightmenu.cpp

HEADERS += mwriteprototype.h \
    mforestrightmenu.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../lib_view/MMiddleScrollWidget/MMiddleScrollWidget.pri)

DISTFILES += \
    MWritePrototype.pri
