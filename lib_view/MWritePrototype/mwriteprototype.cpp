#include "mwriteprototype.h"

MWritePrototype::MWritePrototype(QWidget *parent) : QWidget(parent)
{
    content = new MMiddleScrollWidget;

    contentFrame = content->contentArea;

    fullPanel = new QFrame;
    fullPanel->setFixedWidth(170);
    shortPanel = new QFrame;
    shortPanel->setFixedWidth(30);

    forestMenu = new MForestRightMenu(this);

    mainLayout = new QHBoxLayout(this);
    mainLayout->addWidget(content);
    mainLayout->addWidget(fullPanel);
    mainLayout->addWidget(shortPanel);
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);

    this->resizeEvent(nullptr);
}

void MWritePrototype::resizeEvent(QResizeEvent *e)
{
    qDebug() << "MWritePrototype::resizeEvent(QResizeEvent *e)";
    int width;
    if (e == nullptr){
        width = this->width();
        return;
    }
    width = e->size().width();
    if (width >= 730){
        fullPanel->setVisible(true);
        shortPanel->setVisible(false);
        forestMenu->setVisible(false);
        contentFrame->setFixedWidth(550);

        emit signalSwitchPanel(true);
    }
    else {
        fullPanel->setVisible(false);
        shortPanel->setVisible(true);
        forestMenu->setVisible(true);
        width -= 30;
        width >= 570 ? contentFrame->setFixedWidth(550):
                       contentFrame->setFixedWidth(width - 20);
        emit signalSwitchPanel(false);
    }
}
