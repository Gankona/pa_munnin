#ifndef MFORESTRIGHTMENU_H
#define MFORESTRIGHTMENU_H

#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtWidgets/QPushButton>

class MForestRightMenu : public QObject
{
    Q_OBJECT
public:
    explicit MForestRightMenu(QWidget *parent = nullptr);

    void setVisible(bool visible);
    QPushButton* addButton(QIcon icon, bool isNewRow = false,
                   QString name = "", QString toolTip = "");
    void setCurrent(QPushButton *sender);
    bool isButtonPresent(QPushButton *button);

private:
    QWidget *parent;
    QList<QList<QPushButton*>> listButton;

    void closeMenu();
    void redrawAll();

signals:
    void signalChooseMenu(QPushButton*);

public slots:
    void slotOpenMenu();
    void slotChooseMenu();
};

#endif // MFORESTRIGHTMENU_H
