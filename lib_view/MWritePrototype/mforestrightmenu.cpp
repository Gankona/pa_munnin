#include "mforestrightmenu.h"

MForestRightMenu::MForestRightMenu(QWidget *parent)
                : parent(parent)
{
    listButton.clear();
}

QPushButton* MForestRightMenu::addButton(QIcon icon, bool isNewRow, QString name,
                                 QString toolTip)
{
    qDebug() << "MForestRightMenu::addButton()";
    if (isNewRow){
        static QList<QPushButton*> list;
        listButton.push_back(list);
    }
    listButton.last().push_back(new QPushButton(icon, name, parent));
    QPushButton *b = listButton.last().last();
    b->setToolTip(toolTip);
    b->setFocusPolicy(Qt::NoFocus);
    b->setFlat(true);
    b->setFixedSize(30, 30);
    QObject::connect(b, SIGNAL(released()), this, SLOT(slotOpenMenu()));
    QObject::connect(b, SIGNAL(released()), this, SLOT(slotChooseMenu()));
    this->closeMenu();
    this->redrawAll();
    return b;
}

bool MForestRightMenu::isButtonPresent(QPushButton *button)
{
    qDebug() << "MForestRightMenu::isButtonPresent(QPushButton *button)";
    foreach (QList<QPushButton*> list, listButton)
        foreach (QPushButton* b, list)
            if (b == button)
                return true;
    return false;
}

void MForestRightMenu::setVisible(bool visible)
{
    qDebug() << "MForestRightMenu::setVisible(bool visible)";
    if (visible)
        foreach (QList<QPushButton*> list, listButton)
            list.first()->setVisible(true);
    else
        foreach (QList<QPushButton*> list, listButton)
            foreach (QPushButton* b, list)
                b->setVisible(false);
    redrawAll();
}

inline void MForestRightMenu::redrawAll()
{
    qDebug() << "MForestRightMenu::redrawAll()";
    int i = 0;
    foreach (QList<QPushButton*> list, listButton){
        int j = 0;
        foreach (QPushButton *b, list){
            j++;
            b->move(parent->width() - 30*j, i*30);
        }
        i++;
    }
}

inline void MForestRightMenu::closeMenu()
{
    qDebug() << "MForestRightMenu::closeMenu()";
    foreach (QList<QPushButton*> list, listButton)
        foreach (QPushButton* b, list)
            b == list.first() ? b->setVisible(true)
                              : b->setVisible(false);
}

void MForestRightMenu::slotOpenMenu()
{
    qDebug() << "MForestRightMenu::slotOpenMenu()";
    foreach (QList<QPushButton*> list, listButton){
        if ((QPushButton*)sender() == list.first()){
            if (! list.last()->isVisible())
                foreach (QPushButton* b, list)
                    b->setVisible(true);
            else
                closeMenu();
        }
        else
            if (list.last()->isVisible())
                foreach (QPushButton* b, list)
                    if (b != list.first())
                        b->setVisible(false);
    }
}

void MForestRightMenu::slotChooseMenu()
{
    qDebug() << "MForestRightMenu::slotChooseMenu()";
    foreach (QList<QPushButton*> list, listButton){
        bool isThisList = false;
        foreach (QPushButton *b, list)
            if (b == (QPushButton*)sender())
                isThisList = true;
        if (isThisList){
            if ((QPushButton*)sender() == list.first())
                return;
            else {
                list.replace(0, (QPushButton*)sender());
                redrawAll();
                emit signalChooseMenu((QPushButton*)sender());
            }
        }
    }
    this->closeMenu();
}

void MForestRightMenu::setCurrent(QPushButton *sender)
{
    qDebug() << "MForestRightMenu::setCurrent(QPushButton *sender)";
    foreach (QList<QPushButton*> list, listButton){
        bool isThisList = false;
        foreach (QPushButton *b, list)
            if (b == sender)
                isThisList = true;
        if (isThisList){
            if (sender == list.first())
                return;
            else
                list.replace(0, sender);
        }
    }
    redrawAll();
}
