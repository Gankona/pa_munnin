include($$PWD/../../lib_view/MMiddleScrollWidget/MMiddleScrollWidget.pri)

isEmpty(MWritePrototype.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MWritePrototype/ -lMWritePrototype
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MWritePrototype/ -lMWritePrototype
    INCLUDEPATH += $$PWD/../../lib_view/MWritePrototype
    DEPENDPATH += $$PWD/../../lib_view/MWritePrototype
}
