#ifndef MWRITEPROTOTYPE_H
#define MWRITEPROTOTYPE_H

#include "mforestrightmenu.h"
#include "mmiddlescrollwidget.h"

#include <QtCore/QDebug>
#include <QtCore/qglobal.h>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>

#if defined(MWRITEPROTOTYPE_LIBRARY)
#  define MWRITEPROTOTYPESHARED_EXPORT Q_DECL_EXPORT
#else
#  define MWRITEPROTOTYPESHARED_EXPORT Q_DECL_IMPORT
#endif

class MWRITEPROTOTYPESHARED_EXPORT MWritePrototype : public QWidget
{
    Q_OBJECT
public:
    explicit MWritePrototype(QWidget *parent = 0);

    QFrame *fullPanel;
    QFrame *contentFrame;
    QList <QLabel*> listTipLabelFull;
    QList <QPushButton*> listButtonFull;
    MForestRightMenu *forestMenu;

    void resizeEvent(QResizeEvent *e);

private:
    MMiddleScrollWidget *content;
    QFrame *shortPanel;
    QHBoxLayout *mainLayout;

signals:
    void signalSwitchPanel(bool isFull);
};

#endif // MWRITEPROTOTYPE_H
