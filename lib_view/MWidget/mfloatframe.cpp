#include "mfloatframe.h"

MFloatFrame::MFloatFrame()
{
    isCollapse = isExpand = false;
}

void MFloatFrame::slotSetCollapse()
{
    qDebug() << "MFloatFrame::slotSetCollapse()";
    if (isExpand)
        return;
    if (size().height() <= 4){
        setVisible(false);
        setFixedHeight(0);
        isCollapse = false;
    }
    else {
        if (!isCollapse)
            isCollapse = true;
        QTimer::singleShot(20, this, SLOT(slotSetCollapse()));
        setFixedHeight(size().height() - 4);
    }
}

void MFloatFrame::slotSetExpand()
{
    qDebug() << "MFloatFrame::slotSetExpand()";
    if (isCollapse)
        return;
    if (!isVisible())
        setVisible(true);
    if (size().height() > maxHeight){
        setFixedHeight(maxHeight);
        isExpand = false;
    }
    else {
        QTimer::singleShot(20, this, SLOT(slotSetExpand()));
        setFixedHeight(size().height() + 4);
        if (!isExpand)
            isExpand = true;
    }
}

void MFloatFrame::slotSetMaxHeight(int max)
{
    qDebug() << "MFloatFrame::slotSetMaxHeight(int max)";
    slotSetExpand();
    maxHeight = max;
}
