#include "mwidget.h"

MWidget::MWidget(QString name, bool isVisibleAddDelete)
    : isFree(isVisibleAddDelete)
{
    hPrototypeBox = new QHBoxLayout;
    hPrototypeBox->setSpacing(0);
    hPrototypeBox->setMargin(0);

    vPrototypeBox = new QVBoxLayout(this);
    vPrototypeBox->setSpacing(2);
    vPrototypeBox->setMargin(2);

    isExpand = true;

    contentLabel = new MFloatFrame;
    contentLabel->slotSetMaxHeight(200);

    nameLabel = new QLabel(" " + name);
    nameLabel->setFixedHeight(25);

    addDeleteButton = new QPushButton("+");
    addDeleteButton->setFlat(true);
    addDeleteButton->setFixedSize(25, 25);
    addDeleteButton->setFocusPolicy(Qt::NoFocus);
    addDeleteButton->setVisible(false);

    expandButton = new QPushButton;
    expandButton->setFlat(true);
    expandButton->setFixedSize(25, 25);
    expandButton->setFocusPolicy(Qt::NoFocus);

    hPrototypeBox->addWidget(nameLabel);
    hPrototypeBox->addWidget(addDeleteButton);
    hPrototypeBox->addWidget(expandButton);

    vPrototypeBox->addLayout(hPrototypeBox);
    vPrototypeBox->addWidget(contentLabel);
//    this->setStyleSheet("background: #101010; color: white");

    QObject::connect(expandButton, SIGNAL(pressed()), this, SLOT(slotCollapseExpand()));
    QObject::connect(addDeleteButton, SIGNAL(released()), this, SLOT(slotSetAddDelete()));
    this->setExpandIcon();

    if (isVisibleAddDelete)
        this->setVisibleAddDelete(true);
    contentLabel->slotSetCollapse();
}

void MWidget::setExpandIcon()
{
    qDebug() << "MWidget::setExpandIcon()";
    if (isExpand)
        expandButton->setIcon(QIcon("://images/expand.png"));
    else
        expandButton->setIcon(QIcon("://images/expand2.png"));
}

bool MWidget::isFreePrototype()
{
    qDebug() << "MWidget::isFreePrototype()";
    return isFree;
}

void MWidget::slotCollapseExpand()
{
    qDebug() << "MWidget::slotCollapseExpand()";
    contentLabel->height() > 10 ? isExpand = false : isExpand = true;
    if (isExpand){
        if(isFree)
            return;
        else
            contentLabel->slotSetExpand();
    }
    else
        contentLabel->slotSetCollapse();
    setExpandIcon();
}

inline void MWidget::setVisibleAddDelete(bool visible)
{
    qDebug() << "MWidget::setVisibleAddDelete(bool visible)";
    addDeleteButton->setVisible(visible);
}

void MWidget::slotSetAddDelete()
{
    qDebug() << "MWidget::slotSetAddDelete()";
    isFree ? isFree = false : isFree = true;
    isFree ? addDeleteButton->setText("+")
           : addDeleteButton->setText("-");
    emit changeContentStatus(isFree);
}

void MWidget::clear()
{
    qDebug() << "MWidget::clear()";
    isFree = true;
}
