#ifndef MWIDGET_H
#define MWIDGET_H

#include <mfloatframe.h>

#include <QtCore/qglobal.h>
#include <QtGui/QPixmap>
#include <QtCore/QTimer>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>

#if defined(MWIDGET_LIBRARY)
#  define MWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MWIDGETSHARED_EXPORT MWidget : public QWidget
{
    Q_OBJECT
public:
    MWidget(QString name, bool isVisibleAddDelete = false);

    MFloatFrame *contentLabel;
    QPixmap image;//зачем это тут?

    void clear();
    bool isFreePrototype();

private:
    QLabel *nameLabel;
    QPushButton *addDeleteButton;
    QPushButton *expandButton;

    QHBoxLayout *hPrototypeBox;
    QVBoxLayout *vPrototypeBox;

    bool isExpand;
    bool isFree;

    void setExpandIcon();
    void setVisibleAddDelete(bool visible);

signals:
    void changeContentStatus(bool);

public slots:
    void slotCollapseExpand();
    void slotSetAddDelete();
};

#endif // MWIDGET_H
