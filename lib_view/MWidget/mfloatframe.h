#ifndef MFLOATFRAME_H
#define MFLOATFRAME_H

#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtWidgets/QFrame>
#include <QDebug>

class MFloatFrame : public QFrame
{
    Q_OBJECT
public:
    explicit MFloatFrame();

private:
    int maxHeight;
    bool isCollapse;
    bool isExpand;

public slots:
    void slotSetCollapse();
    void slotSetExpand();
    void slotSetMaxHeight(int max);
};

#endif // MFLOATFRAME_H
