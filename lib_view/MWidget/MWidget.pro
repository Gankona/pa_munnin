#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T22:39:24
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = MWidget
TEMPLATE = lib

DEFINES += MWIDGET_LIBRARY

SOURCES += mwidget.cpp \
    mfloatframe.cpp

HEADERS += mwidget.h\
    mfloatframe.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    resources.qrc

DISTFILES += \
    MWidget.pri
