isEmpty(MWidget.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MWidget/ -lMWidget
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MWidget/ -lMWidget
    INCLUDEPATH += $$PWD/../../lib_view/MWidget
    DEPENDPATH += $$PWD/../../lib_view/MWidget
}
