#ifndef MFLOATOPASITYWIDGET_H
#define MFLOATOPASITYWIDGET_H

#include <QtCore/qglobal.h>
#include <QtCore/QDebug>
#include <QtCore/QPropertyAnimation>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsOpacityEffect>

#if defined(MFLOATOPASITYWIDGET_LIBRARY)
#  define MFLOATOPASITYWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MFLOATOPASITYWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

class MFLOATOPASITYWIDGETSHARED_EXPORT MFloatOpasityWidget : public QFrame
{
    Q_OBJECT
public:
    explicit MFloatOpasityWidget(QWidget *parent = nullptr);

    void deleteWidget();

private:
    QGraphicsOpacityEffect *opacity;

public slots:
    void showWidget(QPoint point);
};

#endif // MFLOATOPASITYWIDGET_H
