#-------------------------------------------------
#
# Project created by QtCreator 2016-01-10T01:44:19
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

QT       -= gui

TARGET = MFloatOpasityWidget
TEMPLATE = lib

DEFINES += MFLOATOPASITYWIDGET_LIBRARY

SOURCES += mfloatopasitywidget.cpp

HEADERS += mfloatopasitywidget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MFloatOpasityWidget.pri
