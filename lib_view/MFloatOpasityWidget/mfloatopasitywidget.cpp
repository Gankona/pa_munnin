#include "mfloatopasitywidget.h"

MFloatOpasityWidget::MFloatOpasityWidget(QWidget *parent) : QFrame(parent)
{
    opacity = new QGraphicsOpacityEffect(this);
    opacity->setOpacity(0);
    this->setGraphicsEffect(opacity);
//    this->setStyleSheet("background: black; color: white;");
}

void MFloatOpasityWidget::showWidget(QPoint point)
{
    qDebug() << point << parent();
    this->move(point);
    this->show();
    this->raise();
    QPropertyAnimation *animation = new QPropertyAnimation(opacity, "opacity");
    animation->setDuration(700);
    animation->setStartValue(0);
    animation->setEndValue(0.8);
    animation->start();
}

void MFloatOpasityWidget::deleteWidget()
{
    QPropertyAnimation *animation = new QPropertyAnimation(opacity, "opacity");
    animation->setDuration(500);
    animation->setStartValue(0.8);
    animation->setEndValue(0);
    animation->start();

    QObject::connect(animation, SIGNAL(finished()), this, SLOT(deleteLater()));
}
