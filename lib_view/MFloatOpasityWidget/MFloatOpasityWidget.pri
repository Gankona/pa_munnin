isEmpty(MFloatOpasityWidget.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MFloatOpasityWidget/ -lMFloatOpasityWidget
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MFloatOpasityWidget/ -lMFloatOpasityWidget
    INCLUDEPATH += $$PWD/../../lib_view/MFloatOpasityWidget
    DEPENDPATH += $$PWD/../../lib_view/MFloatOpasityWidget
}
