include($$PWD/../../lib_view/MService/MService.pri)
include($$PWD/../../lib_view/MSortBar/MSortBar.pri)

isEmpty(MPrototypeRight.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MPrototypeRight/ -lMPrototypeRight
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MPrototypeRight/ -lMPrototypeRight
    INCLUDEPATH += $$PWD/../../lib_view/MPrototypeRight
    DEPENDPATH += $$PWD/../../lib_view/MPrototypeRight
}
