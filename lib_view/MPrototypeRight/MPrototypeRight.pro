#-------------------------------------------------
#
# Project created by QtCreator 2016-01-10T01:40:36
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = MPrototypeRight
TEMPLATE = lib

DEFINES += MPROTOTYPERIGHT_LIBRARY

SOURCES += mprototyperight.cpp

HEADERS += mprototyperight.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../lib_view/MService/MService.pri)
include($$PWD/../../lib_view/MSortBar/MSortBar.pri)

DISTFILES += \
    MPrototypeRight.pri
