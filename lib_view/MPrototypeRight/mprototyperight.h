#ifndef MPROTOTYPERIGHT_H
#define MPROTOTYPERIGHT_H

#include "msortbar.h"

#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

#if defined(MPROTOTYPERIGHT_LIBRARY)
#  define MPROTOTYPERIGHTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MPROTOTYPERIGHTSHARED_EXPORT Q_DECL_IMPORT
#endif

class MPROTOTYPERIGHTSHARED_EXPORT MPrototypeRight : public QFrame
{
    Q_OBJECT
public:
    MPrototypeRight(QWidget *parent = nullptr);

    QLabel *titleLabel;
    QWidget *contentFrame;
    QList <QPushButton*> listLeftButton;
    MSortBar *sortPanel;

    void updateLeftPanel();
    void resize(QSize size);
    void setSortPanel();

private:
    QPushButton *openMenuButton;

signals:
    void signalSetCurrentWidget(int);
    void signalSort(MSort);

public slots:
    void slotOpenMenu();
    void slotSetWidget();
    void slotSetCurrentWidget(int i);
};

#endif // MPROTOTYPERIGHT_H
