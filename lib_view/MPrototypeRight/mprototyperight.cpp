#include "mprototyperight.h"

MPrototypeRight::MPrototypeRight(QWidget *parent) : QFrame(parent)
{
    listLeftButton.clear();

    titleLabel = new QLabel(this);
    titleLabel->setFixedHeight(30);

    contentFrame = new QWidget(this);
//    contentFrame->setStyleSheet("background: yellow");

    sortPanel = nullptr;
    openMenuButton = nullptr;
}

void MPrototypeRight::updateLeftPanel()
{
    foreach (QPushButton* b, listLeftButton)
       QObject::connect(b,    SIGNAL(released()),
                        this, SLOT  (slotSetWidget()));
    this->resize(this->size());
}

void MPrototypeRight::slotSetWidget()
{
    for (int i = 0; i < listLeftButton.length(); i++)
        if (listLeftButton.at(i) == (QPushButton*)sender())
            slotSetCurrentWidget(i);
}

void MPrototypeRight::slotSetCurrentWidget(int i)
{
    emit signalSetCurrentWidget(i);
    for (int j = 0; j < listLeftButton.length(); j++)
        (i == j) ? listLeftButton.at(j)->setChecked(true),
                   titleLabel->setText(listLeftButton.at(j)->toolTip())
                 : listLeftButton.at(j)->setChecked(false);
}

void MPrototypeRight::resize(QSize size)
{
    if (sortPanel != nullptr){
        if (sortPanel->isVisible()){
            sortPanel->move(30, 0);
            sortPanel->setFixedSize(size.width() - 30, 30);
            openMenuButton->move(0, 0);
        }
    }
    titleLabel->setFixedSize(size.width() - 30, 30);
    titleLabel->move(30, 0);
    contentFrame->setFixedSize(size - QSize(30, 30));
    contentFrame->move(30, 30);
    for (int i = 0; i < listLeftButton.length(); i++){
        listLeftButton.at(i)->move(0, 30*(i+1));
        listLeftButton.at(i)->setFixedSize(30, 30);
    }
}

void MPrototypeRight::setSortPanel()
{
    qDebug() << "MPrototypeRight::setSortPanel 1";
    sortPanel = new MSortBar(this);
    sortPanel->setVisible(false);
    qDebug() << "MPrototypeRight::setSortPanel 2";
    openMenuButton = MService::convertButton(QIcon(""), tr("Меню"), this);
    qDebug() << "MPrototypeRight::setSortPanel 3";
    QObject::connect(openMenuButton, SIGNAL(released()),
                     this, SLOT(slotOpenMenu()));
    qDebug() << "MPrototypeRight::setSortPanel 4";
    QObject::connect(sortPanel, SIGNAL(signalSort(MSort)),
                     this,      SIGNAL(signalSort(MSort)));
    qDebug() << "MPrototypeRight::setSortPanel 5";
    //this->resizeEvent(nullptr);
    this->resize(this->size());
    qDebug() << "MPrototypeRight::setSortPanel 6";
}

void MPrototypeRight::slotOpenMenu()
{
    if (sortPanel != nullptr)
        sortPanel->isVisible() ? sortPanel->setVisible(false),
                                  titleLabel->setVisible(true)
                               : sortPanel->setVisible(true),
                                  titleLabel->setVisible(false);
    else
        titleLabel->isVisible() ? titleLabel->setVisible(false)
                                : titleLabel->setVisible(true);
}
