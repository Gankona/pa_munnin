include($$PWD/../../lib_view/MService/MService.pri)
include($$PWD/../../lib_view/MSortBar/MSortBar.pri)
include($$PWD/../../lib_view/MWidget/MWidget.pri)
include($$PWD/../../lib/MAbstractData/MAbstractData.pri)

isEmpty(MPrototypeView.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MPrototypeView/ -lMPrototypeView
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MPrototypeView/ -lMPrototypeView
    INCLUDEPATH += $$PWD/../../lib_view/MPrototypeView
    DEPENDPATH += $$PWD/../../lib_view/MPrototypeView
}
