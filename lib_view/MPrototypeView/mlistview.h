#ifndef MLISTVIEW_H
#define MLISTVIEW_H

#include "mservice.h"
#include "mwidget.h"
#include "mabstractdata.h"

#include <QtCore/QMap>
#include <QtWidgets/QGridLayout>

struct LabelLabelButton{
    QLabel *date;
    QLabel *text;
    QPushButton *button;
};

class MListView : public MWidget
{
    Q_OBJECT
public:
    MListView(MDataList *list);

    MDataList *listData;

private:
    QList <LabelLabelButton*> listElement;
    QLabel *upLabel;
    QGridLayout *grid;

    bool isDatePresent;

signals:
    void signalComplete(MDataList*);

public slots:
    void slotCompleteSomeFromList();
};

#endif // MLISTVIEW_H
