#-------------------------------------------------
#
# Project created by QtCreator 2016-01-10T00:20:32
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = MPrototypeView
TEMPLATE = lib

DEFINES += MPROTOTYPEVIEW_LIBRARY

SOURCES += mprototypeview.cpp \
    mprototypestick.cpp \
    mlistview.cpp

HEADERS += mprototypeview.h \
    mprototypestick.h \
    mlistview.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../lib_view/MService/MService.pri)
include($$PWD/../../lib_view/MSortBar/MSortBar.pri)
include($$PWD/../../lib_view/MWidget/MWidget.pri)
include($$PWD/../../lib/MAbstractData/MAbstractData.pri)

RESOURCES += \
    resources.qrc

DISTFILES += \
    MPrototypeView.pri
