#include "mlistview.h"

MListView::MListView(MDataList *list)
    : MWidget(tr("Список"))
{
    listData = list;
    grid = new QGridLayout(contentLabel);
    grid->setMargin(0);
    grid->setSpacing(0);
    if (listData->list.length() == 0){
        this->deleteLater();
        return;
    }
    else if (listData->list.first().isDatePresent){
        upLabel = new QLabel(tr("Выполнить до:"));
        upLabel->setAlignment(Qt::AlignLeft);
        grid->addWidget(upLabel, 1, 1, 1, 3);
    }

    int i = 1;
    foreach (MDataElement element, listData->list){
        i++;
        listElement.push_back(new LabelLabelButton);
        if (element.isDatePresent){
            listElement.last()->date = new QLabel(element.dateTime.time().toString("hh:mm") + "\n "
                                                + element.dateTime.date().toString("dd.MM yyyy"));
            grid->addWidget(listElement.last()->date, i, 1);
        }
        listElement.last()->text = new QLabel(element.text);
        listElement.last()->text->setAlignment(Qt::AlignLeft);
        element.isComplete ? listElement.last()->button
                              = MService::convertButton(QIcon("://images/fileAdd.png"), tr("выполнить"))
                            : listElement.last()->button
                              = MService::convertButton(QIcon("://images/info.png"), tr("отменить"));
        grid->addWidget(listElement.last()->text, i, 2);
        grid->addWidget(listElement.last()->button, i, 3);
    }
}

void MListView::slotCompleteSomeFromList()
{
    int i(0);
    foreach (LabelLabelButton *l, listElement){
        if (l->button == (QPushButton*)sender()){
            //listData->list.at(i).complete();
            emit signalComplete(listData);
            return;
        }
        i++;
    }
}
