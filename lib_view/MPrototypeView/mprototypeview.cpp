#include "mprototypeview.h"

MPrototypeView::MPrototypeView(QWidget *parent) : QWidget(parent), settings("Gankona", "Munnin")
{
    qDebug() << "MPrototypeView::MPrototypeView";

    foreach (MPrototypeStick *s, listStick)
        delete s;
    foreach (MAbstractData *i, listData)
        delete i;
    listData.clear();
    listStick.clear();

    shortRightPanel = new QWidget(this);
    fullRightPanel = new QWidget(this);
    contentWidget = new QWidget(this);
    contentWidget->show();
    shortListButton.clear();
    fullListButton.clear();

    sort = MSort(settings.value("View/Sort", MSort::Optimal).toInt());
    viewType = Menum(settings.value("View/ViewType", Menum::ListView).toInt());

    this->slotSetSort(sort);
    this->slotSetView(viewType);
}

void MPrototypeView::slotSetSort(MSort sort)
{
    qDebug() << "MPrototypeView::slotSetSort(MSort sort)";
    if (this->sort == sort)
        return;
    this->sort = sort;
    settings.setValue("View/Sort", sort);
    switch (sort){
    case MSort::AtoZ:
        for (int i = 0; i < listStick.length(); i++)
            for (int j = i; j < listStick.length(); j++)
                if (listStick.at(j)->data->title[0] >
                        listStick.at(i)->data->title[0])
                    listStick.replace(i, listStick.at(j));
        break;
    case MSort::ZtoA:
        for (int i = 0; i < listStick.length(); i++)
            for (int j = i; j < listStick.length(); j++)
                if (listStick.at(j)->data->title[0] <
                        listStick.at(i)->data->title[0])
                    listStick.replace(i, listStick.at(j));
        break;
    case MSort::Optimal:
        break;
    case MSort::OlderFirst:
        for (int i = 0; i < listStick.length(); i++)
            for (int j = i; j < listStick.length(); j++)
                if (listStick.at(j)->data->createDate >
                        listStick.at(i)->data->createDate)
                    listStick.replace(i, listStick.at(j));
        break;
    case MSort::NewFirst:
        for (int i = 0; i < listStick.length(); i++)
            for (int j = i; j < listStick.length(); j++)
                if (listStick.at(j)->data->createDate <
                        listStick.at(i)->data->createDate)
                    listStick.replace(i, listStick.at(j));
        break;
    case MSort::Priority:
        break;
    case MSort::DateComplete:
        break;
    default:;
    }

    resizeEvent(nullptr);
}

void MPrototypeView::slotSetView(Menum view)
{
    qDebug() << "MPrototypeView::slotSetView(Menum view)";
    if (view == viewType)
        return;
    viewType = view;
    settings.setValue("View/ViewType", view);
    resizeEvent(nullptr);
}

void MPrototypeView::slotSetData(QList<MAbstractData *> list)
{
    qDebug() << "MPrototypeView::slotSetData";
    foreach (MAbstractData *i, listData)
        delete i;
    listData.clear();
    qDebug() << list.length() << listData.length();
    foreach (MAbstractData *i, list)
        listData.push_back(i);
    deleteContent();
    resizeEvent(nullptr);
}

void MPrototypeView::keyPressEvent(QKeyEvent *e)
{

}

void MPrototypeView::resizeEvent(QResizeEvent *e)
{
    qDebug() << "MPrototypeView::resizeEvent(QResizeEvent *e)";
    QSize size;
    e == nullptr ? size = this->size()
                 : size = e->size();
    int h = 0;
    int panelW = 0;
    int contentW = 0;
    int countStick = 0;
    switch (viewType){
    case Menum::OneView :{
        if (size.width() > 600){
            fullRightPanel->setVisible(true);
            shortRightPanel->setVisible(false);
            panelW = 120;
        }
        else {
            fullRightPanel->setVisible(false);
            shortRightPanel->setVisible(true);
            panelW = 30;
        }
        contentW = size.width() - (panelW + 20);
    }
        break;
    case Menum::ListView :{
        if (size.width() > 600){
            fullRightPanel->setVisible(true);
            shortRightPanel->setVisible(false);
            panelW = 120;
        }
        else {
            fullRightPanel->setVisible(false);
            shortRightPanel->setVisible(true);
            panelW = 30;
        }
        contentW = size.width() - (panelW + 20);
        foreach (MPrototypeStick *s, listStick){
            countStick++;
            s->move(10, h);
            s->setFixedSize(contentW, s->getMinimalSize().height());
            h += s->getMinimalSize().height();
            qDebug() << "s->getMinimalSize().height() " << s->getMinimalSize().height() << s->pos();
            h += 10;
            fullListButton.at(countStick-1)->move(size.width() - 120, (countStick-1)*30);
            shortListButton.at(countStick-1)->move(size.width() - 30, (countStick-1)*30);
        }
        contentWidget->setFixedSize(contentW, h+20);
        qDebug() << "h = " << h;
    }
        break;
    case Menum::MosaicView :{
        fullRightPanel->setVisible(false);
        shortRightPanel->setVisible(false);
    }
        break;
    default:;
    }
}

inline void MPrototypeView::deleteContent()
{
    qDebug() << "MPrototypeView::deleteContent()";
    //если это новое то очищаем что было
    //и перезаписываем на новое
    foreach (MPrototypeStick *s, listStick)
        delete s;
    foreach (QPushButton *b, shortListButton)
        delete b;
    foreach (QPushButton *b, fullListButton)
        delete b;
    listStick.clear();
    shortListButton.clear();
    fullListButton.clear();
    foreach (MAbstractData *d, listData){
        listStick.push_back(new MPrototypeStick(d, contentWidget));
        shortListButton.push_back(MService::convertButton
                                  (QString::number(currentStick),
                                   shortRightPanel));
        fullListButton.push_back(MService::convertButton
                                  (QString::number(currentStick)
                                   + d->title,
                                    fullRightPanel));
        fullListButton.last()->setFixedWidth(120);
        QObject::connect(fullListButton.last(), SIGNAL(released()),
                         this, SLOT(slotClickToButton()));
        QObject::connect(shortListButton.last(), SIGNAL(released()),
                         this, SLOT(slotClickToButton()));
    }
}

void MPrototypeView::slotClickToButton()
{
    qDebug() << "MPrototypeView::slotClickToButton()";
    int current = -1;
    for (int i = 0; i < fullListButton.length(); i++)
        if (fullListButton.at(i) == (QPushButton*)sender())
            current = i;
    for (int i = 0; i < shortListButton.length(); i++)
        if (shortListButton.at(i) == (QPushButton*)sender())
            current = i;
    if (current == -1)
        return;
    shortListButton.at(currentStick)->setChecked(false);
    fullListButton.at(currentStick)->setChecked(false);
    fullListButton.at(current)->setChecked(true);
    shortListButton.at(current)->setChecked(true);
    currentStick = current;
    contentWidget->move(10, listStick.at(current)->y());
}
