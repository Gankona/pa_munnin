#ifndef MPROTOTYPESTICK_H
#define MPROTOTYPESTICK_H

#include "mservice.h"
#include "mlistview.h"
#include "mabstractdata.h"

#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>

class MPrototypeStick : public QWidget
{
    Q_OBJECT
public:
    explicit MPrototypeStick(MAbstractData *data, QWidget *parent = 0);

    QSize getMinimalSize();
    int importent;
    MAbstractData *data;
    void refindImportent();

private:
    //date
    QLabel *topCreateDateLabel;
    QLabel *topCompleteDateLabel;
    QLabel *buttonEditLastLabel;
    QPushButton *completeButton;
    QHBoxLayout *upLayout;

    //title
    QLabel *titleLabel;
    QPushButton *editButton;
    QHBoxLayout *titleLayout;

    //image
    QLabel *image;

    //list
    MListView *list;

    //text
    QLabel *textLabel;

    //down
    QLabel *lastEditLabel;
    QHBoxLayout *downLayout;
    QPushButton *deleteButton;

    QVBoxLayout *mainLayout;

signals:
    void signalEditStick(QString);
    void signalComplete(MAbstractData*);

private slots:
    void slotSendToEdit();
    void slotSendComplete();
    void slotSendComplete(MDataList list);
};

#endif // MPROTOTYPESTICK_H
