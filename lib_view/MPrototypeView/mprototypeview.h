#ifndef MPROTOTYPEVIEW_H
#define MPROTOTYPEVIEW_H

#include "msortbar.h"
#include "mprototypestick.h"
#include "mabstractdata.h"

#include <QtCore/QList>
#include <QtCore/QSettings>
#include <QtCore/qglobal.h>
#include <QtGui/QKeyEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>

#if defined(MPROTOTYPEVIEW_LIBRARY)
#  define MPROTOTYPEVIEWSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MPROTOTYPEVIEWSHARED_EXPORT Q_DECL_IMPORT
#endif

class MPROTOTYPEVIEWSHARED_EXPORT MPrototypeView : public QWidget
{
    Q_OBJECT
public:
    explicit MPrototypeView(QWidget *parent = 0);

    void keyPressEvent(QKeyEvent *e);
    void resizeEvent(QResizeEvent *e);

private:
    QSettings settings;
    Menum viewType;
    MSort sort;
    QList <MPrototypeStick*> listStick;
    MPrototypeStick *oneStick;
    QList <MAbstractData*> listData;

    QWidget *shortRightPanel;
    QWidget *fullRightPanel;
    QWidget *contentWidget;
    QList <QPushButton*> shortListButton;
    QList <QPushButton*> fullListButton;

    int currentStick = 0;

    void deleteContent();

signals:

public slots:
    void slotSetData(QList<MAbstractData*> list);
    void slotSetSort(MSort sort);
    void slotSetView(Menum view);
    void slotClickToButton();
};

#endif // MPROTOTYPEVIEW_H

