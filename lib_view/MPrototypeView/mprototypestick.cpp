#include "mprototypestick.h"

MPrototypeStick::MPrototypeStick(MAbstractData *data, QWidget *parent)
    : QWidget(parent), data(data)
{
    //up
    this->show();
    topCreateDateLabel = new QLabel(tr("Создано: ") + data->createDate.time().toString("hh:mm")
                                    + '\n' + data->createDate.date().toString("dd.MM.yyyy") + ' ');
    topCreateDateLabel->setAlignment(Qt::AlignLeft);
    topCompleteDateLabel = new QLabel;
    topCompleteDateLabel->setAlignment(Qt::AlignRight);
    if (data->isMainDataPresent)
        topCompleteDateLabel->setText(tr("Выполнить до: ") + data->endDate.time().toString("hh:mm")
                                      + "\n " + data->endDate.date().toString("dd.MM yyyy"));
    completeButton = MService::convertButton(QIcon(":/://images/info.png"), "выполнить");
    upLayout = new QHBoxLayout;
    upLayout->setMargin(0);
    upLayout->setSpacing(0);
    upLayout->addWidget(topCreateDateLabel);
    upLayout->addWidget(topCompleteDateLabel);
    upLayout->addWidget(completeButton);

    //title
    titleLabel = new QLabel(data->title);
    titleLabel->setAlignment(Qt::AlignCenter);
    editButton = MService::convertButton(QIcon("://images/expand2.png"), tr("редактировать"));
    titleLayout = new QHBoxLayout;
    titleLayout->setMargin(0);
    titleLayout->setSpacing(0);
    titleLayout->addWidget(titleLabel);
    titleLayout->addWidget(editButton);

    //down
    lastEditLabel = new QLabel;
    lastEditLabel->setAlignment(Qt::AlignRight);
    if (data->listDate.length() != 0)
        lastEditLabel->setText(tr("Последнее редактирование \n")
                               + data->listDate.last().toString("hh:mm dd.MM.yyyy"));
    deleteButton = MService::convertButton(QIcon("://images/fileAdd.png"), tr("удалить"));
    downLayout  = new QHBoxLayout;
    downLayout->setMargin(0);
    downLayout->setSpacing(0);
    downLayout->addWidget(deleteButton);
    downLayout->addWidget(new QFrame);
    downLayout->addWidget(lastEditLabel);

    //Компоновка главного окна
    mainLayout  = new QVBoxLayout(this);
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    mainLayout->addLayout(upLayout);
    mainLayout->addLayout(titleLayout);
    if (data->isImagePresent){
        image = new QLabel;
        image->setPixmap(QPixmap(data->image));
        mainLayout->addWidget(image);
    }
    if (data->isListPresent){
        list = new MListView(&data->dataList);
        mainLayout->addWidget(list);
    }
    if (data->isTextPresent){
        textLabel = new QLabel(data->text);
        mainLayout->addWidget(textLabel);
    }
    mainLayout->addLayout(downLayout);
}

QSize MPrototypeStick::getMinimalSize()
{
    return this->sizeHint();
}

void MPrototypeStick::refindImportent()
{
    qDebug() << "MPrototypeStick::refindImportent()";
    importent = 0;
    switch (data->prioriTypeData){
    case Menum::Max : importent += 100;
    case Menum::High : importent += 100;
    case Menum::Middle : importent += 100;
    case Menum::Low : importent += 100;
    default:;
    }
    if (data->isMainDataPresent)
        importent += 1000/(QDateTime::currentDateTime().secsTo(data->endDate) / 3600);
}

void MPrototypeStick::slotSendComplete()
{
    qDebug() << "MPrototypeStick::slotSendComplete()";
    Menum::Waiting ? data->statusTypeData = Menum::Complete
                   : data->statusTypeData = Menum::Waiting;
    emit signalComplete(data);
}

void MPrototypeStick::slotSendToEdit()
{
    qDebug() << "MPrototypeStick::slotSendToEdit()";
    emit signalEditStick(data->dataKey);
}

void MPrototypeStick::slotSendComplete(MDataList list)
{
    qDebug() << "MPrototypeStick::slotSendComplete(MDataList list)";
    data->dataList = list;
    emit signalComplete(data);
}
