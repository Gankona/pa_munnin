include($$PWD/../../lib_view/MWritePrototype/MWritePrototype.pri)
include($$PWD/../../lib/MProfileInfo/MProfileInfo.pri)
include($$PWD/../../lib_view/MPasswordSwitcher/MPasswordSwitcher.pri)
include($$PWD/../../lib_view/MFloatOpasityWidget/MFloatOpasityWidget.pri)
include($$PWD/../../lib_view/MPrototypeRight/MPrototypeRight.pri)
include($$PWD/../../lib_view/MPrototypeView/MPrototypeView.pri)
include($$PWD/../../lib_view/MListEdit/MListEdit.pri)
include($$PWD/../../lib/MSettingModel/MSettingModel.pri)
include($$PWD/../../lib_view/MMiddleScrollWidget/MMiddleScrollWidget.pri)
include($$PWD/../../lib_view/MItemSwitch/MItemSwitch.pri)

isEmpty(DesktopView.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/DesktopView/ -lDesktopView
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/DesktopView/ -lDesktopView
    INCLUDEPATH += $$PWD/../../lib_view/DesktopView
    DEPENDPATH += $$PWD/../../lib_view/DesktopView
}
