#include "settingsview.h"

SettingsView::SettingsView(MSettingModel *settingModel, QWidget *parent)
    : MPrototypeRight(parent), setting("Gankona", "Munnin"), settingModel(settingModel)
{
    QString path = ":/LeftPanel/Files/Images/LeftPanel/";

    listLeftButton.push_back(MService::convertButton(QIcon(path + "view.png"), tr("Настройка изображения"), this));
    listLeftButton.push_back(MService::convertButton(QIcon(path + "viewStick.png"), tr("Настройка интерфейса"), this));
    listLeftButton.push_back(MService::convertButton(QIcon(path + "viewToDo.png"), tr("Сетевые настройки"), this));
    listLeftButton.push_back(MService::convertButton(QIcon(path + "info.png"), tr("Настройка аккаунта"), this));
    listLeftButton.push_back(MService::convertButton(QIcon(path + "hotKey.png"), tr("Настройка гарячих клавиш"), this));

    this->updateLeftPanel();

    currentWidget = -1;
    graphic = nullptr;
    interface = nullptr;
    network = nullptr;
    account = nullptr;
    hotKey = nullptr;

    this->slotSetCurrentWidget(0);

    QObject::connect(this, SIGNAL(signalSetCurrentWidget(int)),
                     this, SLOT  (slotSetWidget         (int)));
}

void SettingsView::resizeEvent(QResizeEvent *e)
{
    qDebug() << "Settings::resizeEvent(QResizeEvent *e)";
    QSize size;
    e == nullptr ? size = this->size()
                 : size = e->size();
    if (graphic != nullptr)
        graphic->setFixedSize(size);
    else if (interface != nullptr)
        interface->setFixedSize(size);
    else if (network != nullptr)
        network->setFixedSize(size);
    else if (account != nullptr)
        account->setFixedSize(size);
    else if (hotKey != nullptr)
        hotKey->setFixedSize(size);
    this->resize(size);
}

void SettingsView::slotSetWidget(int number)
{
    qDebug() << number << " Settings::slotSetWidget(int number) " << currentWidget;
    if (currentWidget == number)
        return;
    //delete
    switch (currentWidget){
    case 0:
        delete graphic;
        graphic = nullptr;
        break;
    case 1:
        delete interface;
        interface = nullptr;
        break;
    case 2:
        delete network;
        network = nullptr;
        break;
    case 3:
        delete account;
        account = nullptr;
        break;
    case 4:
        delete hotKey;
        hotKey = nullptr;
        break;
    default:;
    }
    //create
    currentWidget = number;
    switch (number){
    case 0:
        graphic = new GraphicSetting(settingModel->graphic, this->contentFrame);
        graphic->show();
        break;
    case 1:
        interface = new InterfaceSetting(this->contentFrame);
        interface->show();
        break;
    case 2:
        network = new NetworkSetting(this->contentFrame);
        network->show();
        break;
    case 3:
        account = new AccountSetting(this->contentFrame);
        account->show();
        break;
    case 4:
        hotKey = new HotKeySetting(this->contentFrame);
        hotKey->show();
        break;
    default:;
    }
    this->resizeEvent(nullptr);
}
