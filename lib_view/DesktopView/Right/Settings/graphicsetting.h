#ifndef GRAPHICSETTING_H
#define GRAPHICSETTING_H

#include "mgraphicssetting.h"
#include "mmiddlescrollwidget.h"
#include "mservice.h"

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QSlider>
#include <QtWidgets/QPushButton>

class GraphicSetting : public MMiddleScrollWidget
{
    Q_OBJECT

    MGraphicsSetting *graphicSetting;

public:
    GraphicSetting(MGraphicsSetting *graphicSetting, QWidget *parent = 0);

private:
    QVBoxLayout *mainLayout;

    //isFixedSizeWindow;
    QHBoxLayout *isFixedSizeWindowLayout;
    QWidget *isFixedSizeWindowWgt;
    QLabel *isFixedSizeWindowLabel;
    QPushButton *isFixedSizeWindowYButton;
    QPushButton *isFixedSizeWindowNButton;

    //isChangeSizeRateably;
    QHBoxLayout *isChangeSizeRateablyLayout;
    QWidget *isChangeSizeRateablyWgt;
    QLabel *isChangeSizeRateablyLabel;
    QPushButton *isChangeSizeRateablyYButton;
    QPushButton *isChangeSizeRateablyNButton;

    //isMenuVisible;
    QHBoxLayout *isMenuVisibleLayout;
    QWidget *isMenuVisibleWgt;
    QLabel *isMenuVisibleLabel;
    QPushButton *isMenuVisibleYButton;
    QPushButton *isMenuVisibleNButton;

    //isAlwaysShowWindow;
    QHBoxLayout *isAlwaysShowWindowLayout;
    QWidget *isAlwaysShowWindowWgt;
    QLabel *isAlwaysShowWindowLabel;
    QPushButton *isAlwaysShowWindowYButton;
    QPushButton *isAlwaysShowWindowNButton;

    //isFloatLoginWindow;
    QHBoxLayout *isFloatLoginWindowLayout;
    QWidget *isFloatLoginWindowWgt;
    QLabel *isFloatLoginWindowLabel;
    QPushButton *isFloatLoginWindowYButton;
    QPushButton *isFloatLoginWindowNButton;

    //isFixedStartWindowSize;
    QHBoxLayout *isFixedStartWindowSizeLayout;
    QWidget *isFixedStartWindowSizeWgt;
    QLabel *isFixedStartWindowSizeLabel;
    QPushButton *isFixedStartWindowSizeYButton;
    QPushButton *isFixedStartWindowSizeNButton;

    //startFixedWindowSize;
    QHBoxLayout *startFixedWindowSizeLayout;
    QWidget *startFixedWindowSizeWgt;
    QLabel *startFixedWindowSizeLabel;

    //minimumSize;
    QHBoxLayout *minimumSizeLayout;
    QWidget *minimumSizeWgt;
    QLabel *minimumSizeLabel;

    //button
    QHBoxLayout *buttonLayout;
    QPushButton *backButton;
    QPushButton *defaultButton;
    QPushButton *saveButton;
    QWidget *buttonWidget;

    void setCurrentSetting();

signals:
    void backToMenu();

private slots:
    void slotSaveSetting();
    void slotBackToHome();
    void slotSetDefault();
};

#endif // GRAPHICSETTING_H
