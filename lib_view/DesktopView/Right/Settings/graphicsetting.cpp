#include "graphicsetting.h"

GraphicSetting::GraphicSetting(MGraphicsSetting *graphicSetting, QWidget *parent)
    : MMiddleScrollWidget(parent), graphicSetting(graphicSetting)
{
    mainLayout = new QVBoxLayout(this->contentArea);
    mainLayout->setSpacing(2);
    mainLayout->setMargin(2);

    MService::createItem(isFixedSizeWindowLayout, isFixedSizeWindowWgt,
                         isFixedSizeWindowLabel, mainLayout,
                         tr("Закрепить размер"), true,
                         isFixedSizeWindowYButton, isFixedSizeWindowNButton,
                         tr("да"), tr("нет"));

    MService::createItem(isChangeSizeRateablyLayout, isChangeSizeRateablyWgt,
                         isChangeSizeRateablyLabel, mainLayout,
                         tr("Менять размер окна пропорционально"), true,
                         isChangeSizeRateablyYButton, isChangeSizeRateablyNButton,
                         tr("да"), tr("нет"));

    MService::createItem(isMenuVisibleLayout, isMenuVisibleWgt,
                         isMenuVisibleLabel, mainLayout,
                         tr("Показывать меню"), true,
                         isMenuVisibleYButton, isMenuVisibleNButton,
                         tr("да"), tr("нет"));

    MService::createItem(isAlwaysShowWindowLayout, isAlwaysShowWindowWgt,
                         isAlwaysShowWindowLabel, mainLayout,
                         tr("Всегда показывать окно"), true,
                         isAlwaysShowWindowYButton, isAlwaysShowWindowNButton,
                         tr("да"), tr("нет"));

    MService::createItem(isFloatLoginWindowLayout, isFloatLoginWindowWgt,
                         isFloatLoginWindowLabel, mainLayout,
                         tr("Плавающее окно авторизации"), true,
                         isFloatLoginWindowYButton, isFloatLoginWindowNButton,
                         tr("да"), tr("нет"));

    MService::createItem(isFixedStartWindowSizeLayout, isFixedStartWindowSizeWgt,
                         isFixedStartWindowSizeLabel, mainLayout,
                         tr("Зафиксировать размер окна при старте"), true,
                         isFixedStartWindowSizeYButton, isFixedStartWindowSizeNButton,
                         tr("да"), tr("нет"));

    MService::createItem(startFixedWindowSizeLayout, startFixedWindowSizeWgt,
                         startFixedWindowSizeLabel,  mainLayout,
                         tr("Стартовый фиксированый размер окна"));

    MService::createItem(minimumSizeLayout, minimumSizeWgt,
                         minimumSizeLabel,  mainLayout,
                         tr("Минимальный размер окна"));

    buttonLayout = new QHBoxLayout;
    backButton = MService::convertButton(tr("назад"), 80, 25);
    defaultButton = MService::convertButton(tr("по умолчанию"), 120, 25);
    saveButton = MService::convertButton(tr("сохранить"), 80, 25);

    buttonLayout->setMargin(0);
    buttonLayout->setMargin(0);
    buttonLayout->addWidget(backButton);
    buttonLayout->addWidget(new QFrame);
    buttonLayout->addWidget(defaultButton);
    buttonLayout->addWidget(new QFrame);
    buttonLayout->addWidget(saveButton);
    buttonWidget = new QWidget;
    buttonWidget->setLayout(buttonLayout);
    mainLayout->addWidget(buttonWidget);
    mainLayout->addWidget(new QWidget);
    //this->setWidgetResizable(true);
}

void GraphicSetting::setCurrentSetting(){}

void GraphicSetting::slotBackToHome(){}

void GraphicSetting::slotSaveSetting(){}

void GraphicSetting::slotSetDefault(){}
