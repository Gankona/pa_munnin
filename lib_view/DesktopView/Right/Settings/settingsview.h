#ifndef SETTINGSVIEW_H
#define SETTINGSVIEW_H

#include "mservice.h"
#include "mprototyperight.h"
#include "msettingmodel.h"

#include "Right/Settings/hotkeysetting.h"
#include "Right/Settings/accountsetting.h"
#include "Right/Settings/graphicsetting.h"
#include "Right/Settings/networksetting.h"
#include "Right/Settings/interfacesetting.h"

#include <QtCore/QSettings>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>

class SettingsView : public MPrototypeRight
{
    Q_OBJECT

    MSettingModel *settingModel;
public:    
    SettingsView(MSettingModel *settingModel, QWidget *parent = nullptr);

    GraphicSetting *graphic;
    InterfaceSetting *interface;
    NetworkSetting *network;
    AccountSetting *account;
    HotKeySetting *hotKey;

    QSettings setting;

    void resizeEvent(QResizeEvent *e);

private:
    int currentWidget;

public slots:
    void slotSetWidget(int number);
};

#endif // SETTINGSVIEW_H
