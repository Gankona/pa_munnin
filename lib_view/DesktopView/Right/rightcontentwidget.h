#ifndef RIGHTCONTENTWIDGET_H
#define RIGHTCONTENTWIDGET_H

#include "Right/infor.h"
#include "Right/hotkeyr.h"
#include "Right/homewidget.h"
#include "Right/Settings/settingsview.h"
#include "Right/ViewData/viewdata.h"
#include "Right/WriteNew/writenew.h"
#include "Right/Calendar/calendarr.h"

#include <QtCore/QDebug>
#include <QtGui/QResizeEvent>

class RightContentWidget : public QWidget
{
    Q_OBJECT
public:
    RightContentWidget(MSettingModel *settingModel, QWidget *parent = nullptr);

    QHBoxLayout *layout;

    SettingsView *settings;
    ViewData *viewData;
    WriteNew *writeNew;
    CalendarR *calendar;

    InfoR *info;
    HotKeyR *hotKey;
    HomeWidget *homeWidget;

    void resizeEvent(QResizeEvent *e);

private:
    int currentWidget;

    MSettingModel *settingModel;

signals:
    void signalWriteTodo(MAbstractData);
    void signalWriteEditTodo(MAbstractData);
    void signalSetData(QList<MAbstractData*>);
    void signalGetData(Menum parametr);

public slots:
    void slotSetCurrentWidget(int number);
};

#endif // RIGHTCONTENTWIDGET_H
