#include "rightcontentwidget.h"

RightContentWidget::RightContentWidget(MSettingModel *settingModel, QWidget *parent)
    : QWidget(parent), settingModel(settingModel)
{
//    this->setStyleSheet("background: #202020; color: white");
    currentWidget = -1;
    homeWidget = nullptr;
    writeNew = nullptr;
    viewData = nullptr;
    calendar = nullptr;
    settings = nullptr;
    hotKey = nullptr;
    info = nullptr;

    slotSetCurrentWidget(0);
}

void RightContentWidget::slotSetCurrentWidget(int number)
{
    qDebug() << number
             << " RightContentWidget::slotSetCurrentWidget(int number) "
             << currentWidget;
    if (currentWidget == number)
        return;
    //delete
    switch (currentWidget){
    case 0:
        delete homeWidget;
        homeWidget = nullptr;
        break;
    case 1:
        delete writeNew;
        writeNew = nullptr;
        break;
    case 2:
        delete viewData;
        viewData = nullptr;
        break;
    case 3:
        delete calendar;
        calendar = nullptr;
        break;
    case 4:
        delete settings;
        settings = nullptr;
        break;
    case 5:
        delete hotKey;
        hotKey = nullptr;
        break;
    case 6:
        delete info;
        info = nullptr;
        break;
    default:;
    }
    qDebug() << "create";
    //create
    currentWidget = number;
    switch (number){
    case 0:
        qDebug() << "home";
        homeWidget = new HomeWidget(this);
        homeWidget->show();
        break;
    case 1:
        qDebug() << "writeNew";
        writeNew = new WriteNew(this);
        writeNew->show();
        QObject::connect(writeNew, SIGNAL(signalWriteTodo(MAbstractData)),
                         this,     SIGNAL(signalWriteTodo(MAbstractData)));

        QObject::connect(writeNew, SIGNAL(signalWriteEditTodo(MAbstractData)),
                         this,     SIGNAL(signalWriteEditTodo(MAbstractData)));
        break;
    case 2:
        qDebug() << "viewData";
        viewData = new ViewData(this);
        viewData->show();
        QObject::connect(this, SIGNAL(signalSetData(QList<MAbstractData*>)),
                         viewData, SLOT(slotSetData(QList<MAbstractData*>)));
        QObject::connect(viewData, SIGNAL(signalGetData(Menum)),
                         this,     SIGNAL(signalGetData(Menum)));
        emit signalGetData(Menum::Waiting);
        break;
    case 3:
        qDebug() << "Calendar";
        calendar = new CalendarR(this);
        calendar->show();
        break;
    case 4:
        qDebug() << "Settings";
        settings = new SettingsView(settingModel, this);
        settings->show();
        break;
    case 5:
        qDebug() << "hotKey";
        hotKey = new HotKeyR(this);
        hotKey->show();
        break;
    case 6:
        qDebug() << "info";
        info = new InfoR(this);
        info->show();
        break;
    default:;
    }
    qDebug() << "call resize";
    this->resizeEvent(nullptr);
}

void RightContentWidget::resizeEvent(QResizeEvent *e)
{
    qDebug() << "RightContentWidget::resizeEvent(QResizeEvent *e)";
    QSize size;
    e == nullptr ? size = this->size()
                 : size = e->size();
    if (homeWidget != nullptr)
        homeWidget->setFixedSize(size);
    else if (writeNew != nullptr)
        writeNew->setFixedSize(size);
    else if (viewData != nullptr)
        viewData->setFixedSize(size);
    else if (calendar != nullptr)
        calendar->setFixedSize(size);
    else if (settings != nullptr)
        settings->setFixedSize(size);
    else if (hotKey != nullptr)
        hotKey->setFixedSize(size);
    else if (info != nullptr)
        info->setFixedSize(size);
}
