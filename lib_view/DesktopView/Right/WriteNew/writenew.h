#ifndef WRITENEW_H
#define WRITENEW_H

#include "mservice.h"
#include "mprototyperight.h"
#include "Right/WriteNew/writeplan.h"
#include "Right/WriteNew/writetodo.h"
#include "Right/WriteNew/writestick.h"

#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>

class WriteNew : public MPrototypeRight
{
    Q_OBJECT
public:
    explicit WriteNew(QWidget *parent);

private:
    int currentWidget;

    WritePlan *writePlan;
    WriteTodo *writeTodo;
    WriteStick *writeStick;

    void resizeEvent(QResizeEvent *e);

signals:
    void signalWriteTodo(MAbstractData);
    void signalWriteEditTodo(MAbstractData);

public slots:
    void slotSetWidget(int number);
};

#endif // WRITENEW_H
