#include "writetodo.h"

WriteTodo::WriteTodo(MAbstractData *editData, QWidget *parent)
      : MWritePrototype(parent)
{
    createContent();
    createShortPanel();
    createFullPanel();

    this->slotReleaseParametr(privateAccessF);
    this->slotReleaseParametr(presentTimerF);
    this->slotReleaseParametr(midProiriF);
    this->slotReleaseParametr(noRepeatF);
    slotSwitchPanel(true);
    slotSwitchPanel(false);

    if (editData != nullptr){
        isNewTodo = false;
        completeCentrLabel->setText(tr("Редактирование"));
        fillContent(editData);
    }
    else{
        isNewTodo = true;
        completeCentrLabel->setText(tr("Создание"));
    }

    QObject::connect(this, SIGNAL(signalSwitchPanel(bool)),
                     this, SLOT  (slotSwitchPanel  (bool)));
    QObject::connect(forestMenu, SIGNAL(signalChooseMenu   (QPushButton*)),
                     this,       SLOT  (slotReleaseParametr(QPushButton*)));
}

inline void WriteTodo::fillContent(MAbstractData *data)
{
    qDebug() << data;
}

void WriteTodo::setDefault()
{
    qDebug() << "WriteTodo::setDefault()";
    dateEdit->clear();
    title->clear();
    tegs->clear();
    imageLabel->clear();
    imageWgt->clear();
    textWgt->clear();
    textEdit->clear();
    listWgt->clear();
    listEdit->clear();
}

inline void WriteTodo::createContent()
{
    qDebug() << "WriteTodo::createContent()";
    //шапка
    completeWrite = MService::convertButton(QIcon(":/Other/Files/Images/Other/trayIcon.jpg"), tr("Выполнить"));
    completeLeftLabel = new QLabel;
    completeCentrLabel = new QLabel;
    completeRightLabel = new QLabel;
    completeCentrLabel->setAlignment(Qt::AlignCenter);
    completeLeftLabel->setAlignment(Qt::AlignLeft);
    completeRightLabel->setAlignment(Qt::AlignRight);
    completeLayout = new QHBoxLayout;
    completeLayout->setSpacing(2);
    completeLayout->setMargin(0);
    completeLayout->addWidget(completeLeftLabel);
    completeLayout->addWidget(completeCentrLabel);
    completeLayout->addWidget(completeRightLabel);
    completeLayout->addWidget(completeWrite);
    QObject::connect(completeWrite, SIGNAL(released()), this, SLOT(slotWriteTodo()));

    //название
    title = new MPushEditLabel;

    //теги
    tegs = new MPushEditLabel;
    tegs->setTags(true);

    //дата
    dateLabel = new QLabel(tr("Дата") + ": ");
    dateEdit = new QDateTimeEdit;
    dateEdit->setCalendarPopup(true);
//    dateEdit->setStyleSheet("background: white; color: black");
    dateEdit->setMinimumDateTime(QDateTime::currentDateTime());
    dateEdit->setCalendarWidget(new QCalendarWidget);
    dateEdit->setDisplayFormat("dd.MM.yy hh:mm");
    dateEdit->setFrame(false);

    //компоновка первыйх трьох
    titleLayout = new QVBoxLayout;
    titleLayout->setSpacing(0);
    titleLayout->setMargin(0);
    titleLayout->addWidget(new QLabel(tr("Название")+": "));
    titleLayout->addWidget(new QLabel(tr("Теги")+": "));
    titleLayout->addWidget(dateLabel);

    contTTDLayout = new QVBoxLayout;
    contTTDLayout->setSpacing(0);
    contTTDLayout->setMargin(0);
    contTTDLayout->addWidget(title);
    contTTDLayout->addWidget(tegs);
    contTTDLayout->addWidget(dateEdit);

    titleTegsDateLayout = new QHBoxLayout;
    titleTegsDateLayout->setSpacing(0);
    titleTegsDateLayout->setMargin(0);
    titleTegsDateLayout->addLayout(titleLayout);
    titleTegsDateLayout->addLayout(contTTDLayout);
    titleTegsDateLayout->setStretch(1, 2);

    //изображение
    imageWgt = new MWidget(tr("Изображение"), true);
    imageLabel = new QLabel;
    imageLayout = new QHBoxLayout(imageWgt->contentLabel);
    imageLayout->addWidget(imageLabel);

    //список
    listWgt = new MWidget(tr("Список"), true);
    listEdit = new MListEdit;
    listLayout = new QHBoxLayout(listWgt->contentLabel);
    listLayout->addWidget(listEdit);
    listLayout->setSpacing(0);
    listLayout->setMargin(0);
    QObject::connect(listEdit, SIGNAL(setMaxHeight(int)),
                     listWgt->contentLabel, SLOT(slotSetMaxHeight(int)));
    listWgt->contentLabel->slotSetMaxHeight(34);

    //текст
    textWgt = new MWidget(tr("Текст"), true);
    textEdit = new QTextEdit;
    textLayout = new QHBoxLayout(textWgt->contentLabel);
    textLayout->addWidget(textEdit);

    //oшибки
    errorLabel = new QLabel;
    QFont font = errorLabel->font();
    font.setPixelSize(10);
    errorLabel->setFont(font);
    errorLabel->setFixedHeight(10);

    //Компоновка
    contentLayout = new QVBoxLayout(contentFrame);
    contentLayout->setSpacing(2);
    contentLayout->setMargin(5);
    contentLayout->addLayout(completeLayout);
    contentLayout->addLayout(titleTegsDateLayout);
    contentLayout->addWidget(imageWgt);
    contentLayout->addWidget(listWgt);
    contentLayout->addWidget(textWgt);
    contentLayout->addWidget(errorLabel);
    contentLayout->addWidget(new QFrame);
}

inline void WriteTodo::createFullPanel()
{
    qDebug() << "WriteTodo::createFullPanel()";
    QString path = ":/LeftPanel/Files/Images/LeftPanel/";

    accessLabel = new QLabel(tr("Параметр доступа") + " :");
    accessLabel->setFixedHeight(30);
    publicAccessF =  MService::convertButton(QIcon(path + "settings.png"), tr("Открыт для всех"));
    privateAccessF = MService::convertButton(QIcon(path + "settings.png"), tr("Приватный"));
    friendAccessF =  MService::convertButton(QIcon(path + "settings.png"), tr("Разрешено для друзей"));
    QObject::connect(publicAccessF,  SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(privateAccessF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(friendAccessF,  SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    accessLayout = new QHBoxLayout;
    accessLayout->setMargin(0);
    accessLayout->setSpacing(0);
    accessLayout->addWidget(new QFrame);
    accessLayout->addWidget(publicAccessF);
    accessLayout->addWidget(privateAccessF);
    accessLayout->addWidget(friendAccessF);

    timerLabel = new QLabel(tr("Напоминание") + " :");
    timerLabel->setFixedHeight(30);
    presentTimerF = MService::convertButton(QIcon(path + "settings.png"), tr("Включить время выполнения"));
    denaidTimerF =  MService::convertButton(QIcon(path + "settings.png"), tr("Без время выполнения"));
    QObject::connect(presentTimerF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(denaidTimerF,  SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    timerLayout = new QHBoxLayout;
    timerLayout->addWidget(new QFrame);
    timerLayout->addWidget(presentTimerF);
    timerLayout->addWidget(denaidTimerF);

    prioriLabel = new QLabel(tr("Приоритет") + " :");
    prioriLabel->setFixedHeight(30);
    maxProiriF = MService::convertButton(QIcon(path + "settings.png"), tr("Максимальный приоритет"));
    higProiriF = MService::convertButton(QIcon(path + "settings.png"), tr("Повышеный приоритет"));
    midProiriF = MService::convertButton(QIcon(path + "settings.png"), tr("Обычный приоритет"));
    lowProiriF = MService::convertButton(QIcon(path + "settings.png"), tr("Пониженый приоритет"));
    minProiriF = MService::convertButton(QIcon(path + "settings.png"), tr("Минимальный приоритет"));
    QObject::connect(maxProiriF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(higProiriF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(midProiriF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(lowProiriF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(minProiriF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    prioriLayout = new QHBoxLayout;
    prioriLayout->addWidget(new QFrame);
    prioriLayout->addWidget(maxProiriF);
    prioriLayout->addWidget(higProiriF);
    prioriLayout->addWidget(midProiriF);
    prioriLayout->addWidget(lowProiriF);
    prioriLayout->addWidget(minProiriF);

    repeatLabel = new QLabel(tr("Настройка повторов") + " :");
    repeatLabel->setFixedHeight(30);
    noRepeatF =      MService::convertButton(QIcon(path + "settings.png"), tr("Без повторов"));
    dailyRepeatF =   MService::convertButton(QIcon(path + "settings.png"), tr("Ежеденевные напоминания"));
    weeklyRepeatF =  MService::convertButton(QIcon(path + "settings.png"), tr("Еженедельные напоминания"));
    weekdayRepeatF = MService::convertButton(QIcon(path + "settings.png"), tr("Напоминания по будням"));
    weekendRepeatF = MService::convertButton(QIcon(path + "settings.png"), tr("Напоминания на выходных"));
    unStaticRepeatF= MService::convertButton(QIcon(path + "settings.png"), tr("Неупорядочные напоминания"));
    QObject::connect(noRepeatF,       SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(dailyRepeatF,    SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(weekdayRepeatF,  SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(weekendRepeatF,  SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(weeklyRepeatF,   SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    QObject::connect(unStaticRepeatF, SIGNAL(released()), this, SLOT(slotReleaseFullButton()));
    repeatLayout = new QHBoxLayout;
    repeatLayout->addWidget(noRepeatF);
    repeatLayout->addWidget(dailyRepeatF);
    repeatLayout->addWidget(weeklyRepeatF);
    repeatLayout->addWidget(weekdayRepeatF);
    repeatLayout->addWidget(weekendRepeatF);
    repeatLayout->addWidget(unStaticRepeatF);

    fullPanelLayout = new QVBoxLayout(fullPanel);
    fullPanelLayout->addWidget(accessLabel);
    fullPanelLayout->addLayout(accessLayout);
    fullPanelLayout->addWidget(timerLabel);
    fullPanelLayout->addLayout(timerLayout);
    fullPanelLayout->addWidget(prioriLabel);
    fullPanelLayout->addLayout(prioriLayout);
    fullPanelLayout->addWidget(repeatLabel);
    fullPanelLayout->addLayout(repeatLayout);
    fullPanelLayout->addWidget(new QFrame);

//    fullPanel->setStyleSheet("background: white; color: red");
}

inline void WriteTodo::createShortPanel()
{
    qDebug() << "WriteTodo::createShortPanel()";
    QString path = ":/LeftPanel/Files/Images/LeftPanel/";

    publicAccessS = forestMenu->addButton (QIcon(path + "viewToDo.png"), true,  "", tr("Открыт для всех"));
    privateAccessS = forestMenu->addButton(QIcon(path + "viewToDo.png"), false, "", tr("Приватный"));
    friendAccessS = forestMenu->addButton (QIcon(path + "viewToDo.png"), false, "", tr("Разрешено для друзей"));

    presentTimerS = forestMenu->addButton(QIcon(path + "viewToDo.png"), true,  "", tr("Включить время выполнения"));
    denaidTimerS = forestMenu->addButton (QIcon(path + "viewToDo.png"), false, "", tr("Без время выполнения"));

    maxProiriS = forestMenu->addButton(QIcon(path + "viewToDo.png"), true,  "", tr("Максимальный приоритет"));
    higProiriS = forestMenu->addButton(QIcon(path + "viewToDo.png"), false, "", tr("Повышеный приоритет"));
    midProiriS = forestMenu->addButton(QIcon(path + "viewToDo.png"), false, "", tr("Обычный приоритет"));
    lowProiriS = forestMenu->addButton(QIcon(path + "viewToDo.png"), false, "", tr("Пониженый приоритет"));
    minProiriS = forestMenu->addButton(QIcon(path + "viewToDo.png"), false, "", tr("Минимальный приоритет"));

    dailyRepeatS = forestMenu->addButton   (QIcon(path + "viewToDo.png"), true,  "", tr("Ежеденевные напоминания"));
    noRepeatS = forestMenu->addButton      (QIcon(path + "viewToDo.png"), false, "", tr("Без повторов"));
    weeklyRepeatS = forestMenu->addButton  (QIcon(path + "viewToDo.png"), false, "", tr("Еженедельные напоминания"));
    unStaticRepeatS = forestMenu->addButton(QIcon(path + "viewToDo.png"), false, "", tr("Неупорядочные напоминания"));
    weekdayRepeatS = forestMenu->addButton (QIcon(path + "viewToDo.png"), false, "", tr("Напоминания по будням"));
    weekendRepeatS = forestMenu->addButton (QIcon(path + "viewToDo.png"), false, "", tr("Напоминания на выходных"));
}

void WriteTodo::slotSwitchPanel(bool isFull)
{
    qDebug() << "WriteTodo::slotSwitchPanel(bool isFull)";
    ///отслеживает изменения в одной панеле и переносит их на другую
    ///своего рода синхронизация между панелями
    if (isFull){
        //отрисовываем полную панель
        accessStatus == Menum::Public ? publicAccessF->setChecked(true)
                                        : publicAccessF->setChecked(false);
        accessStatus == Menum::Private ? privateAccessF->setChecked(true)
                                         : privateAccessF->setChecked(false);
        accessStatus == Menum::Friendly ? friendAccessF->setChecked(true)
                                          : friendAccessF->setChecked(false);

        timerStatus  == Menum::Present ? presentTimerF->setChecked(true)
                                        : presentTimerF->setChecked(false);
        timerStatus  == Menum::Denaid ? denaidTimerF->setChecked(true)
                                       : denaidTimerF->setChecked(false);

        prioriStatus == Menum::Max ? maxProiriF->setChecked(true)
                                       : maxProiriF->setChecked(false);
        prioriStatus == Menum::High ? higProiriF->setChecked(true)
                                        : higProiriF->setChecked(false);
        prioriStatus == Menum::Middle ? midProiriF->setChecked(true)
                                          : midProiriF->setChecked(false);
        prioriStatus == Menum::Low ? lowProiriF->setChecked(true)
                                       : lowProiriF->setChecked(false);
        prioriStatus == Menum::Min ? minProiriF->setChecked(true)
                                       : minProiriF->setChecked(false);

        repeatStatus == Menum::Daily ? dailyRepeatF->setChecked(true)
                                       : dailyRepeatF->setChecked(false);
        repeatStatus == Menum::No ? noRepeatF->setChecked(true)
                                    : noRepeatF->setChecked(false);
        repeatStatus == Menum::Unstatic ? unStaticRepeatF->setChecked(true)
                                          : unStaticRepeatF->setChecked(false);
        repeatStatus == Menum::Weekday ? weekdayRepeatF->setChecked(true)
                                         : weekdayRepeatF->setChecked(false);
        repeatStatus == Menum::Weekend ? weekendRepeatF->setChecked(true)
                                         : weekendRepeatF->setChecked(false);
        repeatStatus == Menum::Weekly ? weeklyRepeatF->setChecked(true)
                                        : weeklyRepeatF->setChecked(false);
    }
    else {
        //отрисовываем сокращенную панель
        switch (accessStatus){
        case Menum::Public : forestMenu->setCurrent(publicAccessS);  break;
        case Menum::Private : forestMenu->setCurrent(privateAccessS); break;
        case Menum::Friendly : forestMenu->setCurrent(friendAccessS);  break;
        default:;
        }

        switch (timerStatus){
        case Menum::Present : forestMenu->setCurrent(presentTimerS); break;
        case Menum::Denaid : forestMenu->setCurrent(denaidTimerS);  break;
        default:;
        }

        switch (prioriStatus){
        case Menum::Max : forestMenu->setCurrent(maxProiriS); break;
        case Menum::High: forestMenu->setCurrent(higProiriS); break;
        case Menum::Middle: forestMenu->setCurrent(midProiriS); break;
        case Menum::Low: forestMenu->setCurrent(lowProiriS); break;
        case Menum::Min: forestMenu->setCurrent(minProiriS); break;
        default:;
        }

        switch (repeatStatus){
        case Menum::Daily : forestMenu->setCurrent(dailyRepeatS); break;
        case Menum::No : forestMenu->setCurrent(noRepeatS); break;
        case Menum::Unstatic : forestMenu->setCurrent(unStaticRepeatS); break;
        case Menum::Weekday : forestMenu->setCurrent(weekdayRepeatS);  break;
        case Menum::Weekend : forestMenu->setCurrent(weekendRepeatS);  break;
        case Menum::Weekly : forestMenu->setCurrent(weeklyRepeatS);   break;
        default:;
        }
    }
}

void WriteTodo::slotReleaseParametr(QPushButton *senderButton)
{
    qDebug() << "WriteTodo::slotReleaseParametr(QPushButton *senderButton)";
    ///в зависимости от нажатого параметра следуем им
    /// то есть меняем параметры доступа, либо добавляем и убираем даты
    /// выставляем приоритет и частоту повторов
    QPushButton *s = senderButton;
    //Доступ------------------------------------------
    if ((s == publicAccessF) || (s == publicAccessS)){
        accessStatus = Menum::Public;
    }
    else if ((s == privateAccessF) || (s == privateAccessS)){
        accessStatus = Menum::Private;
    }
    else if ((s == friendAccessF) || (s == friendAccessS)){
        accessStatus = Menum::Friendly;
    }
    //-----------------------------------------------------

    //Напоминания------------------------------------------
    else if ((s == presentTimerF) || (s == presentTimerS)){
        timerStatus = Menum::Present;
        listEdit->setDateVisible(true);
    }
    else if ((s == denaidTimerF) || (s == denaidTimerS)){
        timerStatus = Menum::Denaid;
        listEdit->setDateVisible(false);
    }
    //-----------------------------------------------------

    //Приоритет--------------------------------------
    else if ((s == maxProiriF) || (s == maxProiriS)){
        prioriStatus = Menum::Max;
    }
    else if ((s == higProiriF) || (s == higProiriS)){
        prioriStatus = Menum::High;
    }
    else if ((s == midProiriF) || (s == midProiriS)){
        prioriStatus = Menum::Middle;
    }
    else if ((s == lowProiriF) || (s == lowProiriS)){
        prioriStatus = Menum::Low;
    }
    else if ((s == minProiriF) || (s == minProiriS)){
        prioriStatus = Menum::Min;
    }
    //-----------------------------------------------

    //Повторение-----------------------------------------
    else if ((s == dailyRepeatF) || (s == dailyRepeatS)){
        repeatStatus = Menum::Daily;
    }
    else if ((s == noRepeatF) || (s == noRepeatS)){
        repeatStatus = Menum::No;
    }
    else if ((s == unStaticRepeatF) || (s == unStaticRepeatS)){
        repeatStatus = Menum::Unstatic;
    }
    else if ((s == weekdayRepeatF) || (s == weekdayRepeatS)){
        repeatStatus = Menum::Weekday;
    }
    else if ((s == weekendRepeatF) || (s == weekendRepeatS)){
        repeatStatus = Menum::Weekend;
    }
    else if ((s == weeklyRepeatF) || (s == weeklyRepeatS)){
        repeatStatus = Menum::Weekly;
    }
    this->slotSwitchPanel(! forestMenu->isButtonPresent(senderButton));
    //-----------------------------------------------------------------
}

void WriteTodo::slotReleaseFullButton()
{
    qDebug() << "WriteTodo::slotReleaseFullButton()";
    slotReleaseParametr((QPushButton*)sender());
}

void WriteTodo::slotWriteTodo()
{
    qDebug() << "WriteTodo::slotWriteTodo()";
    MAbstractData send;
    send.clear();

    //image
    imageWgt->isFreePrototype() ? send.isImagePresent = false
                                : send.isImagePresent = true,
                                  send.image = imageWgt->image;
    //title
    if (!title->isComplete()){
        errorLabel->setText(tr("Отсутствует название"));
        QTimer::singleShot(7000, errorLabel, SLOT(clear()));
        return;
    }
    send.title = title->result;
    //tegs
    send.tegs = tegs->result;
    //date
    send.isMainDataPresent = timerStatus;
    if (send.isMainDataPresent)
        send.endDate = dateEdit->dateTime();
    //text
    textEdit->toPlainText().isEmpty() ? send.isTextPresent = false
            : send.isTextPresent = true,
              send.text = textEdit->toPlainText();
    //set Date
    if (isNewTodo)
        send.createDate = QDateTime::currentDateTime();
    else
        send.listDate.push_back(QDateTime::currentDateTime());
    //list
    if (listEdit->isComplete())
        send.dataList = listEdit->getData();
    else {
        errorLabel->setText(tr("Одно из полей списка не заполнено"));
        QTimer::singleShot(7000, errorLabel, SLOT(clear()));
        return;
    }

    send.repeatTypeData = repeatStatus;
    send.accessTypeData = accessStatus;
    send.prioriTypeData = prioriStatus;
    send.timerTypeData = timerStatus;
    send.statusTypeData = Menum::Waiting;
    send.typeData = Menum::ToDo;

    //send
    if (isNewTodo)
        emit signalWriteTodo(send);
    else
        emit signalWriteEditTodo(send);
}
