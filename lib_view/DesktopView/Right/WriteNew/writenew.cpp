#include "writenew.h"

WriteNew::WriteNew(QWidget *parent) : MPrototypeRight(parent)
{
    qDebug() << "writeNew 1";
    QString path = ":/LeftPanel/Files/Images/LeftPanel/";

    listLeftButton.push_back(MService::convertButton(QIcon(path + "fileAdd.png"), tr("Добавить заметку"), this));
    listLeftButton.push_back(MService::convertButton(QIcon(path + "timeAdd.png"), tr("Добавить напоминание"), this));
    listLeftButton.push_back(MService::convertButton(QIcon(path + "dayPlan.png"), tr("Добавить расписание"), this));

    qDebug() << "writeNew 2";
    this->updateLeftPanel();

    qDebug() << "writeNew 3";
    currentWidget = -1;
    writePlan = nullptr;
    writeTodo = nullptr;
    writeStick = nullptr;
    qDebug() << "writeNew 4";
    slotSetCurrentWidget(0);

    qDebug() << "writeNew 5";
    QObject::connect(this, SIGNAL(signalSetCurrentWidget(int)),
                     this, SLOT  (slotSetWidget         (int)));
}

void WriteNew::resizeEvent(QResizeEvent *e)
{
    qDebug() << "WriteNew::resizeEvent(QResizeEvent *e)";
    QSize size;
    e == nullptr ? size = this->size()
                 : size = e->size();
    if (writePlan != nullptr)
        writePlan->setFixedSize(size);
    else if (writeStick != nullptr)
        writeStick->setFixedSize(size);
    else if (writeTodo != nullptr)
        writeTodo->setFixedSize(size);
    this->resize(size);
}

void WriteNew::slotSetWidget(int number)
{
    qDebug() << number
             << " WriteNew::slotSetWidget(int number) "
             << currentWidget;
    if (currentWidget == number)
        return;
    //delete
    switch (currentWidget){
    case 0:
        delete writeStick;
        writeStick = nullptr;
        break;
    case 1:
        delete writeTodo;
        writeTodo = nullptr;
        break;
    case 2:
        delete writePlan;
        writePlan = nullptr;
        break;
    default:;
    }
    //create
    currentWidget = number;
    switch (number){
    case 0:
        writeStick = new WriteStick(this->contentFrame);
        writeStick->show();
        break;
    case 1:
        writeTodo = new WriteTodo(nullptr, this->contentFrame);
        writeTodo->show();
        QObject::connect(writeTodo, SIGNAL(signalWriteTodo(MAbstractData)),
                         this,      SIGNAL(signalWriteTodo(MAbstractData)));

        QObject::connect(writeTodo, SIGNAL(signalWriteEditTodo(MAbstractData)),
                         this,      SIGNAL(signalWriteEditTodo(MAbstractData)));
        break;
    case 2:
        writePlan = new WritePlan(this->contentFrame);
        writePlan->show();
        break;
    default:;
    }
    this->resizeEvent(nullptr);
}
