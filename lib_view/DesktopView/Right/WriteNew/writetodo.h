#ifndef WRITETODO_H
#define WRITETODO_H

#include "mabstractdata.h"
#include "mservice.h"
#include "mlistedit.h"
#include "mpusheditlabel.h"
#include "mwriteprototype.h"
#include "mwidget.h"

#include <QtCore/QDebug>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QTimeEdit>

class WriteTodo : public MWritePrototype
{
    Q_OBJECT
public:
    explicit WriteTodo(MAbstractData *editData = nullptr,
                       QWidget *parent = 0);

private:

    bool isNewTodo;

    //content-------------------------------
    ///upper
    QPushButton *completeWrite;
    QLabel *completeLeftLabel;
    QLabel *completeCentrLabel;
    QLabel *completeRightLabel;
    QHBoxLayout *completeLayout;

    ///title
    MPushEditLabel *title;

    ///tegs
    MPushEditLabel *tegs;

    ///date
    QLabel *dateLabel;
    QDateTimeEdit *dateEdit;

    QVBoxLayout *titleLayout;
    QVBoxLayout *contTTDLayout;
    QHBoxLayout *titleTegsDateLayout;

    ///image
    MWidget *imageWgt;
    QLabel *imageLabel;
    QHBoxLayout *imageLayout;

    ///list
    MWidget *listWgt;
    MListEdit *listEdit;
    QHBoxLayout *listLayout;

    ///text
    MWidget *textWgt;
    QTextEdit *textEdit;
    QHBoxLayout *textLayout;

    QLabel *errorLabel;
    QVBoxLayout *contentLayout;

    void createContent();
    void fillContent(MAbstractData *data);
    void setDefault();

    //union_panel_parametr------------------
    Menum repeatStatus;
    Menum accessStatus;
    Menum prioriStatus;
    Menum timerStatus;
    /*int accessStatus;
    int timerStatus;
    int prioriStatus;
    int repeatStatus;*/

    //short_panel---------------------------
    QPushButton *publicAccessS;
    QPushButton *privateAccessS;
    QPushButton *friendAccessS;

    QPushButton *presentTimerS;
    QPushButton *denaidTimerS;

    QPushButton *maxProiriS;
    QPushButton *higProiriS;
    QPushButton *midProiriS;
    QPushButton *lowProiriS;
    QPushButton *minProiriS;

    QPushButton *dailyRepeatS;
    QPushButton *noRepeatS;
    QPushButton *weeklyRepeatS;
    QPushButton *unStaticRepeatS;
    QPushButton *weekdayRepeatS;
    QPushButton *weekendRepeatS;

    void createShortPanel();

    //full_panel----------------------------
    QLabel *accessLabel;
    QPushButton *publicAccessF;
    QPushButton *privateAccessF;
    QPushButton *friendAccessF;
    QHBoxLayout *accessLayout;

    QLabel *timerLabel;
    QPushButton *presentTimerF;
    QPushButton *denaidTimerF;
    QHBoxLayout *timerLayout;

    QLabel *prioriLabel;
    QPushButton *maxProiriF;
    QPushButton *higProiriF;
    QPushButton *midProiriF;
    QPushButton *lowProiriF;
    QPushButton *minProiriF;
    QHBoxLayout *prioriLayout;

    QLabel *repeatLabel;
    QPushButton *noRepeatF;
    QPushButton *dailyRepeatF;
    QPushButton *weeklyRepeatF;
    QPushButton *weekdayRepeatF;
    QPushButton *weekendRepeatF;
    QPushButton *unStaticRepeatF;
    QHBoxLayout *repeatLayout;

    QVBoxLayout *fullPanelLayout;

    void createFullPanel();
    //--------------------------------------

signals:
    void signalWriteTodo(MAbstractData);
    void signalWriteEditTodo(MAbstractData);

public slots:
    void slotWriteTodo();
    void slotReleaseFullButton();
    void slotSwitchPanel(bool isFull);
    void slotReleaseParametr(QPushButton *senderButton);
};

#endif // WRITETODO_H
