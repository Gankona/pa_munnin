#ifndef HOMEWIDGET_H
#define HOMEWIDGET_H

#include <QtWidgets/QWidget>

class HomeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit HomeWidget(QWidget *parent = 0);
};

#endif // HOMEWIDGET_H
