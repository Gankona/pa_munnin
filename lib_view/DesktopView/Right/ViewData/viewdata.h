#ifndef VIEWDATA_H
#define VIEWDATA_H

#include "mprototyperight.h"
#include "Right/ViewData/viewcalendar.h"
#include "Right/ViewData/viewstick.h"

class ViewData : public MPrototypeRight
{
    Q_OBJECT
public:
    explicit ViewData(QWidget *parent = 0);

private:
    int currentWidget;

    ViewCalendar *calendar;
    ViewStick *stick;

    void resizeEvent(QResizeEvent *e);

signals:
    void signalGetData(Menum parametr);

public slots:
    void slotSetWidget(int number);
    void slotSetData(QList<MAbstractData*> listData);
};

#endif // VIEWDATA_H
