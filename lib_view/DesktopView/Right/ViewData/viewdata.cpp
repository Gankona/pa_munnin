#include "viewdata.h"

ViewData::ViewData(QWidget *parent) : MPrototypeRight(parent)
{
    QString path = ":/LeftPanel/Files/Images/LeftPanel/";

    listLeftButton.push_back(MService::convertButton(QIcon(path + "fileAdd.png"),
                                                     tr("Посмотреть календарь"), this));
    listLeftButton.push_back(MService::convertButton(QIcon(path + "timeAdd.png"),
                                                     tr("Посмотреть заметки"), this));

    this->updateLeftPanel();
    this->setSortPanel();

    currentWidget = -1;
    calendar = nullptr;
    stick = nullptr;

    QObject::connect(this, SIGNAL(signalSetCurrentWidget(int)),
                     this, SLOT  (slotSetWidget         (int)));
}

void ViewData::resizeEvent(QResizeEvent *e)
{
    qDebug() << "ViewData::resizeEvent(QResizeEvent *e)";
    QSize size;
    e == nullptr ? size = this->size()
                 : size = e->size();
    if (calendar != nullptr)
        calendar->setFixedSize(size);
    else if (stick != nullptr)
        stick->setFixedSize(size);
    this->resize(size);
}

void ViewData::slotSetWidget(int number)
{
    qDebug() << number
             << "ViewData::slotSetWidget(int number)  "
             << currentWidget;
    if (currentWidget == number)
        return;
    //delete
    switch (currentWidget){
    case 0:
        if (calendar != nullptr){
            delete calendar;
            calendar = nullptr;
        }
        break;
    case 1:
        if (stick != nullptr){
            delete stick;
            stick = nullptr;
        }
        break;
    default:;
    }
    //create
    currentWidget = number;
    switch (number){
    case 0:
        calendar = new ViewCalendar(this->contentFrame);
        calendar->show();
        break;
    case 1:
        stick = new ViewStick(this->contentFrame);
        stick->show();
        break;
    default:;
    }
    this->resizeEvent(nullptr);
}

void ViewData::slotSetData(QList<MAbstractData *> listData)
{
    qDebug() << "ViewData::slotSetData";
    if (calendar != nullptr)
        calendar->slotSetData(listData);
    else if (stick != nullptr)
        stick->slotSetData(listData);
    else {
        slotSetWidget(1);
        slotSetData(listData);
        foreach (MAbstractData *m, listData){
            qDebug() << "\n" << m->title << m->text << m->createDate << m->endDate;
        }
    }
}
