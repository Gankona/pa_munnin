#ifndef UPINFORMWIDGET_H
#define UPINFORMWIDGET_H

#include "Floating/loginfloatwindow.h"
#include "Floating/accountfloatinfowindow.h"
#include "Floating/registrationfloatwindow.h"

#include <QtCore/QTimer>
#include <QtCore/QDateTime>
#include <QtCore/QSettings>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>

class UpInformWidget : public QFrame
{
    Q_OBJECT
public:
    explicit UpInformWidget(QFrame *parent = nullptr);

    QLabel *loginLabel;
    QLabel *dateTimeLabel;
    QPushButton *avtorizationButton;
    QPushButton *aboutAccountButton;

    QHBoxLayout *upLayout;
    QSettings settings;

    AccountFloatInfoWindow *accountInfoWindow;
    LoginFloatWindow *loginWindow;
    RegistrationFloatWindow *registrationWindow;

signals:
    void signalGetLogin(QStringList);
    void signalGetRegistration(QStringList);

    void signalLogout();
    void signalOpenEditAccount();

public slots:
    void slotUpgradeTime();
    void slotClickAboutAccount();
};

#endif // UPINFORMWIDGET_H
