#ifndef LEFTPANEL_H
#define LEFTPANEL_H

#include "mservice.h"

#include <QDebug>
#include <QtWidgets/QFrame>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

class LeftPanel : public QFrame
{
    Q_OBJECT
public:
    explicit LeftPanel(QFrame *parent = nullptr);

    QFrame *fakeFrame;
    QPushButton *switchMenuButton;
    QList <QPushButton*> listButton;

    QVBoxLayout *mainLayout;
    QVBoxLayout *listLayout;

signals:
    void signalSwitchMenu();
    void signalSetRightWidget(int);

public slots:
    void slotClickToBarButton();
};

#endif // LEFTPANEL_H
