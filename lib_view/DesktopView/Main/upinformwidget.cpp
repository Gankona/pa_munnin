#include "upinformwidget.h"

UpInformWidget::UpInformWidget(QFrame *parent)
    : QFrame(parent),
      settings("Gankona", "Munnin")
{
//    this->setStyleSheet("background: #202020; color: white");
    this->setFixedHeight(30);

    dateTimeLabel = new QLabel;
    this->slotUpgradeTime();
    accountInfoWindow = nullptr;
    loginWindow = nullptr;
    registrationWindow = nullptr;

    loginLabel = new QLabel("Login ");
    loginLabel->setAlignment(Qt::AlignVCenter | Qt::AlignRight);

    avtorizationButton = new QPushButton(QIcon(":/Account/Files/Images/Account/logout.png"), "");
    avtorizationButton->setFocusPolicy(Qt::NoFocus);
    avtorizationButton->setFlat(true);
    avtorizationButton->setFixedSize(30, 30);

    aboutAccountButton = new QPushButton(QIcon(":/Account/Files/Images/Account/about.svg"), "");
    aboutAccountButton->setFocusPolicy(Qt::NoFocus);
    aboutAccountButton->setFlat(true);
    aboutAccountButton->setFixedSize(30, 30);

    upLayout = new QHBoxLayout(this);
    upLayout->setSpacing(0);
    upLayout->setMargin(0);
    upLayout->addWidget(dateTimeLabel);
    upLayout->addWidget(new QFrame);
    upLayout->addWidget(loginLabel);
    upLayout->addWidget(aboutAccountButton);
    upLayout->addWidget(avtorizationButton);

    QObject::connect(aboutAccountButton, SIGNAL(released()), this, SLOT(slotClickAboutAccount()));
    QObject::connect(avtorizationButton, SIGNAL(released()), this, SIGNAL(signalLogout()));
}

void UpInformWidget::slotUpgradeTime()
{
    QTimer::singleShot(1000, this, SLOT(slotUpgradeTime()));
    dateTimeLabel->setText(" " + QDateTime::currentDateTime().toString
                           (settings.value("/system/dateTimeFormat", "dd.MM.yy hh:mm:ss").toString()));
}

void UpInformWidget::slotClickAboutAccount()
{
    qDebug() << "UpInformWidget::slotClickAboutAccount()" << sender();
    if (accountInfoWindow == nullptr){
        if ((QPushButton*)sender() == aboutAccountButton){
            qDebug() << "q";
            accountInfoWindow = new AccountFloatInfoWindow(static_cast<QWidget*>(this->parent()));
            accountInfoWindow->showWidget(QPoint(static_cast<QWidget*>(this->parent())->width()
                                                 - accountInfoWindow->width() - 10, 40));

            QObject::connect(accountInfoWindow->avtorizateButton, SIGNAL(released()),
                             this,                                SLOT  (slotClickAboutAccount()));

            QObject::connect(accountInfoWindow->registrationButton, SIGNAL(released()),
                             this,                                  SLOT  (slotClickAboutAccount()));

            QObject::connect(accountInfoWindow, SIGNAL(signalLogout()),
                             this,              SIGNAL(signalLogout()));

            QObject::connect(accountInfoWindow, SIGNAL(signalOpenEditAccount()),
                             this,              SIGNAL(signalOpenEditAccount()));

            if (loginWindow != nullptr){
                loginWindow->deleteWidget();
                loginWindow = nullptr;
            }
            else if (registrationWindow != nullptr){
                registrationWindow->deleteWidget();
                registrationWindow = nullptr;
            }
        }
        else {
            accountInfoWindow->deleteWidget();
            accountInfoWindow = nullptr;
        }
    }
    else {
        qDebug() << "a";
        if ((QPushButton*)sender() == accountInfoWindow->avtorizateButton){
            qDebug() << "s";
            loginWindow = new LoginFloatWindow(static_cast<QWidget*>(this->parent()));
            loginWindow->showWidget(QPoint(static_cast<QWidget*>(this->parent())->width()
                                                 - loginWindow->width() - 10, 40));
            QObject::connect(loginWindow, SIGNAL(signalLogin   (QStringList)),
                             this,        SIGNAL(signalGetLogin(QStringList)));
        }
        else if ((QPushButton*)sender() == accountInfoWindow->registrationButton){
            qDebug() << "d";
            registrationWindow = new RegistrationFloatWindow (static_cast<QWidget*>(this->parent()));
            registrationWindow->showWidget(QPoint(static_cast<QWidget*>(this->parent())->width()
                                                 - registrationWindow->width() - 10, 40));
            QObject::connect(registrationWindow, SIGNAL(signalRegistration   (QStringList)),
                             this,               SIGNAL(signalGetRegistration(QStringList)));
        }
        accountInfoWindow->deleteWidget();
        accountInfoWindow = nullptr;
    }
}
