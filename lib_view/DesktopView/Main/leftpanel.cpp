#include "leftpanel.h"

LeftPanel::LeftPanel(QFrame *parent) : QFrame(parent)
{
    listButton.clear();
//    this->setStyleSheet("QFrame {background: #202020; color: white} "
//                        "QPushButton { background: darkgreen; color: white; }");
    this->setFixedWidth(30);

    switchMenuButton = new QPushButton("☰");
    switchMenuButton->setFlat(true);
    switchMenuButton->setFocusPolicy(Qt::NoFocus);
    switchMenuButton->setFixedSize(30, 30);
//    switchMenuButton->setStyleSheet("background: #202020; color: white");
    QObject::connect(switchMenuButton, SIGNAL(clicked(bool)), this, SIGNAL(signalSwitchMenu()));

    QString path = ":/LeftPanel/Files/Images/LeftPanel/";

    listButton.push_back(MService::convertButton(QIcon(":/Other/Files/Images/Other/trayIcon.jpg"), tr("Главная")));
    listButton.push_back(MService::convertButton(QIcon(path + "fileAdd.png"), tr("Создать")));
    listButton.push_back(MService::convertButton(QIcon(path + "viewStick.png"), tr("Просмотр")));
    listButton.push_back(MService::convertButton(QIcon(path + "calendar.png"), tr("Календарь")));

    listLayout = new QVBoxLayout(this);
    listLayout->setSpacing(0);
    listLayout->setMargin(0);
    foreach (QPushButton *b, listButton)
        listLayout->addWidget(b);
    listLayout->addWidget(new QFrame);
    listButton.push_back(MService::convertButton(QIcon(path + "settings.png"), tr("Настройки")));
    listLayout->addWidget(listButton.last());
    listButton.push_back(MService::convertButton(QIcon(path + "hotKey.png"), tr("Гарячие клавиши")));
    listLayout->addWidget(listButton.last());
    listButton.push_back(MService::convertButton(QIcon(path + "info.png"), tr("Справка")));
    listLayout->addWidget(listButton.last());

    foreach (QPushButton *b, listButton)
        QObject::connect(b, SIGNAL(released()), this, SLOT(slotClickToBarButton()));
}

void LeftPanel::slotClickToBarButton()
{
    qDebug() << "LeftPanel::slotClickToBarButton()";
    for (int i = 0; i < listButton.length(); i++){
        if (listButton.at(i) == (QPushButton*)sender()){
            if (listButton.at(i)->isChecked()){
                listButton.at(i)->setChecked(true);
            }
            emit signalSetRightWidget(i);
        }
        else
            listButton.at(i)->setChecked(false);
    }
}
