#ifndef DESKTOPVIEW_H
#define DESKTOPVIEW_H

#include "mprofileinfo.h"
#include "Floating/errormsg.h"
#include "Main/leftpanel.h"
#include "Main/upinformwidget.h"
#include "Tray/traymainwidget.h"
#include "Left/leftmenuwidget.h"
#include "Right/rightcontentwidget.h"

#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/qglobal.h>
#include <QtCore/QSettings>
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QVBoxLayout>

#if defined(DESKTOPVIEW_LIBRARY)
#  define DESKTOPVIEWSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DESKTOPVIEWSHARED_EXPORT Q_DECL_IMPORT
#endif

class DESKTOPVIEWSHARED_EXPORT DesktopView : public QWidget
{
    Q_OBJECT

    QSettings setting;

public:
    explicit DesktopView();

    TrayMainWidget *tray;

private:
    LeftPanel *leftPanel;
    UpInformWidget *upPanel;
    LeftMenuWidget *leftWidget;
    RightContentWidget *rightWidget;

    ErrorMsg *msg;

    MProfileInfo *profile;
    MSettingModel *settingModel;

    QGridLayout *mainLayout;
    QVBoxLayout *rightLayout;

    QPoint fixedPos;
    QPoint mousePos;

    void keyReleaseEvent(QKeyEvent* e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void resizeEvent(QResizeEvent*e);

    bool isInclude(QWidget *widget, QPoint pos);

signals:
    void signalLogout();
    void signalGetLogin(QStringList);
    void signalGetRegistration(QStringList);

    void signalSetData(QList<MAbstractData*>);
    void signalGetData(Menum parametr);

    //write
    void signalWriteTodo(MAbstractData);
    void signalWriteEditTodo(MAbstractData);

public slots:
    void slotSwitchVisible();
    void slotSearchPosibleToolTip();

    void slotSetMsg(QString result, int type);
    void slotCloseMsg();
    void slotSetLogin(MProfileInfo *profileS);

    void slotLogout();
};

#endif // MAINWINDOW_H
