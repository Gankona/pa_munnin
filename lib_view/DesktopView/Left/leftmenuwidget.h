#ifndef LEFTMENUWIDGET_H
#define LEFTMENUWIDGET_H

#include "mwidget.h"

#include <QtCore/QSettings>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

class LeftMenuWidget : public QFrame
{
    Q_OBJECT
public:
    explicit LeftMenuWidget(QFrame *parent = nullptr);

    QLabel *menuName;

    bool isVolantateHide;

private:
    QSettings setting;
    QLabel *infoLeftFrame;
    QVBoxLayout *mainLayout;
    QVBoxLayout *menuLayout;
    QList <QPushButton*> listButton;

    MWidget *calendar;
    MWidget *topNote;
    MWidget *topToDo;
    MWidget *topToDo2;
    MWidget *topToDo3;
    MWidget *topToDo4;
    MWidget *topToDo5;
    MWidget *topToDo6;

public slots:
    void slotSwitchMenuVisible();
};

#endif // LEFTMENUWIDGET_H

