#include "leftmenuwidget.h"

LeftMenuWidget::LeftMenuWidget(QFrame *parent) : QFrame(parent), setting("Gankona", "Munnin")
{
    //this->setStyleSheet("background: #202020; color: white");
    this->setFixedWidth(170);

    menuName = new QLabel(" " + tr("Меню"));
    menuName->setFixedHeight(30);
    //menuName->setStyleSheet("background: #202020; color: white");

    menuLayout = new QVBoxLayout(this);
    menuLayout->setSpacing(1);
    menuLayout->setMargin(1);

    calendar = new MWidget(tr("календарь"));
    topNote  = new MWidget(tr("топ заметки"));
    topToDo  = new MWidget(tr("топ напоминания"));
    topToDo2  = new MWidget(tr("топ напоминания2"));
    topToDo3  = new MWidget(tr("топ напоминания3"));
    topToDo4  = new MWidget(tr("топ напоминания4"));
    topToDo5  = new MWidget(tr("топ напоминания5"));
    topToDo6  = new MWidget(tr("топ напоминания6"));

    infoLeftFrame = new QLabel;

    menuLayout->addWidget(calendar);
    menuLayout->addWidget(topNote);
    menuLayout->addWidget(topToDo);
    menuLayout->addWidget(topToDo2);
    menuLayout->addWidget(topToDo3);
    menuLayout->addWidget(topToDo4);
    menuLayout->addWidget(topToDo5);
    menuLayout->addWidget(topToDo6);
    menuLayout->addWidget(infoLeftFrame);

    bool isVisibleMenu = setting.value("View/VisibleLeftSide", true).toBool();
    this->setVisible(isVisibleMenu);
    isVolantateHide = this->isVisible();
    menuName->setVisible(isVisibleMenu);
}

void LeftMenuWidget::slotSwitchMenuVisible()
{
    qDebug() << "LeftMenuWidget::slotSwitchMenuVisible()";
    if (static_cast<QWidget*>(this->parent())->size().width() <= 550)
        return;
    this->setVisible(! isVolantateHide);
    menuName->setVisible(this->isVisible());
    setting.setValue("View/VisibleLeftSide", this->isVisible());
    isVolantateHide = this->isVisible();
}
