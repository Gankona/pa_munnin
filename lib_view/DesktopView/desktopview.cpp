#include "desktopview.h"

DesktopView::DesktopView() : setting("Gankona", "Munnin")
{
    settingModel = new MSettingModel;
    qDebug() << "DesktopView::DesktopView() 0";
    mousePos = QPoint(-100, -100);
    profile = nullptr;
    msg = nullptr;

//    this->setStyleSheet("background: black");
    tray = new TrayMainWidget;

    mainLayout = new QGridLayout(this);
    mainLayout->setSpacing(2);
    mainLayout->setMargin(0);

    qDebug() << "DesktopView::DesktopView() 1";
    upPanel = new UpInformWidget;
    leftPanel = new LeftPanel;
    qDebug() << "DesktopView::DesktopView() 2";
    leftWidget = new LeftMenuWidget;
    qDebug() << "DesktopView::DesktopView() 3";
    rightWidget = new RightContentWidget(settingModel);

    qDebug() << "DesktopView::DesktopView() 4";
    mainLayout->addWidget(leftPanel->switchMenuButton, 0, 0);
    mainLayout->addWidget(leftPanel, 1, 0);
    mainLayout->addWidget(leftWidget->menuName, 0, 1);
    mainLayout->addWidget(leftWidget, 1, 1);
    mainLayout->addWidget(upPanel, 0, 2);
    mainLayout->addWidget(rightWidget, 1, 2);

    qDebug() << "DesktopView::DesktopView() 5";
    this->slotSearchPosibleToolTip();
    qDebug() << this;

    if (setting.value("Graphic/isFixedStartWindowSize", false).toBool())
        this->resize(setting.value("Graphic/startFixedWindowSize", QVariant(QSize(600, 400))).toSize());
    else
        this->resize(setting.value("Graphic/currentSize", QVariant(QSize(600, 400))).toSize());

    if (setting.value("Graphic/isFixedSizeWindow", false).toBool())
        this->setFixedSize(this->size());
    else
        this->setMinimumSize(setting.value("Graphic/minimumSize", QVariant(QSize(400, 500))).toSize());

    this->setVisible(setting.value("Graphic/isCurrentVisibleWindow", false).toBool());

    qDebug() << "DesktopView::DesktopView() 6 " << this->size();
    QObject::connect(tray, SIGNAL(signalSwitchVisibleWindow()),
                     this, SLOT  (slotSwitchVisible()));

    QObject::connect(leftPanel,  SIGNAL(signalSwitchMenu()),
                     leftWidget, SLOT  (slotSwitchMenuVisible()));

    QObject::connect(leftPanel,   SIGNAL(signalSetRightWidget(int)),
                     rightWidget, SLOT  (slotSetCurrentWidget(int)));

    QObject::connect(upPanel, SIGNAL(signalGetLogin(QStringList)),
                     this,    SIGNAL(signalGetLogin(QStringList)));

    QObject::connect(upPanel, SIGNAL(signalGetRegistration(QStringList)),
                     this,    SIGNAL(signalGetRegistration(QStringList)));

    QObject::connect(upPanel, SIGNAL(signalLogout()),
                     this,    SLOT  (slotLogout()));

    QObject::connect(rightWidget, SIGNAL(signalWriteEditTodo(MAbstractData)),
                     this,        SIGNAL(signalWriteEditTodo(MAbstractData)));

    QObject::connect(rightWidget, SIGNAL(signalWriteTodo(MAbstractData)),
                     this,        SIGNAL(signalWriteTodo(MAbstractData)));

    QObject::connect(rightWidget, SIGNAL(signalGetData(Menum)),
                     this,        SIGNAL(signalGetData(Menum)));

    QObject::connect(this,        SIGNAL(signalSetData(QList<MAbstractData*>)),
                     rightWidget, SIGNAL(signalSetData(QList<MAbstractData*>)));
}

void DesktopView::slotSwitchVisible()
{
    qDebug() << "DesktopView::slotSwitchVisible()";
    this->setVisible(! this->isVisible());
    if (this->isVisible())
        tray->listAction.at(0)->setText(tr("спрятать"));
    else
        tray->listAction.at(0)->setText(tr("показать"));
}

void DesktopView::keyReleaseEvent(QKeyEvent *e)
{
    qDebug() << "DesktopView::keyReleaseEvent(QKeyEvent *e) " << e;
    if (e->key() == Qt::Key_Escape){
        if (upPanel->loginWindow != nullptr){
            upPanel->loginWindow->deleteWidget();
            upPanel->loginWindow = nullptr;
        }
        if (upPanel->accountInfoWindow != nullptr){
            upPanel->accountInfoWindow->deleteWidget();
            upPanel->accountInfoWindow = nullptr;
        }
        if (upPanel->registrationWindow != nullptr){
            upPanel->registrationWindow->deleteWidget();
            upPanel->registrationWindow = nullptr;
        }
        if (msg != nullptr)
            this->slotCloseMsg();
    }
}

void DesktopView::mouseMoveEvent(QMouseEvent *e)
{
    qDebug() << e;
    mousePos = e->pos();
}

void DesktopView::slotSearchPosibleToolTip()
{
    //что я тут хотел сделать?
    QTimer::singleShot(1500, this, SLOT(slotSearchPosibleToolTip()));
    if (mousePos.manhattanLength() - fixedPos.manhattanLength() < 10){}
    fixedPos = mousePos;
}

void DesktopView::mouseReleaseEvent(QMouseEvent *e)
{
    qDebug() << "DesktopView::mouseReleaseEvent(QMouseEvent *e)";
    if (upPanel->loginWindow != nullptr)
        if (! isInclude(upPanel->loginWindow, e->pos())){
            upPanel->loginWindow->deleteWidget();
            upPanel->loginWindow = nullptr;
        }
    if (upPanel->accountInfoWindow != nullptr){
        if (! isInclude(upPanel->accountInfoWindow, e->pos())){
            upPanel->accountInfoWindow->deleteWidget();
            upPanel->accountInfoWindow = nullptr;
        }
    }
    if (upPanel->registrationWindow != nullptr){
        if (! isInclude(upPanel->registrationWindow, e->pos())){
            upPanel->registrationWindow->deleteWidget();
            upPanel->registrationWindow = nullptr;
        }
    }
    if (msg != nullptr){
        if (! isInclude(msg, e->pos()))
            this->slotCloseMsg();
    }
}

void DesktopView::resizeEvent(QResizeEvent *e)
{
    int otstup(10);
    if (! setting.value("Graphic/isFloatLoginWindow", true).toBool()){
        otstup = 0;
        if (upPanel->accountInfoWindow != nullptr)
            upPanel->accountInfoWindow->setFixedSize(upPanel->accountInfoWindow->width(),
                                                     e->size().height() - (40 - otstup));
        if (upPanel->loginWindow != nullptr)
            upPanel->loginWindow->setFixedSize(upPanel->loginWindow->width(),
                                               e->size().height() - (40 - otstup));
        if (upPanel->registrationWindow != nullptr)
            upPanel->registrationWindow->setFixedSize(upPanel->registrationWindow->width(),
                                                      e->size().height() - (40 - otstup));
    }
    qDebug() << "DesktopView::resizeEvent(QResizeEvent *e)";
    if (upPanel->accountInfoWindow != nullptr)
        upPanel->accountInfoWindow->move(QPoint(this->width()
      - upPanel->accountInfoWindow->width() - otstup, 40));
    qDebug() << "DesktopView::resizeEvent(QResizeEvent *e) 1";

    if (upPanel->loginWindow != nullptr)
        upPanel->loginWindow->move(QPoint(this->width()
      - upPanel->loginWindow->width() - otstup, 40));
    qDebug() << "DesktopView::resizeEvent(QResizeEvent *e) 2";

    if (upPanel->registrationWindow != nullptr)
        upPanel->registrationWindow->move(QPoint(this->width()
      - upPanel->registrationWindow->width() - otstup, 40));
    qDebug() << "DesktopView::resizeEvent(QResizeEvent *e) 3";

    if (msg != nullptr){
        msg->move(this->width()/3, this->height()/4);
        msg->setFixedSize(this->width()/3, 100);
    }
    qDebug() << "DesktopView::resizeEvent(QResizeEvent *e) 4";

    if (leftWidget->isVolantateHide){
        if (leftWidget->isVisible()){
            if (e->size().width() <= 550){
                leftWidget->setVisible(false);
                leftWidget->menuName->setVisible(false);
            }
        }
        else {
            if (e->size().width() > 550){
                leftWidget->setVisible(true);
                leftWidget->menuName->setVisible(true);
            }
        }
    }
    qDebug() << "DesktopView::resizeEvent(QResizeEvent *e) 5";
}

bool DesktopView::isInclude(QWidget *widget, QPoint pos)
{
    qDebug() << "DesktopView::isInclude(QWidget *widget, QPoint pos) " << widget;
    if ((pos.y() > widget->y())
            &&(pos.y() < widget->y() + widget->height())
            &&(pos.x() > widget->x())
            &&(pos.x() < widget->x() + widget->width()))
        return true;
    else
        return false;
}

void DesktopView::slotSetMsg(QString result, int type)
{
    qDebug() << "DesktopView::slotSetMsg() " << result;
    if (type == 0){
        if (upPanel->registrationWindow != nullptr)
            upPanel->registrationWindow->errorLabel->setText(result);
    }
    else if (type == 1){}
    else if (type == 2){
        if (upPanel->loginWindow != nullptr)
            upPanel->loginWindow->errorLabel->setText(result);
    }
    else {
        if (upPanel->loginWindow != nullptr){
            delete upPanel->loginWindow;
            upPanel->loginWindow = nullptr;
        }
        if (upPanel->registrationWindow != nullptr){
            delete upPanel->registrationWindow;
            upPanel->registrationWindow = nullptr;
        }
        msg = new ErrorMsg(this, result);
        msg->move((this->width()-this->sizeHint().width())/2,
                  this->height()/3);
        msg->setFixedSize(this->sizeHint() + QSize(50, 10));
        msg->show();
        msg->raise();
        QObject::connect(msg, SIGNAL(signalClose()), this, SLOT(slotCloseMsg()));
    }
}

void DesktopView::slotCloseMsg()
{
    qDebug() << "DesktopView::slotCloseMsg()";
    delete msg;
    msg = nullptr;
}

void DesktopView::slotSetLogin(MProfileInfo *profileS)
{
    qDebug() << "DesktopView::slotSetLogin()";
    profile = profileS;
    profile->login == "NoName" ?
                upPanel->loginLabel->setText(profile->login)
              : upPanel->loginLabel->setText(profile->login + " [" + profile->name + ']');
    settingModel->createSetting(profile->login);
}

void DesktopView::slotLogout()
{
    qDebug() << "DesktopView::slotLogout()";
    emit signalLogout();
    if (profile != nullptr){
        delete profile;
        profile = nullptr;
    }
    emit signalGetLogin(QStringList{"NoName", ""});
}
