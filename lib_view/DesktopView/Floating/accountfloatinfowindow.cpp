#include "accountfloatinfowindow.h"

AccountFloatInfoWindow::AccountFloatInfoWindow(QWidget *parent, MProfileInfo *profile)
    : MFloatOpasityWidget(parent)
{
    titleLabel = new QLabel(tr("Информация про аккаунт"));
    titleLabel->setAlignment(Qt::AlignCenter);

    vLayout = new QVBoxLayout(this);
    vLayout->addWidget(titleLabel);

    if (profile == nullptr){
        loginLabel = new QLabel(" " + tr("NoName"));//59000158153626

        avtorizateButton = new QPushButton(tr("войти"));
        avtorizateButton->setFocusPolicy(Qt::NoFocus);
        registrationButton = new QPushButton("Регистрация");
        registrationButton->setFocusPolicy(Qt::NoFocus);

        vLayout->addWidget(loginLabel);
        vLayout->addWidget(avtorizateButton);
        vLayout->addWidget(registrationButton);
    }
    else{
        loginLabel = new QLabel(" " + profile->login
                                + '[' + profile->name + ']');
        gLayout = new QGridLayout;
        gLayout->setMargin(1);
        gLayout->setSpacing(1);
        leftPanel = new QFrame;
        leftPanel->setFixedWidth(20);
        gLayout->addWidget(leftPanel, 0, 0, profile->listIpPort.length() - 1, 0);
        for (int i = 0; i < profile->listIpPort.length(); i++)
            gLayout->addWidget(new QLabel(profile->listIpPort.at(i)->name), i, 1);
        settingsButton = new QPushButton(QIcon(":/LeftPanel/Files/Images/LeftPanel/settings.png"), tr("Настройка"));
        settingsButton->setFocusPolicy(Qt::NoFocus);
        settingsButton->setFixedHeight(20);
        logoutButton = new QPushButton(QIcon(":/Account/Files/Images/Account/logout.png"), "");
        logoutButton->setFocusPolicy(Qt::NoFocus);
        logoutButton->setFixedSize(20, 20);

        hLayout = new QHBoxLayout;
        hLayout->addWidget(settingsButton);
        hLayout->addWidget(logoutButton);

        vLayout->addWidget(loginLabel);
        vLayout->addLayout(gLayout);
        vLayout->addLayout(hLayout);

        QObject::connect(logoutButton, SIGNAL(released()), this, SIGNAL(signalLogout()));
        QObject::connect(settingsButton, SIGNAL(released()), this, SIGNAL(signalOpenEditAccount()));
    }
    errorLabel = new QLabel;
    vLayout->addWidget(errorLabel);
    loginLabel->setAlignment(Qt::AlignLeft);

    this->setFixedSize(vLayout->sizeHint());
    //this->setStyleSheet("background: #000000; color: white");
}
