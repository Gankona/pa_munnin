#include "errormsg.h"

ErrorMsg::ErrorMsg(QWidget *parent, QString text, float opacityValue) : QLabel(text, parent)
{
    qDebug() << "ErrorMsg::ErrorMsg(QWidget *parent, QString text, float opacityValue)";
    //this->setStyleSheet("background: black; color:white; "
      //                  "border: 2px solid white;");
    this->setAlignment(Qt::AlignCenter);
    opacity = new QGraphicsOpacityEffect(this);
    opacity->setOpacity(opacityValue);
    this->setWordWrap(true);

    QTimer::singleShot(7000, this, SIGNAL(signalClose()));
}

