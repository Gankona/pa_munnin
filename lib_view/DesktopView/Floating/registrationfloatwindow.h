#ifndef REGISTRATIONFLOATWINDOW_H
#define REGISTRATIONFLOATWINDOW_H

#include "mservice.h"
#include "mpasswordswitcher.h"
#include "mfloatopasitywidget.h"

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

class RegistrationFloatWindow : public MFloatOpasityWidget
{
    Q_OBJECT
public:
    explicit RegistrationFloatWindow(QWidget *parent = 0);

    QLabel *errorLabel;

private:
    QLabel *nameLabel;
    QLabel *emailLabel;
    QLabel *titleLabel;
    QLabel *loginLabel;
    QLabel *passwordLabel;
    QLabel *password2Label;
    QLineEdit *nameEdit;
    QLineEdit *emailEdit;
    QLineEdit *loginEdit;
    QLineEdit *passwordEdit;
    QLineEdit *password2Edit;
    QPushButton *registrationButton;
    QVBoxLayout *vLayout;

    MPasswordSwitcher *passwordSwitcher;

signals:
    void signalRegistration(QStringList);

public slots:
    void slotRegistration();
};

#endif // REGISTRATIONFLOATWINDOW_H
