#ifndef ERRORMSG_H
#define ERRORMSG_H

#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtWidgets/QLabel>

class ErrorMsg : public QLabel
{
    Q_OBJECT
public:
    explicit ErrorMsg(QWidget *parent = nullptr,
                      QString text = "",
                      float opacityValue = 0.7);

private:
    QGraphicsOpacityEffect *opacity;

signals:
    void signalClose();
};

#endif // ERRORMSG_H
