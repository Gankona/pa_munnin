#include "loginfloatwindow.h"

LoginFloatWindow::LoginFloatWindow(QWidget *parent)
    : MFloatOpasityWidget(parent)
{
    titleLabel = new QLabel(tr("Вход в аккаунт"));
    titleLabel->setAlignment(Qt::AlignCenter);

    loginLabel = new QLabel(" " + tr("логин"));
    loginLabel->setAlignment(Qt::AlignLeft);

    passwordLabel = new QLabel(" " + tr("пароль"));
    passwordLabel->setAlignment(Qt::AlignLeft);

    loginEdit = new QLineEdit;

    passwordEdit = new QLineEdit;
    passwordEdit->setEchoMode(QLineEdit::Password);

    forgetPasswordButton = new QPushButton("Забыли пароль?");
    forgetPasswordButton->setFocusPolicy(Qt::NoFocus);

    loginButton = new QPushButton(tr("войти"));
    loginButton->setFocusPolicy(Qt::NoFocus);

    passwordSwitcher = new MPasswordSwitcher(passwordEdit);

    errorLabel = new QLabel;
    QFont font;
    font.setPixelSize(8);
    errorLabel->setFont(font);
    errorLabel->setWordWrap(true);

    vLayout = new QVBoxLayout(this);
    vLayout->addWidget(titleLabel);
    vLayout->addWidget(loginLabel);
    vLayout->addWidget(loginEdit);
    vLayout->addWidget(passwordLabel);
    vLayout->addWidget(passwordEdit);
    vLayout->addWidget(passwordSwitcher);
    vLayout->addWidget(forgetPasswordButton);
    vLayout->addWidget(loginButton);
    vLayout->addWidget(errorLabel);

    this->setFixedSize(vLayout->sizeHint());
    //this->setStyleSheet("background: #000000; color: white");

    QObject::connect(loginButton, SIGNAL(pressed()), this, SLOT(slotLogin()));
}

void LoginFloatWindow::slotLogin()
{
    qDebug() << "LoginFloatWindow::slotLogin()";
    QString result = MService::checkDataInput(loginEdit->text(), tr("логин"));
    if (result != ""){
        errorLabel->setText(result);
        return;
    }

    result = MService::checkDataInput(passwordEdit->text(), tr("пароль"));
    if ((result != "") && (loginEdit->text() != "NoName")){
        errorLabel->setText(result);
        return;
    }

    emit signalLogin(QStringList{loginEdit->text(), passwordEdit->text()});
}
