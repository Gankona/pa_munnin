#ifndef LOGINFLOATWINDOW_H
#define LOGINFLOATWINDOW_H

#include "mservice.h"
#include "mpasswordswitcher.h"
#include "mfloatopasitywidget.h"

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

class LoginFloatWindow : public MFloatOpasityWidget
{
    Q_OBJECT
public:
    explicit LoginFloatWindow(QWidget *parent = 0);

    QLabel *errorLabel;

private:
    QLabel *loginLabel;
    QLabel *titleLabel;
    QLabel *passwordLabel;
    QLineEdit *loginEdit;
    QLineEdit *passwordEdit;
    QPushButton *loginButton;
    QPushButton *forgetPasswordButton;
    QVBoxLayout *vLayout;

    MPasswordSwitcher *passwordSwitcher;

signals:
    void signalLogin(QStringList);

public slots:
    void slotLogin();
};

#endif // LOGINFLOATWINDOW_H
