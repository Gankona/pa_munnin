#include "registrationfloatwindow.h"

RegistrationFloatWindow::RegistrationFloatWindow(QWidget *parent)
    : MFloatOpasityWidget(parent)
{
    titleLabel = new QLabel(tr("Регистрация"));
    titleLabel->setAlignment(Qt::AlignCenter);

    loginLabel = new QLabel(" " + tr("логин"));
    loginLabel->setAlignment(Qt::AlignLeft);

    nameLabel = new QLabel(" " + tr("имя"));
    nameLabel->setAlignment(Qt::AlignLeft);

    emailLabel = new QLabel(" " + tr("e-mail"));
    emailLabel->setAlignment(Qt::AlignLeft);

    passwordLabel = new QLabel(" " + tr("пароль"));
    passwordLabel->setAlignment(Qt::AlignLeft);

    password2Label = new QLabel(" " + tr("повтор пароля"));
    password2Label->setAlignment(Qt::AlignLeft);

    nameEdit = new QLineEdit;
    emailEdit = new QLineEdit;
    loginEdit = new QLineEdit;

    passwordEdit = new QLineEdit;
    passwordEdit->setEchoMode(QLineEdit::Password);

    password2Edit = new QLineEdit;
    password2Edit->setEchoMode(QLineEdit::Password);

    registrationButton = new QPushButton(tr("Зарегистрировать"));
    registrationButton->setFocusPolicy(Qt::NoFocus);

    passwordSwitcher = new MPasswordSwitcher(passwordEdit, password2Edit);

    errorLabel = new QLabel;
    QFont font;
    font.setPixelSize(8);
    errorLabel->setFont(font);
    errorLabel->setWordWrap(true);

    vLayout = new QVBoxLayout(this);
    vLayout->addWidget(titleLabel);
    vLayout->addWidget(loginLabel);
    vLayout->addWidget(loginEdit);
    vLayout->addWidget(nameLabel);
    vLayout->addWidget(nameEdit);
    vLayout->addWidget(emailLabel);
    vLayout->addWidget(emailEdit);
    vLayout->addWidget(passwordLabel);
    vLayout->addWidget(passwordEdit);
    vLayout->addWidget(password2Label);
    vLayout->addWidget(password2Edit);
    vLayout->addWidget(passwordSwitcher);
    vLayout->addWidget(registrationButton);
    vLayout->addWidget(errorLabel);

    this->setFixedSize(vLayout->sizeHint());
    //this->setStyleSheet("background: #000000; color: white");

    QObject::connect(registrationButton, SIGNAL(pressed()), this, SLOT(slotRegistration()));
}

void RegistrationFloatWindow::slotRegistration()
{
    qDebug() << "RegistrationFloatWindow::slotRegistration()";
    QString result = MService::checkDataInput(nameEdit->text(), tr("имя"));
    if (result != ""){
        errorLabel->setText(result);
        return;
    }

    result = MService::checkDataInput(loginEdit->text(), tr("логин"));
    if (result != ""){
        errorLabel->setText(result);
        return;
    }

    result = MService::checkEmail(emailEdit->text());
    if (result != ""){
        errorLabel->setText(result);
        return;
    }

    result = MService::checkDataInput(passwordEdit->text(), tr("пароль"));
    if (result != ""){
        errorLabel->setText(result);
        return;
    }

    if (passwordEdit->text() != password2Edit->text()){
        errorLabel->setText(tr("Пароли не совпадают"));
        return;
    }

    emit signalRegistration(QStringList{nameEdit->text(), loginEdit->text(),
                                        emailEdit->text(), passwordEdit->text()});
}
