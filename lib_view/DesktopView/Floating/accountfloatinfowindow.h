#ifndef ACCOUNTFLOATINFOWINDOW_H
#define ACCOUNTFLOATINFOWINDOW_H

#include "mservice.h"
#include "mprofileinfo.h"
#include "mfloatopasitywidget.h"

#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QVBoxLayout>

class AccountFloatInfoWindow : public MFloatOpasityWidget
{
    Q_OBJECT
public:
    explicit AccountFloatInfoWindow(QWidget *parent = 0, MProfileInfo *profile = nullptr);

    QPushButton *logoutButton;
    QPushButton *settingsButton;
    QPushButton *avtorizateButton;
    QPushButton *registrationButton;

private:
    QLabel *emailLabel;
    QLabel *errorLabel;
    QLabel *loginLabel;
    QLabel *nameLabel;
    QLabel *titleLabel;
    QFrame *leftPanel;

    QGridLayout *gLayout;
    QHBoxLayout *hLayout;
    QVBoxLayout *vLayout;

signals:
    void signalLogout();
    void signalOpenEditAccount(); 
};

#endif // ACCOUNTFLOATINFOWINDOW_H
