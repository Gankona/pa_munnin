#ifndef TRAYMAINWIDGET_H
#define TRAYMAINWIDGET_H

#include <QtCore/QObject>
#include <QtWidgets/QMenu>
#include <QtWidgets/QApplication>
#include <QtWidgets/QSystemTrayIcon>

class TrayMainWidget : public QSystemTrayIcon
{
    Q_OBJECT
public:
    TrayMainWidget();

    QMenu *menu;
    QList <QAction*> listAction;

signals:
    void signalSwitchVisibleWindow();

public slots:
    void slotClickToMenuButton();
};

#endif // TRAYMAINWIDGET_H
