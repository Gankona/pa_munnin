#include "traymainwidget.h"

TrayMainWidget::TrayMainWidget()
{
    this->setIcon(QIcon(":/Other/Files/Images/Other/trayIcon.jpg"));

    listAction.clear();
    listAction.push_back(new QAction(QIcon(":/Other/Files/Images/Other/showHideIcon.jpg"), tr("показать"), nullptr));
    listAction.push_back(new QAction(QIcon(":/Other/Files/Images/Other/exit.png"), tr("выход"), nullptr));

    menu = new QMenu;
    menu->addAction(listAction.at(0));
    menu->addSeparator();
    menu->addAction(listAction.at(1));
    this->setContextMenu(menu);

    foreach (QAction *a, listAction)
        QObject::connect(a, SIGNAL(triggered(bool)), this, SLOT(slotClickToMenuButton()));
}

void TrayMainWidget::slotClickToMenuButton()
{
    int choose(-1);
    for (int i = 0; i < listAction.length(); i++)
        if ((QAction*)sender() == listAction.at(i))
            choose = i;
    switch (choose) {
    case 0:
        emit signalSwitchVisibleWindow();
        break;
    case 1:
        QApplication::exit();
        break;
    default:
        break;
    }
}
