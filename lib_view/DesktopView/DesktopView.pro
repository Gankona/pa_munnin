#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T23:40:52
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

TARGET = DesktopView
TEMPLATE = lib

DEFINES += DESKTOPVIEW_LIBRARY

SOURCES += desktopview.cpp \
    Floating/accountfloatinfowindow.cpp \
    Floating/errormsg.cpp \
    Floating/loginfloatwindow.cpp \
    Floating/registrationfloatwindow.cpp \
    Left/leftmenuwidget.cpp \
    Main/leftpanel.cpp \
    Main/upinformwidget.cpp \
    Right/Calendar/calendarr.cpp \
    Right/Settings/accountsetting.cpp \
    Right/Settings/hotkeysetting.cpp \
    Right/Settings/interfacesetting.cpp \
    Right/Settings/networksetting.cpp \
    Right/Settings/settingsview.cpp \
    Right/ViewData/viewcalendar.cpp \
    Right/ViewData/viewdata.cpp \
    Right/ViewData/viewstick.cpp \
    Right/WriteNew/writenew.cpp \
    Right/WriteNew/writeplan.cpp \
    Right/WriteNew/writestick.cpp \
    Right/WriteNew/writetodo.cpp \
    Right/homewidget.cpp \
    Right/hotkeyr.cpp \
    Right/infor.cpp \
    Right/rightcontentwidget.cpp \
    Tray/traymainwidget.cpp \
    Right/Settings/graphicsetting.cpp

HEADERS += desktopview.h \
    Floating/accountfloatinfowindow.h \
    Floating/errormsg.h \
    Floating/loginfloatwindow.h \
    Floating/registrationfloatwindow.h \
    Left/leftmenuwidget.h \
    Main/leftpanel.h \
    Main/upinformwidget.h \
    Right/Calendar/calendarr.h \
    Right/Settings/accountsetting.h \
    Right/Settings/hotkeysetting.h \
    Right/Settings/interfacesetting.h \
    Right/Settings/networksetting.h \
    Right/Settings/settingsview.h \
    Right/ViewData/viewcalendar.h \
    Right/ViewData/viewdata.h \
    Right/ViewData/viewstick.h \
    Right/WriteNew/writenew.h \
    Right/WriteNew/writeplan.h \
    Right/WriteNew/writestick.h \
    Right/WriteNew/writetodo.h \
    Right/homewidget.h \
    Right/hotkeyr.h \
    Right/infor.h \
    Right/rightcontentwidget.h \
    Tray/traymainwidget.h \
    Right/Settings/graphicsetting.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    resources.qrc

include($$PWD/../../lib/MProfileInfo/MProfileInfo.pri)
include($$PWD/../../lib_view/MWritePrototype/MWritePrototype.pri)
include($$PWD/../../lib_view/MPasswordSwitcher/MPasswordSwitcher.pri)
include($$PWD/../../lib_view/MFloatOpasityWidget/MFloatOpasityWidget.pri)
include($$PWD/../../lib_view/MPrototypeRight/MPrototypeRight.pri)
include($$PWD/../../lib_view/MPrototypeView/MPrototypeView.pri)
include($$PWD/../../lib_view/MListEdit/MListEdit.pri)
include($$PWD/../../lib/MSettingModel/MSettingModel.pri)
include($$PWD/../../lib_view/MMiddleScrollWidget/MMiddleScrollWidget.pri)
include($$PWD/../../lib_view/MItemSwitch/MItemSwitch.pri)

DISTFILES += \
    DesktopView.pri
