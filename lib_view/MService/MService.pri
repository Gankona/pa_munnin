isEmpty(MService.pri){
    CONFIG(debug, debug|release): LIBS += -L$$PWD/../../debug/lib_view/MService/ -lMService
    CONFIG(release, debug|release): LIBS += -L$$PWD/../../release/lib_view/MService/ -lMService
    INCLUDEPATH += $$PWD/../../lib_view/MService
    DEPENDPATH += $$PWD/../../lib_view/MService
}
