#ifndef MSERVICE_H
#define MSERVICE_H

#include <QtCore/qglobal.h>
#include <QtCore/QObject>

#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QGridLayout>

#if defined(MSERVICE_LIBRARY)
#  define MSERVICESHARED_EXPORT Q_DECL_EXPORT
#else
#  define MSERVICESHARED_EXPORT Q_DECL_IMPORT
#endif

class MSERVICESHARED_EXPORT MService
{
public:
    explicit MService();

    static QPushButton* convertButton(QIcon icon,
                                      QString toolTip = "",
                                      QWidget *parent = nullptr);
    static QPushButton* convertButton(QString name = "",
                                      QString toolTip = "",
                                      QWidget *parent = nullptr);
    static QPushButton* convertButton(QString name = "",
                                      QWidget *parent = nullptr);
    static QPushButton* convertButton(QString name = "", int w = 30,
                                      int h = 30);
    static void createItem(QHBoxLayout *box,
                           QWidget *wgt,
                           QLabel *lbl,
                           QVBoxLayout *mainLayout,
                           QString text,
                           bool isButtonPresent = false,
                           QPushButton *y = nullptr,
                           QPushButton *n = nullptr,
                           QString yText = "", QString nText = "");
    static QString checkEmail(QString emailSend);
    static QString checkDataInput(QString data, QString nameField);
    static QHBoxLayout* getHLayout(QWidget *parent = nullptr, int margin = 0, int spacing = 0);
    static QVBoxLayout* getVLayout(QWidget *parent = nullptr, int margin = 0, int spacing = 0);
    static QGridLayout* getGLayout(QWidget *parent = nullptr, int margin = 0, int spacing = 0);
};

#endif // MSERVICE_H
