#include "mservice.h"

MService::MService()
{

}

QPushButton* MService::convertButton(QIcon icon, QString toolTip, QWidget *parent)
{
    QPushButton *b = new QPushButton(icon, "", parent);
    b->setFlat(true);
    b->setCheckable(true);
    b->setToolTip(toolTip);
    b->setFixedSize(30, 30);
    b->setFocusPolicy(Qt::NoFocus);
    return b;
}

QPushButton* MService::convertButton(QString name, QString toolTip, QWidget *parent)
{
    QPushButton *b = new QPushButton(name, parent);
    b->setFlat(true);
    b->setCheckable(true);
    b->setToolTip(toolTip);
    b->setFixedSize(30, 30);
    b->setFocusPolicy(Qt::NoFocus);
    return b;
}

QPushButton* MService::convertButton(QString name, QWidget *parent)
{
    QPushButton *b = new QPushButton(name, parent);
    b->setFlat(true);
    b->setCheckable(true);
    b->setFixedSize(30, 30);
    b->setFocusPolicy(Qt::NoFocus);
    return b;
}

QPushButton* MService::convertButton(QString name, int w, int h)
{
    QPushButton *b = new QPushButton(name);
    b->setFlat(true);
    b->setCheckable(true);
    b->setFixedSize(w, h);
    b->setFocusPolicy(Qt::NoFocus);
    return b;
}

void MService::createItem(QHBoxLayout *box, QWidget *wgt,
                          QLabel *lbl, QVBoxLayout *mainLayout,
                          QString text, bool isButtonPresent,
                          QPushButton *y, QPushButton *n,
                          QString yText, QString nText)
{
    box = new QHBoxLayout;
    box->setMargin(0);
    box->setSpacing(0);
    wgt = new QWidget;
    wgt->setLayout(box);
    QWidget *wgt2 = new QWidget;
    wgt2->setFixedWidth(25);
    lbl = new QLabel(text);
    lbl->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    lbl->setFixedHeight(25);
    box->addWidget(lbl);
    box->addWidget(wgt2);
    mainLayout->addWidget(wgt);
    if (isButtonPresent){
        y = MService::convertButton(yText, 60, 25);
        n = MService::convertButton(nText, 60, 25);
        box->addWidget(y);
        box->addWidget(n);
    }
    wgt->repaint();
}

QString MService::checkDataInput(QString data, QString nameField)
{
    if (data.length() == 0)
        return QObject::tr("Поле") + ' ' + nameField + ' ' + QObject::tr("обязательно для заполнения");
    for (int i = 0; i < data.length(); i++)
        if (! data[i].isLetterOrNumber())
            if (data[i] != ' ')
                return QObject::tr("Недопустимый символ ") + data[i];
    return "";
}

QString MService::checkEmail(QString emailSend)
{
    int beforeEmail = 0;
    int afterEmail = 0;
    int emailCount = 0;
    if (emailSend.length() == 0)
        return QObject::tr("Поле почты обязательно для заполнения");
    for (int i = 0; i < emailSend.length(); i++){
        if (emailSend[i].isLetterOrNumber() || emailSend[i] == '@'){
            (emailSend[i].isLetterOrNumber()) ?
                        ((emailCount != 0) ? afterEmail++ : beforeEmail++)
                      : emailCount++;
        }
        else if (emailSend[i] == '.');
        else
            return QObject::tr("Недопустимый символ ") + emailSend[i];
    }
    if (emailCount == 0)
        return QObject::tr("Это не почта");
    else if (emailCount != 1)
        return QObject::tr("Почему больше одного @?");
    else if (afterEmail == 0)
        return QObject::tr("Отсутствует домен почты");
    else if (beforeEmail == 0)
        return QObject::tr("Нет ничего перед @");
    return "";
}

QHBoxLayout* getHLayout(QWidget *parent, int margin, int spacing)
{
    QHBoxLayout *layout = new QHBoxLayout;
    layout->setSpacing(spacing);
    layout->setMargin(margin);
    if (parent != nullptr)
        parent->setLayout(layout);
    return layout;
}

QVBoxLayout* getVLayout(QWidget *parent, int margin, int spacing)
{
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(spacing);
    layout->setMargin(margin);
    if (parent != nullptr)
        parent->setLayout(layout);
    return layout;
}

QGridLayout* getGLayout(QWidget *parent, int margin, int spacing)
{
    QGridLayout *layout = new QGridLayout;
    layout->setSpacing(spacing);
    layout->setMargin(margin);
    if (parent != nullptr)
        parent->setLayout(layout);
    return layout;
}
