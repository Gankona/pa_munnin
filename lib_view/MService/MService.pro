#-------------------------------------------------
#
# Project created by QtCreator 2016-01-09T21:59:08
#
#-------------------------------------------------

CONFIG += c++11

QT       += widgets

QT       -= gui

TARGET = MService
TEMPLATE = lib

DEFINES += MSERVICE_LIBRARY

SOURCES += mservice.cpp

HEADERS += mservice.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    MService.pri
